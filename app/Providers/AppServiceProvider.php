<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('title', function ($expression) {
            return "<?php echo str_replace('_', ' ', ucwords($expression, '_')); ?>";
        });

        Validator::extend('alpha_space', function ($attribute, $value) {
            // This will only accept alpha and spaces.
            // If you want to accept hyphens use: /^[\pL\s-]+$/u.
            return preg_match('/^[\pL\s]+$/u', $value);
        });

        Blade::if('env', function ($environment) {
            return app()->environment($environment);
        });


        // DB::listen(function($query) {
        //     if (!file_exists('logs')) mkdir('logs', 0777, true);
        //     fwrite(fopen("logs/query.log", "a"), Carbon::now()->format('Y-m-d H:i:s.u')." ".json_encode($query, true)."\n");
        // });
    }
}
