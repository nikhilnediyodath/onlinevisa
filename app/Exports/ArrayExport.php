<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class ArrayExport implements FromView
{

    public function __construct(array $array){
        $this->array = $array;
    }

    /**
    * @return \Illuminate\Contracts\View\View
    */
    public function view(): View {
        return view('exports.table')->with('table', $this->array);
    }
}
