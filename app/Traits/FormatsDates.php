<?php

namespace App\Traits;

use Carbon\Carbon;
trait FormatsDates
{

    public function getUpdatedAtAttribute($value) {
       return Carbon::parse($value)->format(config('data.date_format'));
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format(config('data.date_format'));
    }

    public function dateFormat(){
        return $this->casts;
    }
}