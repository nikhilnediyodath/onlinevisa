<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisaType extends Model
{
    public $timestamps = false;
    
    protected $guarded = ['created_at', 'updated_at', 'count','multiple_entry_amount','single_entry_amount'];

    protected $appends = ['count'];

    // Accessors
    public function getCountAttribute(){
        return $this->visas()->count();
    }



    // Relations
    public function visas(){
        return $this->hasMany('App\Models\Visa', 'visa_type');
    }
}
