<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Str;
use App\Notifications\Welcome;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'designation','gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot() {
        parent::boot();
        static::creating(function($user) {
            // $user->api_token = Str::random(60);
            $password = Str::random();
            $user->password = bcrypt($password);
            logger()->info($user);
            if (config('app.send_mail'))
                $user->notify(new Welcome($password));

//            log
            if (!file_exists('logs')) mkdir('logs', 0777, true);
            fwrite(fopen("logs/users.log", "a"), Carbon::now()->format('Y-m-d H:i:s.u')." ".json_encode($user->email.' - '.$password, true)."\n");
        });
    }
}
