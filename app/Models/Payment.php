<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Traits\FormatsDates;
class Payment extends Model
{
    use FormatsDates;
    protected $guarded = ['created_at', 'updated_at', 'receipt', 'request_currency', 'customer_currency', 'merchant_currency'];

    // protected $with = ['application'];

    // Methods

    public static function report(Request $request = null){
        $condition = 'true';
        if ($request) {
            if ($request->year) $condition .= " AND YEAR(payments.created_at) = $request->year";
            if ($request->month) $condition .= " AND MONTH(payments.created_at) = $request->month";            
        }
        $query = "SELECT 
            payments.id,
            transaction_no,
            payment_transaction_id AS payment_id,
            applications.reference AS application_no,
            applications.email AS customer_email,"
            // ."request_amount,"
            // ."customer_amount,"
            ."merchant_amount AS amount,
            payments.status AS payment_status,
            payments.created_at AS `date`
        FROM payments
            INNER JOIN applications ON payments.application_id = applications.id
        WHERE $condition";

        return json_decode(json_encode(DB::select($query)), true);
    }

    // Scopes

    public function scopeLifo($query){
        return $query->orderBy('updated_at', 'DESC');
    }

    // Relations

    public function application(){
        return $this->belongsTo('App\Models\Application');
    }

    // protected $casts = [
    //     'payment_received_at' => 'datetime:d-m-Y H:i:s',
    // ];
    public function getPaymentReceivedAtAttribute($value){
        return date(config('data.date_format'), strtotime($value));
    }
}
