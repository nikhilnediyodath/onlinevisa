<?php

namespace App\Models;

use App\Models\Base\Application as BaseApplication;
use Illuminate\Database\Eloquent\Builder;

class Application extends BaseApplication
{

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('temporary', function (Builder $builder) {
            $builder->where('status', '<>','temp');
        });
    }
}
