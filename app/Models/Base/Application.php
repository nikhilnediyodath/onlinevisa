<?php

namespace App\Models\Base;

use App\Models\Visa;
use App\Traits\FormatsDates;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Application extends Model
{
    use Notifiable, FormatsDates;

    protected $with = ['visas', 'promocode', 'attachments', 'payment'];
    protected $hidden = ['promocode_id','otp'];
    // protected $appends = ['icon'];


    // protected $attributes = [
    //     'message' => 'We are processing your request',
    // ];

    // Status Methods
    // public static function statusList(){
    //     return [
    //         'Processing' => 'sync',
    //         'Suspended' => 'archive',
    //         'Approved' => 'check_circle',
    //         'Rejected' => 'cancel',
    //         'Refunded' => 'monetization_on'
    //     ];
    // }

    // public function getIconAttribute(){
    //     return self::statusList()[$this->status];
    // }

    public function getApplicantNameAttribute(){
        return $this->visas()->first()->first_name;
    }

    // Relations

    public function visas(){
        return $this->hasMany('App\Models\Visa');
    }
    public function attachments(){
        return $this->morphMany('App\Models\File', 'parent');
    }
    public function payment(){
        return $this->hasOne('App\Models\Payment');
    }
    public function promocode(){
        return $this->belongsTo('App\Models\Promocode');
    }


    // Scopes

    public function scopeLifo($query){
        return $query->orderBy('updated_at', 'DESC');
    }
    public function scopeLastMonth($query){
        return $query->where('created_at', '>=', Carbon::now()->startOfMonth());
    }
    public function scopeProcessing($query){
        return $query->where('status', 'Processing');
    }
    public function scopeRejected($query){
        return $query->where('status', 'Rejected');
    }
    public function scopeApproved($query){
        return $query->where('status', 'Approved');
    }
    public function scopeSuspended($query){
        return $query->where('status', 'Suspended');
    }

    // Methods

    public static function report(Request $request = null){
        $condition = 'true';
        if ($request) {
            if ($request->year) $condition .= " AND YEAR(applications.created_at) = $request->year";
            if ($request->month) $condition .= " AND MONTH(applications.created_at) = $request->month";
        }
        $query =
            "SELECT
            applications.id,
            reference,
            email,
            applications.status,
            COUNT(visas.id) AS visas,
            total_amt AS cost,
            applications.amount AS payable,
            promocodes.name AS promo,
            payments.payment_transaction_id AS payment_id,
            payments.status AS payment,
            applications.created_at AS `date`
        FROM applications
            LEFT OUTER JOIN visas ON visas.application_id = applications.id
            LEFT JOIN payments ON payments.application_id = applications.id
            LEFT JOIN promocodes ON promocodes.id = applications.promocode_id
        WHERE $condition
        GROUP BY
            visas.id,
            applications.id,
            reference,
            email,
            applications.status,
            applications.total_amt,
            applications.amount,
            promocodes.name,
            payments.payment_transaction_id,
            payments.status,
            `date`";
        return json_decode(json_encode(DB::select($query)), true);
    }
    public static function summary(){
        return [
            'lifetime' => [
                'processing_count' => self::processing()->count(),
                'rejected_count' => self::rejected()->count(),
                'approved_count' => self::approved()->count(),
                'suspended_count' => self::suspended()->count(),

                'processing_visas' => Visa::whereHas('application', function(Builder $query){ $query->processing(); })->count(),
                'rejected_visas' => Visa::whereHas('application', function(Builder $query){  $query->rejected(); })->count(),
                'approved_visas' => Visa::whereHas('application', function(Builder $query){  $query->approved(); })->count(),
                'suspended_visas' => Visa::whereHas('application', function(Builder $query){  $query->suspended(); })->count(),
            ],
            'this_month' => [
                'processing_count' => self::processing()->lastMonth()->count(),
                'rejected_count' => self::rejected()->lastMonth()->count(),
                'approved_count' => self::approved()->lastMonth()->count(),
                'suspended_count' => self::suspended()->lastMonth()->count(),

                'processing_visas' => Visa::whereHas('application', function(Builder $query){ $query->processing(); })->lastMonth()->count(),
                'rejected_visas' => Visa::whereHas('application', function(Builder $query){  $query->rejected(); })->lastMonth()->count(),
                'approved_visas' => Visa::whereHas('application', function(Builder $query){  $query->approved(); })->lastMonth()->count(),
                'suspended_visas' => Visa::whereHas('application', function(Builder $query){  $query->suspended(); })->lastMonth()->count(),
            ]
        ];
    }
}
