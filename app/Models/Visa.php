<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\FormatsDates;

class Visa extends Model
{
    use FormatsDates;
    // protected $dates = ['arrival_date', 'date_of_birth', 'passport_issue_date', 'passport_expiry_date'];
    protected $guarded = ['created_at', 'updated_at', 'user_image', 'national_id', 'passport_first_page', 'passport_last_page', 'attachments', 'citizen', 'language_one', 'language_two', 'residency', 'visatype', 'full_name'];
    protected $with = ['citizen', 'residency', 'visatype', 'language_one', 'language_two'];
    protected $appends = ['attachments', 'user_image', 'national_id', 'passport_first_page', 'passport_last_page', 'full_name'];

    // protected $dateFormat = 'Y-m-d';

    
    public function scopeLastMonth($query){
        return $query->where('created_at', '>=', Carbon::now()->startOfMonth());
    }

    public function setArrivalDateAttribute($value) {
        $this->attributes['arrival_date'] = Carbon::createFromFormat('d-m-Y', $value)->toDateTimeString();
    }
    public function setDateOfBirthAttribute($value) {
        $this->attributes['date_of_birth'] = Carbon::createFromFormat('d-m-Y', $value)->toDateTimeString();
    }
    public function setPassportIssueDateAttribute($value) {
        $this->attributes['passport_issue_date'] = Carbon::createFromFormat('d-m-Y', $value)->toDateTimeString();
    }
    public function setPassportExpiryDateAttribute($value) {
        $this->attributes['passport_expiry_date'] = Carbon::createFromFormat('d-m-Y', $value)->toDateTimeString();
    }

    // Accessors
    public function getFullNameAttribute(){
        return trim(str_replace('  ', ' ', "{$this->first_name} {$this->middle_name} {$this->last_name}"));
    }
    public function getAttachmentsAttribute(){
        return $this->files()->where('file_type', 'attachments')->get();
    }
    public function getUserImageAttribute(){
        return $this->files()->where('file_type', 'user_image')->first();
    }
    public function getNationalIdAttribute(){
        return $this->files()->where('file_type', 'national_id')->first();
    }
    public function getPassportFirstPageAttribute(){
        return $this->files()->where('file_type', 'passport_first_page')->first();
    }
    public function getPassportLastPageAttribute(){
        return $this->files()->where('file_type', 'passport_last_page')->first();
    }

    // Relations

    public function files(){
        return $this->morphMany('App\Models\File', 'parent');
    }
    public function application(){
        return $this->belongsTo('App\Models\Application');
    }
    public function citizen(){
        return $this->belongsTo('App\Models\Country', 'citizenship');
    }
    public function residency(){
        return $this->belongsTo('App\Models\Country', 'residence');
    }
    public function language_one(){
        return $this->belongsTo('App\Models\Language', 'first_language');
    }
    public function language_two(){
        return $this->belongsTo('App\Models\Language', 'second_language');
    }
    public function visatype(){
        return $this->belongsTo('App\Models\VisaType', 'visa_type');
    }

    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
        'created_at' => 'datetime:d-m-Y',
        'arrival_date' => 'datetime:d-m-Y',
        'date_of_birth' => 'datetime:d-m-Y',
        'passport_issue_date' => 'datetime:d-m-Y',
        'passport_expiry_date' => 'datetime:d-m-Y',
    ];
}
