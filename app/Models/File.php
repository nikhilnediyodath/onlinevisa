<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App;

class File extends Model
{
    protected $fillable = ['name', 'file_type', 'doc_type'];
    protected $appends = ['url', 'mime', 'thumb'];

    protected static function boot() {
        parent::boot();
        static::deleting(function($file) {
            Storage::delete($file->name);
        });
    }

    public function getUrlAttribute(){
        return Storage::url($this->name);
    }
    public function getThumbAttribute(){
        return $this->mime == 'application/pdf' ?
            App::environment('prod') ?
                'http://drive.google.com/viewerng/viewer?embedded=true&url='.Storage::url($this->name):
                'http://drive.google.com/viewerng/viewer?embedded=true&url=http://www.africau.edu/images/default/sample.pdf&toolbar=0' :
            url('preview?url='.Storage::url($this->name));
    }
    public function getMimeAttribute(){
        return Storage::mimeType($this->name);
    }
    public function path(){
        return 'uploads/'.$this->name;
    }
}
