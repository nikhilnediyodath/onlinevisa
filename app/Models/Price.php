<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

class Price extends Model
{
    protected $with = ['citizenship', 'residence', 'visatype'];
    protected $guarded = ['created_at', 'updated_at'];

    public static function getPrice(Request $request){
        $prices = Price::where([
                ['visa_type_id', $request->visa_type],
                ['entry_type', $request->entry_type],
                ['citizenship_id', $request->citizenship ?: '*'],
                ['residence_id', $request->residence ?: '*']
            ])->get();
        if ($prices->count() == 1) {
            return $prices->first();
        } else {
            $prices = Price::where([
                    ['visa_type_id', $request->visa_type],
                    [config('app.priority.price.first'), $request->{config('app.priority.request.first')}],
                    [config('app.priority.price.second'), $request->{config('app.priority.request.second')}],
                    [config('app.priority.price.third'), '*']
                ])->get();
            if ($prices->count() == 1) {
                return $prices->first();
            } else {
                $prices = Price::where([
                        ['visa_type_id', $request->visa_type],
                        [config('app.priority.price.first'), $request->{config('app.priority.request.first')}],
                        [config('app.priority.price.second'), '*'],
                        [config('app.priority.price.third'), '*']
                    ])->get();
                if ($prices->count() == 1) {
                    return $prices->first();
                } else {
                    $prices = Price::where([
                            ['visa_type_id', $request->visa_type],
                            [config('app.priority.price.first'), '*'],
                            [config('app.priority.price.second'), '*'],
                            [config('app.priority.price.third'), '*']
                        ])->get();
                    if ($prices->count() == 1) {
                        return $prices->first();
                    }
                }
            }
        }
    }

    // Relations
    public function citizenship(){
        return $this->belongsTo('App\Models\Country', 'citizenship_id');
    }
    public function residence(){
        return $this->belongsTo('App\Models\Country', 'residence_id');
    }
    public function visatype(){
        return $this->belongsTo('App\Models\VisaType', 'visa_type_id');
    }
}
