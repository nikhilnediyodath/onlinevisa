<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $guarded = ['created_at', 'updated_at', 'constraints', 'count'];

    protected $appends = ['constraints', 'count'];

    // Accessors
    public function getCountAttribute(){
        return $this->applications()->count();
    }

    public function scopeActive($query){
        return $query->where('active', 'Active');
    }

    public function getConstraintsAttribute(){
        $arr = array();
        $this->minimum_amt? $arr[] = 'Minimum amt: '.$this->minimum_amt : NULL ;
        $this->maximum_amt? $arr[] = 'Maximum amt: '.$this->maximum_amt : NULL ;
        $this->limit? $arr[] = 'Limit: '.$this->limit : NULL ;
        return implode(', ', $arr);
    }

    public function apply($price){
        $result = 0;
        if ($this->discount_type === 'Fixed Amount')
            $result = $price - $this->amount;
        elseif ($this->discount_type === 'Percentage')
            $result = $price - ($price * ($this->amount/100));
            
        if ($result < 0) return 0;
        return $result;
    }

    // Relations
    public function applications(){
        return $this->hasMany('App\Models\Application');
    }
}
