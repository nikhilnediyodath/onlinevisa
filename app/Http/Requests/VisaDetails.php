<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use DatePeriod;
use DateInterval;

class VisaDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'numeric|nullable',
            'application_id' => 'numeric|nullable',
            'arrival_date' => 'bail|required|date|after:today',
            'citizenship' => 'required|exists:countries,code',
            'residence' => 'required|exists:countries,code',
            'visa_type' => 'required|exists:visa_types,id',
            'entry_type' => 'required',
            'delivery_type' => 'required',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $working_days = $this->delivery_type == "Urgent Delivery" ? config('app.process_days.urgent') : config('app.process_days.normal') ;
            $delivery_date = Carbon::now()->add($working_days, 'day')->endOfDay();
            $total_days = $working_days;

            foreach (new DatePeriod(Carbon::now(), new DateInterval('P1D'), $delivery_date) as $day)
                if ($day->format("N") == 5)// If Friday
                    $total_days += 2;
            
            if (Carbon::create($this->arrival_date)->lessThanOrEqualTo(Carbon::now()->add($total_days, 'day')->endOfDay())) {
                $validator->errors()->add('arrival_date', ($working_days+1).' working days needed for '.strtolower($this->delivery_type));
            }
        });
        // $this->merge(['arrival_date' => 'fdsfds']);
    }
}
