<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_image.id' => 'required|numeric',
            'national_id.id' =>   'required_if:citizenship,KW|
                                required_if:citizenship,IQ|
                                required_if:citizenship,IR|
                                numeric|nullable',
            'first_name' => 'required|alpha_space',
            'middle_name' => 'nullable|alpha_space',
            'last_name' => 'nullable|alpha_space',
            'religion' => 'required|string|alpha_space',
            'date_of_birth' => 'required|date|before_or_equal:today',
            'place_of_birth' => 'required|string|alpha_space',
            'mother_name' => 'required|string|alpha_space',
            'father_name' => 'required|string|alpha_space',
            'marital_status' => 'required|string',
            'spouse_name' => 'required_if:marital_status,Married|string|alpha_space|nullable',
            'first_language' => 'required|exists:languages,id',
            'second_language' => 'required|exists:languages,id',
        ];
    }
}
