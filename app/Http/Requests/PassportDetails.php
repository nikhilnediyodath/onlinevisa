<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class PassportDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'passport_first_page.id' => 'required|numeric',
            'passport_last_page.id' => 'required|numeric',
            'passport_number' => 'required|string|alpha_num',
            'passport_issue_date' => 'required|date|before_or_equal:today',
            'passport_expiry_date' => 'required|date|after:6 months',
            'passport_issue_place' => 'required|string|alpha_space',
        ];
    }

    
    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // Check is minor
            if (Carbon::parse($this->date_of_birth)->age < 18 && (!isset($this->attachments) || sizeof($this->attachments)<2))
                $validator->errors()->add('attachments', "Parent's passport and visa are required");
        });
    }
}
