<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Application;
use App\Models\Price;
use App\Models\User;
use App\Models\Payment;
use App\Models\Visa;
use App\Models\File;
use App\Models\VisaType;

use App\Notifications\EmailVerification;
use App\Notifications\ApplicationReceived;
use Notification;
use Carbon\Carbon;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PaymentsExport;
use Mail;
use App\Http\Controllers\Api\Foloosi;

class TestController extends Controller
{
    public function index(Request $request){
//        return (new App\Notifications\Template(Application::find(1), 1, 'comment'))->toMail('nikhilnediyodath@gmail.com');
//        Notification::send(User::where('is_admin', true)->get(), new App\Notifications\Template(Application::find(1), 9));
        return Application::all();
    }
    public function store(Request $request){
        dd($request);
    }

    public function payment(){
        $client = new Client([
            'headers' => [
                'merchant_key'  =>  config('foloosi.merchant_key'),
                'secret_key'    =>  config('foloosi.secret_key'),
            ]
        ]);

        // Transaction Details --- Tested OK
        $response = $client->get('http://foloosi.com/api/v1/api/transaction-detail/FOICPM66101570086750')->getBody()->getContents();
        $result = json_decode($response, true);

        // Transaction List --- Tested OK
        // $response = $client->get('https://foloosi.com/api/v1/api/transaction-list')->getBody()->getContents();
        // $result = json_decode($response, true);

        // Initialize Payment
        // $response = $client->post('https://foloosi.com/api/v1/api/initialize-setup', [
        //     'form_params' => [
        //         'redirect_url'          => 'http://127.0.0.1:8000/payment',
        //         'transaction_amount'    => 199,
        //         'currency'              => 'AED',
        //         'customer_name'         => 'Nikhil'
        //     ]
        // ]);
        // $data = json_decode($response->getBody()->getContents(), true);
        // return $data['data']['reference_token'];

        dump($result);
    }
    public function paymentPage(){
        return view('foloosi');
    }
    public function show(){
        return view('test');
    }

    public function paymentResponse(Request $request){
        $this->log(json_encode($request->toArray(), true));
    }
    public function demo(){
        return view('demo');
    }

    public function preview(Request $request){
        return view('utils.preview')->with('url', $request->url);
    }
}
