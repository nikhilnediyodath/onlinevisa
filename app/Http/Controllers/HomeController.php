<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function promocodes(){
        return view('promocodes');
    }
    public function staff(){
        if (!Auth::user()->is_admin) abort(401);
        return view('staff');
    }
    public function dashboard(){
        return view('dashboard');
    }
    public function settings(){
        return view('settings');
    }
    public function pricing(){
        return view('pricing');
    }
    public function payments(){
        return view('payments');
    }
    public function paymentReport(){
        return view('payment_report');
    }
    public function applicationReport(){
        return view('application_report');
    }
    public function applications(){
        return view('applications');
    }
    public function summary(){
        return view('summary');
    }
}
