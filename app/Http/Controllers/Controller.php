<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function log($text = ""){
        fwrite(fopen("logs/log.log", "a"), Carbon::now()->format('Y-m-d H:i:s.u')." ".$text."\n");
    }

    protected function validationError($field='name', $error='This field is invalid.', $message='The given data was invalid.'){
        return response()->json([
            'message' => $message,
            'errors' => [
                $field => [$error]
            ]
        ], 422);
    }
}
