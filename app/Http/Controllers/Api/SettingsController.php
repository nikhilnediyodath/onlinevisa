<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VisaType;
use App\Models\Price;
use App\Models\Settings;

class SettingsController extends Controller
{
    public function getSettingsData(){
        return [   
            'visa_types' => VisaType::all(),
            'counters' => Settings::whereIn('id', [1,2,3,4])->get(),
        ];
    }

    public function saveVisaType(Request $request){
        $request->validate([
            'id' => 'numeric|nullable',
            'name' => 'required|string|unique:visa_types,name',
            'status' => 'string',
            'multiple_entry_amount' => 'numeric|required_without:id',
            'single_entry_amount' => 'numeric|required_without:id'
        ]);
        $visa_type = VisaType::findOrNew($request->id);
        $visa_type->fill($request->all());
        $visa_type->save();

        if (!$request->id) {
            Price::create([
                'visa_type_id' => $visa_type->id,
                'entry_type' => 'Single Entry',
                'amount' => $request->single_entry_amount,
            ]);            
            Price::create([
                'visa_type_id' => $visa_type->id,
                'entry_type' => 'Multiple Entry',
                'amount' => $request->multiple_entry_amount,
            ]);
        }
        return VisaType::all();
    }

    public function deleteVisaType(Request $request, $id){
        $visa_type = VisaType::find($id);
        if ($visa_type->count > 0) abort(401);
        $visa_type->delete();       
        return VisaType::all();;
    }

    public function updateSetting(Request $request){
        $request->validate([
            'id' => 'numeric|nullable',
            'name' => 'required|string',
            'value' => 'string',
        ]);
        $setting = Settings::find($request->id);
        $setting->fill($request->all());
        $setting->save();
        return Settings::whereIn('id', [1,2,3,4])->get();
    }
}
