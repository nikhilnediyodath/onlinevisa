<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;

class Foloosi{

    public function __construct() {
        if (config('foloosi.debug')) {
            $this->merchant_key =   config('foloosi.test.merchant_key');
            $this->secret_key   =    config('foloosi.test.secret_key');
        } else {
            $this->merchant_key =   config('foloosi.prod.merchant_key');
            $this->secret_key   =   config('foloosi.prod.secret_key');
        }
        $this->client = new Client([
            'headers' => [
                'merchant_key'  =>  $this->merchant_key,
                'secret_key'    =>  $this->secret_key,
            ]
        ]);
    }

    public function getTransactionDetails($transaction_id){
        $response = $this->client->get('http://foloosi.com/api/v1/api/transaction-detail/'.$transaction_id)->getBody()->getContents();
        $data = json_decode($response, true);
        if ($data['message'] === 'Transaction detail are') {
            return $data['data'];
        }
        return false;
    }

    public function getTransactionList(){
        $response = $this->client->get('https://foloosi.com/api/v1/api/transaction-list')->getBody()->getContents();
        $data = json_decode($response, true);
        if ($data['message'] === 'Transaction') {
            return $data['data']['transactions'];
        }
        return false;
    }

    public function initPayment($request){
        $response = $this->client->post('https://foloosi.com/api/v1/api/initialize-setup', [
            'form_params' => [
                'redirect_url'          =>  'http://127.0.0.1:8000/payment',
                'transaction_amount'    =>  $request['amount'],
                'currency'              =>  config('foloosi.currency'),
                'customer_name'         =>  isset($request['customer_name'])? $request['customer_name'] : '',
                'customer_email'        =>  isset($request['customer_email'])? $request['customer_email'] : '',
                'customer_mobile'       =>  isset($request['customer_mobile'])? $request['customer_mobile'] : '',
                'customer_address'      =>  isset($request['customer_mobile'])? $request['customer_address'] : '',
                'customer_city'         =>  isset($request['customer_city'])? $request['customer_city'] : '',
                'optional1'             =>  isset($request['optional1'])? $request['optional1'] : '',
            ]
        ]);
        $data = json_decode($response->getBody()->getContents(), true);
        if ($data['message'] === 'Application setup successfully') {
            $result['reference_token'] = $data['data']['reference_token'];
            $result['merchant_key'] = $this->merchant_key;
            return $result;
        }
        return false;
    }
}