<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Application;
use App\Models\Payment;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ArrayExport;

class ReportsController extends Controller
{
    public function reportsData(Request $request){
        return [
            'summary' => Application::summary(),
        ];
    }

    public function getPaymentReport(Request $request){
        return [
            'payment' => Payment::report($request),
        ];
    }
    public function getApplicationReport(Request $request){
        return [
            'payment' => Application::report($request),
        ];
    }

    public function exportPaymentReport(Request $request){
        $request->validate([
            'year' => 'numeric|nullable',
            'month' => 'numeric|nullable',
        ]);
        return Excel::download(new ArrayExport(Payment::report($request)), 'payments.xlsx');
    }
    public function exportApplicationReport(Request $request){
        $request->validate([
            'year' => 'numeric|nullable',
            'month' => 'numeric|nullable',
        ]);
        // dd(Application::report($request));return;
        return Excel::download(new ArrayExport(Application::report($request)), 'applications.xlsx');
    }
}
