<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Promocode;

class PromocodeController extends Controller
{
    public function index(){
        return Promocode::all();
    }

    public function store(Request $request){
        $request->validate([
            'id' => 'numeric|nullable',
            'name' => 'required',
            'discount_type' => 'bail|required',
            'amount' => 'required|numeric'
        ]);
        $promocode = Promocode::findOrNew($request->id);
        $promocode->fill($request->all());
        $promocode->save();
        return Promocode::all();
    }
    public function deletePromocode(Request $request, $id){
        $promocode = Promocode::find($id); 
        $promocode->delete();       
        return Promocode::all();;
    }
}
