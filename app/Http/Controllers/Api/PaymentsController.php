<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Payment;
use App\Models\Application;

class PaymentsController extends Controller
{
    public function index(){
        return [
            'payments' => Payment::with('application')->get(),
            'data'  =>  [
                'income'    =>  Payment::where('status', 'success')->sum('customer_amount'),
                'refunded'    =>  Payment::where('status', 'refunded')->sum('customer_amount'),
            ]
        ];
    }

    public function store(Request $request){
        $request->validate([
            'id' => 'nullable|numeric',
            'application_id' => 'required|numeric',
            'payment_transaction_id' => 'nullable|numeric',
            'transaction_no' => 'nullable|alpha_num',
            'request_amount' => 'nullable|numeric',
            'merchant_amount' => 'nullable|numeric',
            'customer_amount' => 'nullable|numeric',
            'status' => 'nullable|alpha',
        ]);
        $payment = Payment::findOrNew($request->id);
        $payment->fill($request->all());
        $payment->save();
        return [
            'application' => $payment->application()->with('payment')->first(),
            'applications' => Application::lifo()->with('payment')->get()
        ];
    }
}
