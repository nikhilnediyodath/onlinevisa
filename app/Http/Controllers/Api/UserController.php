<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
class UserController extends Controller
{
    public function index(){
        return User::all();
    }

    public function store(Request $request){

        $request->validate([
            'id' => 'numeric|nullable',
            'name' => 'required',
            'gender' => 'bail|required',
            'email' => 'bail|required',
            'designation' => 'bail|required',
            'phone' => 'bail|required',
        ]);
        $user = User::findOrNew($request->id);
        $user->fill($request->all());
        $user->is_admin = ($request->designation == 'Admin');
        $user->save();
        return User::all();
    }

    public function destroy(Request $request, $id){
        $user = User::find($id);
        if (User::where('is_admin', true)->count() == 1 && $user->is_admin) abort(401);
        $user->delete();
        return User::all();
    }
}