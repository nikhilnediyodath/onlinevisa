<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Price;
use App\Models\File;
use App\Models\Application;
use App\Models\Payment;
use App\Models\Visa;

use Mail;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Builder;;

class GeneralController extends Controller
{
    public function getPricing(){
        return Price::all();
    }
    public function savePrice(Request $request){
        $request->validate([
            'id' => 'numeric|nullable',
            'residence' => 'bail|required',
            'citizenship' => 'bail|required',
            'entry_type' => 'bail|required',
            'visa_type' => 'bail|required',
            'amount'    => 'bail|required|numeric',
        ]);

        if (($request->{config('app.priority.request.second')}!=='*') && ($request->{config('app.priority.request.first')}==='*')) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    config('app.priority.request.first') => ['The '.config('app.priority.message.first').' field is required with '.config('app.priority.message.second').'.']
                ]
            ], 422);
        }
        if (($request->{config('app.priority.request.third')}!=='*') && ($request->{config('app.priority.request.second')}==='*')) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    config('app.priority.request.second') => ['The '.config('app.priority.message.second').' field is required with '.config('app.priority.message.third').'.']
                ]
            ], 422);
        }

        if ($request->id) {
            $count = Price::where([
                ['visa_type_id', $request->visa_type],
                [ 'citizenship_id', $request->citizenship],
                [ 'residence_id', $request->residence],
                [ 'entry_type', $request->entry_type]
            ])->count();

            if ($count > 0 ) abort(401);
            $price = Price::find($request->id);
            $price->visa_type_id = $request->visa_type;
            $price->citizenship_id = $request->citizenship;
            $price->residence_id = $request->residence;
            $price->entry_type = $request->entry_type;
            $price->amount = $request->amount;
            $price->save();
        } else {
            Price::updateOrCreate([
                'visa_type_id' => $request->visa_type,
                'citizenship_id' => $request->citizenship,
                'residence_id' => $request->residence,
                'entry_type' => $request->entry_type,
            ],[
                'amount' => $request->amount,
            ]);
        }
        return Price::all();
    }
    public function deletePrice(Request $request, $id){
        $price = Price::find($id);
        if (($price->citizenship_id === '*') && ($price->residence_id === '*')) abort(401);
        $price->delete();
        return Price::all();
    }
    public function fileUpload(Request $request){
        $request->validate([
            'file' => 'required|file|mimes:jpeg,bmp,png,pdf|min:20|max:8192',
            'file_type' => 'required|alpha_dash',
            'doc_type' => 'nullable|alpha_space',
        ]);
        $file = new File;
        $file->fill($request->all());
        $file->name = $request->file->store('files');
        $file->save();
        return $file;
        // return File::create(['name' => $request->file->store('files'), 'file_type' => $request->file_type, 'doc_type' => $request->doc_type]);
    }
    public function attachFile(Request $request){
        // return $request;
        $request->validate([
            'application_id' => 'required|numeric',
            'file_id' => 'required|numeric',
        ]);
        $application = Application::find($request->application_id);
        $application->attachments()->save(File::find($request->file_id));
        return $application->fresh();
    }
    public function deleteFile(Request $request, $id){
        $file = File::find($id);
        $application = $file->application;        
        // $act_file = public_path('uploads\files\\'.$file->name);
        // if (file_exists($act_file)) unlink($act_file);
        $file->delete();
        return $application->fresh();
    }
    public function dashboardData(){
        $global_sales_citi = DB::select("SELECT SUM(`price`) as cost, countries.name, visas.citizenship 
            FROM visas 
                INNER JOIN countries ON visas.citizenship=countries.code 
                INNER JOIN applications ON visas.application_id=applications.id 
                INNER JOIN payments ON payments.application_id=applications.id 
            WHERE payments.status='success'
            GROUP BY countries.name, visas.citizenship");

        $global_sales_resi = DB::select("SELECT SUM(`price`) as cost, countries.name, visas.residence 
            FROM visas 
                INNER JOIN countries ON visas.residence=countries.code 
                INNER JOIN applications ON visas.application_id=applications.id 
                INNER JOIN payments ON payments.application_id=applications.id 
            WHERE payments.status='success'
            GROUP BY countries.name, visas.residence");
        $total_sales = DB::select("SELECT SUM(`price`) as cost
            FROM visas 
                INNER JOIN countries ON visas.citizenship=countries.code 
                INNER JOIN applications ON visas.application_id=applications.id 
                INNER JOIN payments ON payments.application_id=applications.id 
            WHERE payments.status='success'");
        return [
            'total_applications' => Application::all()->count(),
            'approved_applications' => Application::where('status', 'Approved')->count(),
            'revenue' => Payment::where('status', 'success')->sum('customer_amount'),
            'approved_visas' => Visa::whereHas('application', function (Builder $query) {
                    $query->where('status', 'Approved');
                })->count(),
            'total_visas' => Visa::all()->count(),
            'promocodes' => Application::whereHas('promocode')->count(),
            'total_sales' => $total_sales[0]->cost,
            'global_sales_citi' => $global_sales_citi,
            'global_sales_resi' => $global_sales_resi,
        ];
    }
    public function sendContactMessage(Request $request){
        $request->validate([
            'name' => 'required|alpha',
            'subject' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string',
        ]);
        $message = "You received a contact message from $request->name : $request->email \n $request->message ";
        Mail::raw($message , function ($message) use ($request){
            $message->to(config('app.email'), config('app.name'));
            $message->subject($request->subject);
        });
    }
}