<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Application;
use App\Notifications\ApplicationResponse;
use App\Notifications\Template;

class ApplicationsController extends Controller
{
    public function index(Request $request){
        // return $request;
        return [
            'applications' => Application::lifo()->where('status','like', $request->status)->with('payment')->get(),
            'data'  =>  [
                'processing' => Application::processing()->count(),
                'rejected' => Application::rejected()->count(),
                'approved' => Application::approved()->count(),
                'suspended' => Application::suspended()->count(),
            ]
        ];
    }

    public function store(Request $request){
        $request->validate([
            'id' => 'required|numeric',
            'message' => 'nullable|string',
            'comment' => 'nullable|string',
            'send_mail' => 'nullable|boolean'
        ]);
        $application = Application::find($request->id);
        $application->message = $request->message;
        $application->comment = $request->comment;
        $application->status = $request->status;
        $application->save();
        if ($request->status == 'Refunded') {
            $payment = $application->payment;
            $payment->status = 'refunded';
            $payment->save();
        }
        if ($request->send_mail) {
            $application->notify(new ApplicationResponse($application));
        }
        // return $application->fresh();
        return [
            'application' => $application->fresh(),
            'applications' => Application::lifo()->with('payment')->get()
        ];
    }

    public function destroy(Request $request, $id){
        $application = Application::find($id);
        $application->payment()->delete();
        $application->visas()->delete();
        $application->attachments()->delete();
        $application->delete();
        return Application::all();;
    }
    public function sendNotification(Request $request, $id){
        $application = Application::find($id);
        $application->notify(new Template($application, $request->template, $request->comments));
        $application->status_code = $request->template;
        $application->save();
        return $application->fresh();
    }
}
