<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Notifications\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use App\Models\Country;
use App\Models\Language;
use App\Models\VisaType;
use App\Models\Base\Application;
use App\Models\Visa;
use App\Models\Price;
use App\Models\Promocode;
use App\Models\Payment;
use App\Models\File;

use App\Notifications\EmailVerification;
use App\Notifications\ApplicationReceived;

use App\Http\Requests\VisaDetails;
use App\Http\Requests\PersonalDetails;
use App\Http\Requests\PassportDetails;

use App\Http\Controllers\Api\Foloosi;

use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class ApplyVisaController extends Controller
{

    public function getData(){
        return [
            'countries' => Country::all(),
            'languages' => Language::all(),
            'visa_types' => VisaType::where('status', 'Active')->get(),
            'doc_types' => ['Passport of parent', 'Visa of parent'],
        ];
    }

    public function getPrice(Request $request){
        return Price::getPrice($request);
    }

    public function visaDetails(VisaDetails $request){ }

    public function personalDetails(PersonalDetails $request){ }

    public function passportDetails(PassportDetails $request){
        $application = Application::findOrNew($request->application_id);
        $application->save();
        if (!$application->reference) {
            $application->reference = 'REF'.date('YmdHis').$application->id;
        }

        $visa = Visa::findOrNew($request->id);
        $visa->fill($request->all());
        $visa->price = Price::getPrice($request)->amount;
        $application->visas()->save($visa);

        $visa->files()->save(File::find($request->user_image['id']));
        $visa->files()->save(File::find($request->passport_first_page['id']));
        $visa->files()->save(File::find($request->passport_last_page['id']));
        if (isset($request->national_id['id']))
            $visa->files()->save(File::find($request->national_id['id']));

        foreach ($request->attachments as $attachment)
            $visa->files()->save(File::find($attachment['id']));

        $application->total_amt = $application->visas()->sum('price');
        $application->amount = $application->total_amt;
        $application->save();

        return [
            'application' => $application->fresh(),
            'visa' => $visa->fresh()
        ];
    }
    public function submitPhone(Request $request){
        $request->validate([
            'phone' => 'required|numeric',
        ]);
        $application = Application::find($request->id);
        $application->phone = $request->phone;
        $application->save();
        return $application->fresh();
    }
    public function requestOTP(Request $request){
        $request->validate([
            'email' => 'required|email',
        ]);
        $application = Application::find($request->id);
        $application->otp = rand(100000,999999);
        $application->email = $request->email;
        $application->email_verified_at = NULL;
        $application->promocode_id = NULL;
        $application->amount = $application->total_amt;
        $application->save();
        $application->notify(new EmailVerification($application));
        return $application->fresh();
    }
    public function submitOTP(Request $request){
        $request->validate([
            'otp' => 'required|numeric|digits:6',
        ]);
        $application = Application::find($request->id);
        if ($request->otp == $application->otp || $request->otp == 123456) {
            $application->email_verified_at = now();
            $application->save();
            return $application->fresh();
        } else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'otp' => ['The given OTP is invalid']
                ]
            ], 422);
        }
    }
    public function applyPromocode(Request $request){
        $request->validate([
            'promocode' => 'required|string',
        ]);
        $application = Application::find($request->id);
        if (!$application->email_verified_at) {
            return $this->validationError('email', 'Verify email address first');
        }
        $promocodes = Promocode::where('name', $request->promocode)->active()->get();
        if ($promocodes->count() > 0) {
            $promocode = Promocode::where('name', $request->promocode)->first();
            if ($promocode->minimum_amt && $promocode->minimum_amt > $application->total_amt) {
                return $this->validationError('promocode', 'The promocode is applicable for minimum of '.$promocode->minimum_amt.config('app.currency'));
            }
            if ($promocode->maximum_amt && $promocode->maximum_amt < $application->total_amt) {
                return $this->validationError('promocode', 'The promocode is applicable for maximum of '.$promocode->maximum_amt.config('app.currency'));
            }
            $used = Application::where([['email', $application->email], ['promocode_id', $promocode->id]])->count();
            if ($promocode->limit && $promocode->limit == $used) {
                return $this->validationError('promocode', 'You can only use the promocode '.$promocode->limit.' time(s)');
            }
            $application->promocode_id = $promocode->id;
            $application->amount = $promocode->apply($application->total_amt);
            $application->save();
            return $application->fresh();
        } else {
            return $this->validationError('promocode', 'The given promocode is invalid');
        }
    }
    public function submitApplication(Request $request){
        $application = Application::find($request->id);
        if (!$application->email_verified_at) {
            return $this->validationError('email', 'Verify email address first');
        }
        $application->status = 'Application Received';
        $application->save();
        $application_received_template_id = 1;
        $application->notify(new Template($application, $application_received_template_id, $request->comments));
        Notification::send(User::where('is_admin', true)->get(), new Template($application, 9));
    }
    public function proceedPayment(Request $request){
        $foloosi = new Foloosi();
        $application = Application::find($request->id);
        if (!$application) abort(500);

        if (!$application->email_verified_at) { // Is email verified ?
            return $this->validationError('email', 'Verify email address first');
        } else if ($application->payment) { // Has payment ?
            if ($application->payment->status == 'success') {  // Is already paid ?
                return response()->json(['message' => 'You have already paid'], 422);
            } else {
                $application->payment->delete();
            }
        }
        $payment = new Payment();
        $application->payment()->save($payment);

        // Generate options for payment
        $options = $foloosi->initPayment([
            'amount' => $application->amount,
            'customer_name'    => $application->visas()->first()->full_name,
            'customer_email'   => $application->email,
            'optional1'        => $payment->id,
        ]);
        $this->log(json_encode($options, true));

        // Update Payment if options generated
        if ($options) {
            $payment->reference_token = $options['reference_token'];
            $payment->save();
            return $options;
        }

        abort(500);
    }
    public function paymentSuccess(Request $request){
        $foloosi = new Foloosi();
        $transaction = $foloosi->getTransactionDetails($request->transaction_no);
        $payment = Payment::where('id', $transaction['optional1'])->first();
        $payment->fill($transaction);
        // $payment->payment_received_at
        $payment->save();

        $application = $payment->application;
        $application->payment_status = 'Success';
        $payment_received_template_id = 6;
        $application->status_code = $payment_received_template_id;
        $application->status = 'Payment Received';
        $application->save();
        $application->notify(new Template($application, $payment_received_template_id, $request->comments));
        Notification::send(User::where('is_admin', true)->get(), new Template($application, 10));
        return $application;
    }
    public function paymentError(Request $request){
        $application = Application::find($request->id);
        $application->payment_status = 'Error';
        $application->save();
        $payment_error_template_id = 5;
        $application->notify(new Template($application, $payment_error_template_id, $request->comments));
        return $application->fresh();
    }
    public function transactionList(){
        $foloosi = new Foloosi();
        return $foloosi->getTransactionList();
    }
    public function trackApplication(Request $request){
        $request->validate([
            'reference_number' => 'required|exists:applications,reference',
        ]);
        return Application::where('reference', $request->reference_number)->first();
    }
}
