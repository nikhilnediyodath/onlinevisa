<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Models\Application;
use App\Models\MailTemplate;
use Illuminate\Support\HtmlString;

class Template extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($application, $template_id, $comments = null)
    {
        $this->application = $application;
        $this->template = MailTemplate::find($template_id);
        $this->comments = $comments;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $text = $this->template->text;

        if ($this->application->payment) {
            $text = preg_replace('/\bTRANSACTION_NO\b/', $this->application->payment->transaction_no, $text);
            $text = preg_replace('/\bPAYMENT_TRANSACTION_ID\b/', $this->application->payment->payment_transaction_id, $text);
            $text = preg_replace('/\bREQUEST_AMOUNT\b/', $this->application->payment->request_amount, $text);
            $text = preg_replace('/\bCUSTOMER_AMOUNT\b/', $this->application->payment->customer_amount, $text);
            $text = preg_replace('/\bMERCHANT_AMOUNT\b/', $this->application->payment->merchant_amount, $text);
            $text = preg_replace('/\bPAYMENT_STATUS\b/', $this->application->payment->status, $text);
            $text = preg_replace('/\bPAYMENT_RECEIVED_AT\b/', $this->application->payment->payment_received_at, $text);
            $text = preg_replace('/\bPAYMENT_DETAILS_UPDATED_AT\b/', $this->application->payment->updated_at, $text);
        }

        $text = preg_replace('/\bAPPLICANT_NAME\b/', $this->application->applicant_name, $text );
        $text = preg_replace('/\bREFERENCE_NO\b/', $this->application->reference, $text );
        $text = preg_replace('/\bEMAIL_ADDRESS\b/', $this->application->email, $text );
        $text = preg_replace('/\bTOTAL_AMOUNT\b/', $this->application->	total_amt, $text );
        $text = preg_replace('/\bPAYABLE_AMOUNT\b/', $this->application->amount, $text );
        $text = preg_replace('/\bAPPLICATION_STATUS\b/', $this->application->status, $text );
        $text = preg_replace('/\bMESSAGE\b/', $this->application->message, $text );
        $text = preg_replace('/\bPAYMENT_REQUESTED_AT\b/', $this->application->payment_received_at, $text );
        $text = preg_replace('/\bCREATED_AT\b/', $this->application->created_at, $text );
        $text = preg_replace('/\bUPDATED_AT\b/', $this->application->updated_at, $text );

        $mail = (new MailMessage)
            ->subject($this->template->subject)
            ->greeting('Dear Sir/Madam,')
            ->line(new HtmlString($text));

        if ($this->comments) $mail->line($this->comments);

        if ($this->template->id == 4) $mail->action('Pay Now', url('pay?id='.$this->application->id));

        $mail->line(new HtmlString(MailTemplate::where('name', 'Signature')->first()->text));

        if ($this->template->id == 8)
            foreach($this->application->attachments as $file)
                $mail->attach($file->path());

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
