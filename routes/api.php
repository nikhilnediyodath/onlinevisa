<?php

use App\Http\Controllers\Api\ApplicationsController;
use App\Http\Controllers\Api\ApplyVisaController;
use App\Http\Controllers\Api\GeneralController;
use App\Http\Controllers\Api\PaymentsController;
use App\Http\Controllers\Api\PromocodeController;
use App\Http\Controllers\Api\ReportsController;
use App\Http\Controllers\Api\SettingsController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('test', [ApplyVisaController::class,'test']);

// Api\ApplyVisaController
Route::get('get_data', [ApplyVisaController::class,'getData']);
Route::post('get_price', [ApplyVisaController::class,'getPrice']);
Route::post('visa_details', [ApplyVisaController::class, 'visaDetails']);
Route::post('image_upload', [ApplyVisaController::class, 'imageUpload']);
Route::post('personal_details', [ApplyVisaController::class, 'personalDetails']);
Route::post('passport_details', [ApplyVisaController::class, 'passportDetails']);
Route::post('submit_phone', [ApplyVisaController::class, 'submitPhone']);
Route::post('request_otp', [ApplyVisaController::class, 'requestOTP']);
Route::post('submit_otp', [ApplyVisaController::class, 'submitOTP']);
Route::post('apply_promocode', [ApplyVisaController::class, 'applyPromocode']);
Route::post('submit_application', [ApplyVisaController::class, 'submitApplication']);
Route::post('proceed_payment', [ApplyVisaController::class, 'proceedPayment']);
Route::post('payment_success', [ApplyVisaController::class, 'paymentSuccess']);
Route::post('payment_error', [ApplyVisaController::class, 'paymentError']);
Route::get('transactions', [ApplyVisaController::class, 'transactionList']);
Route::post('track_application', [ApplyVisaController::class, 'trackApplication']);


// ApplicationsController
Route::get('applications', [ApplicationsController::class, 'index']);
Route::post('applications', [ApplicationsController::class, 'store']);
Route::delete('application/{id}', [ApplicationsController::class, 'destroy']);
Route::post('send_notification/{id}', [ApplicationsController::class, 'sendNotification']);

// PromocodeController
Route::get('promocodes', [PromocodeController::class, 'index']);
Route::post('promocodes', [PromocodeController::class, 'store']);
Route::delete('promocode/{id}', [PromocodeController::class, 'deletePromocode']);

// UserController
Route::get('users', [UserController::class, 'index']);
Route::post('user', [UserController::class, 'store']);
Route::delete('users/{id}', [UserController::class, 'destroy']);

// GeneralController
Route::get('pricing', [GeneralController::class, 'getPricing']);
Route::post('pricing', [GeneralController::class, 'savePrice']);
Route::delete('pricing/{id}', [GeneralController::class, 'deletePrice']);
Route::post('file_upload', [GeneralController::class, 'fileUpload']);
Route::post('attach_file', [GeneralController::class, 'attachFile']);
Route::delete('file/{id}', [GeneralController::class, 'deleteFile']);
Route::get('dashboard_data', [GeneralController::class, 'dashboardData']);
Route::post('contact', [GeneralController::class, 'sendContactMessage']);

// SettingsController
Route::get('get_settings_data', [SettingsController::class, 'getSettingsData']);
Route::post('visa_types', [SettingsController::class, 'saveVisaType']);
Route::delete('visa_type/{id}', [SettingsController::class, 'deleteVisaType']);
Route::put('setting', [SettingsController::class, 'updateSetting']);

// Api\PaymentsController
Route::get('payments', [PaymentsController::class, 'index']);
Route::post('payments', [PaymentsController::class, 'store']);

// Api\ReportsController
Route::get('reports_data', [ReportsController::class, 'reportsData']);
Route::get('payment_report', [ReportsController::class, 'getPaymentReport']);
Route::get('application_report', [ReportsController::class, 'getApplicationReport']);
