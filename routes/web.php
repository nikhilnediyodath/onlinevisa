<?php

use App\Http\Controllers\Api\ReportsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); });
Route::get('about', function () { return view('about'); });
Route::get('faq', function () { return view('faq'); });
Route::get('terms', function () { return view('terms'); });
Route::get('get_in_touch', function () { return view('contact'); });
Route::get('contact2', function () { return view('contact2'); });
Route::get('privacy-policy', function () { return view('privacy-policy'); });
Route::get('terms-conditions', function () { return view('terms_conditions'); });
Route::get('30-days-visa', function () { return view('30_days_visa'); });
Route::get('90-days-visa', function () { return view('90_days_visa'); });
Route::get('family-visa', function () { return view('family_visa'); });
Route::get('visa-agency-abu-dhabi', function () { return view('abu_dhabi_visa'); });
Route::get('home', function () { return view('home_page'); });
Route::get('pay', function () { return view('welcome'); });

Auth::routes(['register' => false]);

Route::get('promocodes', [HomeController::class, 'promocodes']);
Route::get('staff', [HomeController::class, 'staff']);
Route::get('pricing', [HomeController::class, 'pricing']);
Route::get('notifications', [HomeController::class, 'notifications']);
Route::get('dashboard', [HomeController::class, 'dashboard']);
Route::get('settings', [HomeController::class, 'settings']);
Route::get('payments', [HomeController::class, 'payments']);
Route::get('payment_reports', [HomeController::class, 'paymentReport']);
Route::get('application_reports', [HomeController::class, 'applicationReport']);
Route::get('applications', [HomeController::class, 'applications']);
Route::get('summary', [HomeController::class, 'summary']);
Route::resource('mail_templates', 'MailTemplatesController');
// Api\ReportsController
// Route::get('reports/payment/{year}/{month}', 'Api\ReportsController@exportPaymentReport']);
Route::get('reports/payment', [ReportsController::class,'exportPaymentReport']);
Route::get('reports/application', [ReportsController::class,'exportApplicationReport']);

// TestController
Route::get('test', [TestController::class, 'index']);
Route::post('test', [TestController::class, 'store']);
Route::get('test_view',  function () { return view('test'); });
Route::get('demo', [TestController::class, 'demo']);
Route::get('payment', [TestController::class, 'payment']);
Route::get('payment_page', [TestController::class, 'paymentPage']);
Route::get('payment_response', [TestController::class, 'paymentResponse']);
Route::post('payment_response', [TestController::class, 'paymentResponse']);

// Important
Route::get('preview', [TestController::class, 'preview']);

Route::get('{view}',  function ($view) { return view($view); });
