<?php

return [

    'test' => [
        'merchant_key'  =>  env('FOLOOSI_MERCHANT_ID_TEST', ''),
        'secret_key'    =>  env('FOLOOSI_SECRET_KEY_TEST', ''),
    ],

    'prod' => [
        'merchant_key'  =>  env('FOLOOSI_MERCHANT_ID_PROD', ''),
        'secret_key'    =>  env('FOLOOSI_SECRET_KEY_PROD', ''),
    ],

    'debug'         =>  env('FOLOOSI_DEBUG', true),
    
    'currency'      => env('FOLOOSI_CURRENCY', 'AED'),

    'refund_url'    => 'https://www.foloosi.com/merchant/transactions/details/',
];