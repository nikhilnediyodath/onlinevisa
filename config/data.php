<?php

return [

    'date_format' => 'd-m-Y g:i A',

    'application_status' => ['Application Received', 'Processing', 'Pending', 'Payment Requested', 'Payment Received', 'Approved', 'Rejected'],

];