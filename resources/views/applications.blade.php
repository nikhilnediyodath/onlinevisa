@extends('layouts.admin')

@push('css')
<link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
@endpush

@section('content')
<div class="row">

    {{-- @include('applications.visa_details') --}}
    @include('applications.application_wizard')

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Applications</h4>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="form-group">
                        
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_" ng-model="filter.status" value="%" ng-change="init()">
                            <label for="filter_" class="custom-control-label">All</label>
                        </div>
                        @foreach (config('data.application_status') as $status)
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="filter_{{ $status }}" ng-model="filter.status" value="{{ $status }}" ng-change="init()">
                                <label for="filter_{{ $status }}" class="custom-control-label">{{ $status }}</label>
                            </div>
                        @endforeach
                        {{-- <div class="custom-control custom-radio  custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_all"
                                ng-model="filter.status" value="%" ng-change="init()">
                            <label for="filter_all" class="custom-control-label">All</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_processing"
                                ng-model="filter.status" value="Processing" ng-change="init()">
                            <label for="filter_processing" class="custom-control-label">Processing</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_rejected"
                                ng-model="filter.status" value="Rejected" ng-change="init()">
                            <label for="filter_rejected" class="custom-control-label">Rejected</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_suspended"
                                ng-model="filter.status" value="Suspended" ng-change="init()">
                            <label for="filter_suspended" class="custom-control-label">Suspended</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_approved"
                                ng-model="filter.status" value="Approved" ng-change="init()">
                            <label for="filter_approved" class="custom-control-label">Approved</label>
                        </div> --}}
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>reference</th>
                            <th>Email</th>
                            <th>Visas</th>
                            <th>Price</th>
                            <th>Payment</th>
                            <th>Status</th>
                            <th>Last Updated</th>
                            <th></th>
                        </thead>
                        <tbody ng-show="applications">
                            <tr ng-repeat="application in applications">
                                <td>@{{ application.id }}</td>
                                <td>@{{ application.reference }}</td>
                                <td>@{{ application.email }}</td>
                                <td>@{{ application.visas.length }}</td>
                                <td class="text-primary">@{{ application.amount }}</td>
                                <td>
                                    <span ng-show="application.payment.status=='success'"
                                        class="badge badge-success">Success</span>
                                    <span ng-show="application.payment.status=='pending'"
                                        class="badge badge-warning">Pending</span>
                                    <span ng-show="application.payment.status=='error'"
                                        class="badge badge-danger">Error</span>
                                    <span ng-show="application.payment.status=='refunded'"
                                        class="badge badge-info">Refunded</span>
                                </td>
                                <td>@{{ application.status }}</td>
                                <td>@{{ application.updated_at }}</td>
                                <td>
                                    <a   ng-click="viewApplication(application)" title="More Details">
                                        <i class="material-icons">more_vert</i></a>
                                </td>
                        </tbody>
                    </table>
                    <ul class="pagination pull-right">
                        <li class="page-item" ng-class="{disabled:pager.currentPage === 1}"><a class="page-link" href="#" ng-click="setPage(1)">«</a></li>
                        <li class="page-item" ng-class="{disabled:pager.currentPage === 1}"><a class="page-link" href="#" ng-click="setPage(pager.currentPage - 1)">&lsaquo;</a></li>
                        <li class="page-item" ng-repeat="page in pager.pages" ng-class="{active:pager.currentPage === page}"><a class="page-link" href="#" ng-click="setPage(page)">@{{page}}</a></li>
                        <li class="page-item" ng-class="{disabled:pager.currentPage === pager.totalPages}"><a class="page-link" href="#" ng-click="setPage(pager.currentPage + 1)">&rsaquo;</a></li>
                        <li class="page-item" ng-class="{disabled:pager.currentPage === pager.totalPages}"><a class="page-link" href="#" ng-click="setPage(pager.totalPages)">»</a></li>
                    </ul>
                </div>
                <div class="row mt-3">
                    <p class="col-md-6 col-xl-3">Total Processing : @{{ data.processing }}</p>
                    <p class="col-md-6 col-xl-3">Total Rejected : @{{ data.rejected }}</p>
                    <p class="col-md-6 col-xl-3">Total Suspended : @{{ data.suspended }}</p>
                    <p class="col-md-6 col-xl-3">Total Approved : @{{ data.approved }}</p>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<script>
    var app = angular.module('myApp', []);
    app.filter('trusted', ['$sce', function ($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);
    app.controller('myCtrl', function ($scope, $http, PagerService) {
        $scope.applications = null;
        $scope.filter = {status: '%'}

        $scope.all_items = [];
        
        $scope.setPage = function (page) {
            $scope.applications = null;
            // get pager object from service
            $scope.pager = PagerService.GetPager($scope.all_items.length, page);
            if (page < 1 || page > $scope.pager.totalPages) return;

            // get current page of items
            $scope.applications = $scope.all_items.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
        }
        $scope.setPage(1);

        $scope.showApplication = function (reference) {
            angular.forEach($scope.applications, function (value, key) {
                if (value.reference == $scope.reference) {
                    $scope.viewApplication(value);
                }
            })
        }
        $scope.init = function () {      
            $('#application').fadeOut();
            $('#loader').fadeIn();
            $scope.visa_index = 0;
            $scope.reference = (new URL(window.location.href)).searchParams.get('reference');
            $scope.uploads = "{{ asset('uploads/images') }}" + '/';
            $http.get("api/applications?status=" + $scope.filter.status)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $('#loader').fadeOut();
                    $scope.all_items = response.data.applications;
                    $scope.setPage(1);
                    $scope.data = response.data.data;
                    if ($scope.reference) $scope.showApplication($scope.reference);
                }, function (response) {
                    console.error("ERROR applications", response);
                    $('#loader').fadeOut();
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()
        $scope.viewApplication = function (application) {
            $('#application').fadeIn();

            demo.initMaterialWizard();
            setTimeout(function () {
                $('.card.card-wizard').addClass('active');
            }, 600);

            window.location.href = '#application';
            $scope.application = application;
            $scope.application = angular.copy(application);
            $scope.visa_index = 0;
            console.log($scope.application);
        }
        $scope.fileUpload = function (e, field, preview = null) {
            if (!e.target.files[0]) return;
            $('#file-upload-label').addClass('disabled');
            var formData = new FormData();
            formData.append('file', e.target.files[0]);
            formData.append('file_type', 'visa');
            $http({
                method: 'POST',
                url: "api/file_upload",
                data: formData,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function (response) {
                console.log("SUCCESS", response);
                $('#file-upload-label').removeClass('disabled');
                if ($scope.errors) $scope.errors[field] = null;
                else $scope.errors = null;
                $scope.file = response.data;
                if (preview) $('#' + preview).attr('src', $scope.uploads + '/' + response.data)
                // toast.success('File uploaded successfully')
                $scope.saveFile();
            }, function (response) {
                console.log("ERROR", response);
                $('#file-upload-label').removeClass('disabled');
                if (response.status == 422) {
                    if ($scope.errors) $scope.errors[field] = response.data.errors['file'];
                    else $scope.errors = {
                        [field]: response.data.errors['file']
                    };
                    console.log('$scope.errors', $scope.errors);
                } else if (response.status == 413) toastError('Sorry! The file is too large')
                else if (response.status == -1) toastError('Network Error !!')
            });
        }
        $scope.saveFile = function () {
            if (!$scope.file) {
                toast.error('Please choose file upload');
                return;
            }
            $http.post("api/attach_file", {
                    application_id: $scope.application.id,
                    file_id: $scope.file.id
                })
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.file = null;
                    $scope.application = response.data;
                    // toast.success('Successfully Updated');
                    toast.success('File uploaded successfully')
                }, function (response) {
                    console.error('response', response);
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function (value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    }
                });
        }
        $scope.saveApplication = function () {
            console.log('application', $scope.application);
            $('#loader').show();
            $http.post("api/applications", $scope.application)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.application = response.data.application;
                    $scope.all_items = response.data.applications;
                    $scope.setPage(1);
                    $scope.edit_response = false;
                    $('#loader').fadeOut();
                    toast.success('Successfully Updated');
                }, function (response) {
                    console.error('response', response);
                    $('#loader').fadeOut();
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function (value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    }
                });
        }
        $scope.deleteFile = function (file) {
            $http.delete("api/file/" + file.id)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.application = response.data;
                    toast.success('Successfully Deleted');
                }, function (response) {
                    console.error('response', response);
                    toast.error(response.statusText);
                    if (response.status == -1)
                        toast.error('Network Error !!');
                });
        }
        $scope.deleteApplication = function () {
            $http.delete("api/application/" + $scope.application.id)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.application = null;
                    $scope.all_items = null;
                    $scope.all_items = response.data;
                    $scope.setPage(1);
                    $('#application').fadeOut();
                    $('#deleteModal').modal('hide');
                    toast.success('Successfully Deleted');
                }, function (response) {
                    console.error('response', response);
                    toast.error(response.statusText);
                    if (response.status == -1)
                        toast.error('Network Error !!');
                });
        }
        $scope.paymentEdit = function (payment) {
            $scope.payment_edit = payment ? angular.copy(payment) : {application_id: $scope.application.id};
        }
        $scope.savePayment = function () {
            console.log('payment', $scope.payment_edit);
            $('#loader').show();
            $http.post("api/payments", $scope.payment_edit)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.application = response.data.application;
                    $scope.all_items = response.data.applications;
                    $scope.setPage(1);
                    $scope.payment_edit = null;
                    $('#loader').fadeOut();
                    toast.success('Successfully Updated');
                }, function (response) {
                    console.error('response', response);
                    $('#loader').fadeOut();
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function (value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    } else toast.error(response.statusText);
                });
        }
        $scope.notify = function () {
            $('#loader').show();
            $http.post("api/send_notification/" + $scope.application.id, $scope.notification)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $('#loader').fadeOut();
                    toast.success('Successfully Sent');
                }, function (response) {
                    console.error('response', response);
                    $('#loader').fadeOut();
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function (value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    } else toast.error(response.statusText);
                });
        }
    });
</script>
<script src="{{ asset('angularjs/pager-service.js') }}"></script>
@endpush

@push('css')
<style>
    p {
        margin-top: 0;
        margin-bottom: 0rem;
    }

</style>
@endpush
