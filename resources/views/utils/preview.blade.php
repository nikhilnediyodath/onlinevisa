<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            background: url("{{$url ? $url : asset('uploads/files/preview.jpg')}}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            padding: 0px;
            margin: 0px;
        }
        a div{
            height: 100vh;
            width: 100vw;
        }
    </style>
</head>
<body>
    <a href="#" onclick="clicked()">
    <div></div></a>
</body>
<script>
    function clicked(event) {
        var url = "{{$url ? $url : asset('uploads/files/preview.jpg')}}";
        window.top.open(url,'_blank');
    }
</script>
</html>