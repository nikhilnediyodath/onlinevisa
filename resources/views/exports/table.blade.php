<table>
    <thead>
        @foreach ($table as $row)
            <tr>
                @foreach ($row as $key => $value)
                    <th><b>@title($key)</b></th>
                @endforeach
            </tr>
            @break
        @endforeach
    </thead>
    <tbody>
        @foreach ($table as $row)
            <tr>
                @foreach ($row as $key => $value)
                    <td>{{ $value }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
