@extends('layouts.landing')
@section('meta_tags')

    <meta property="og:description" content="Apply for UAE visa effortlessly with VisaByMakto. Discover our streamlined process for obtaining tourist visas and explore the wonders of UAE." />
    <meta property="og:title" content="Apply for UAE Visa | VisaByMakto - Hassle-Free" />
    <meta name="description" content="Apply for UAE visa with VisaByMakto. Experience a hassle-free process and obtain your tourist visa to explore the wonders of UAE. Contact us now!" />
    <meta name="keywords" content="apply for UAE visa, tourist visa">
    <meta name="title" content="Apply for Best UAE Visa & Tourist Visa | VisaByMakto">

@endsection

@section('heading', 'FAQ')

@section('content')

<div class="row">
    <div class="col-md-10 offset-md-1">
    <div class="accordion" style="width: 100%;" id="accordionExample">
        <div class="card" ng-repeat="faq in faqs">
          <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#@{{ faq.id }}" aria-expanded="false" aria-controls="@{{ faq.id }}">
                @{{ faq.title }}
              </button>
            </h2>
          </div>
          <div id="@{{ faq.id }}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                @{{ faq.text }}
                <li style="list-style-type: none;" ng-repeat="ul in faq.ul">@{{ ul }}</li>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@push('js')
<script>
    app.controller('myCtrl', function ($scope, $http) {
        $scope.faqs = [
            {
                id: "faq-1",
                title: "When can I apply for a UAE visa?",
                text: "You can apply for your UAE visa anywhere between 2 – 58 working days prior to your travel date."
            },
            {
                id: "faq-2",
                title: "How many days will it take to process the visa?",
                ul: [
                    "Visa applications require a minimum of 3 – 5 working days to be processed.",
                    "An express visa requires a minimum of 1 working day to be processed."
                ]
            },
            {
                id: "faq-3",
                title: "Can I apply for a UAE visa if I have not booked a flight?",
                text: "Yes! you can apply for a UAE visa"
            },
            {
                id: "faq-4",
                title: "What are the steps involved in the visa application?",
                text: "Once you choose the type of the visa that best suits your travel needs, and complete the payment online you need to upload the required documents. The application is then sent for processing with the authorities."
            },
            {
                id: "faq-5",
                title: "What is the validity of a UAE visa?",
                text: "Depending on the purpose of your visit, you may be granted a visa for 14, 30, or 90 days."
            },
            {
                id: "faq-6",
                title: "Can I travel to the other Emirates if I have a valid UAE visa?",
                text: "Yes, once you have a valid UAE visa, you can travel to any of the Emirates within UAE."
            },
            {
                id: "faq-7",
                title: "Is there a passport validity requirement for a UAE visa?",
                text: "Yes, your passport must be valid for at least six months from the date of travel to qualify for a tourist visa."
            },
            {
                id: "faq-8",
                title: "How will I receive my UAE visa?",
                text: "Your UAE visa will be sent via email to the primary email address provided at the time of application."
            },
            {
                id: "faq-9",
                title: "What are the photograph guidelines?",
                text: "Clear photographs with a white background in JPEG format. File size must be under 2 MB."
            },
            {
                id: "faq-10",
                title: "What are the document guidelines?",
                text: "Please upload all documents as JPEG files. File size must be under 2 MB"
            },
            {
                id: "faq-11",
                title: "Are there any age/ gender restrictions?",
                text: "Parents are required to apply/ submit necessary supporting documents for:",
                ul: [
                    "Male children between the ages of 0 – 18",
                    "Female children between the ages of 0 – 18",
                    "Husband or parents are required to apply/ submit necessary supporting documents for women between the ages of 18 - 29.",
                    "For both men and women between the ages of 56 – 80, travellers may need to provide a copy of their passport and necessary documents as proof that they are visiting a direct blood relative residing in the UAE",
                    "Please send all relevant documents via email to us."
                ]
            },
            {
                id: "faq-12",
                title: "How will I know if additional documents are required, outside of the instances above?",
                text: "If there is a request for additional documents we will get in touch with you via email with the necessary details. All communication will be sent to the email ID provided by you, at the time of booking."
            },
            {
                id: "faq-13",
                title: "Can I apply for a child only?",
                text: "Yes, but only when both parents hold a valid UAE residency permit. Copies of supporting documents along with the application when applying for a child travelling alone need to be sent to us"
            },
            {
                id: "faq-14",
                title: "I am travelling with infants. Do I need to declare information or submit any additional documents?",
                text: "When travelling with an infant, the visa application for the mother and child should be made together. Alternatively, you can also provide a passport copy of the infant which has the mother’s valid UAE resident visa attached"
            },
            {
                id: "faq-15",
                title: "What airports can I fly in to / out of?",
                text: "You can arrive at or depart from any airport within UAE."
            },
            {
                id: "faq-16",
                title: "What happens if I extend my stay in UAE without renewing the visa?",
                text: "Besides facing legal actions and payment of hefty overstay penalty, you may not be allowed to apply for UAE visa in future."
            },
            {
                id: "faq-17",
                title: "What is ‘Ok to Board’?",
                text: "‘Ok to Board’ is an approval from the airline stating that the passenger is travelling with all the necessary documents required for travel and entry in to the UAE."
            },
            {
                id: "faq-18",
                title: "Who needs an ‘Ok to Board’ approval?",
                text: "Indian, Srilankan and Nepalese citizens (amongst others) need OK to Board approval when they travel to UAE. Passengers with an ECR (Emigration Check Required) passport need an OTB approval from the airline. However, there are some airlines who need an OTB approval even for passengers with ECNR passports, depending on departure hub."
            },
            {
                id: "faq-19",
                title: "How do I apply for an ‘Ok to Board’ approval?",
                text: "When you apply for your UAE visa with us we take care of any OTB approvals needed as well. Reach out to us on our customer care if you need OK TO BOARD and have any questions. Please note there would be additional charges as per airlines."
            },
            {
                id: "faq-20",
                title: "How much time do airlines take to update OK To Board message?",
                text: "Airlines take minimum 24 hrs to 48 hrs to update OK To Board message."
            },
            {
                id: "faq-21",
                title: "Which airlines require OK to Board?",
                text: "Ok to board is a mandatory requirement, if you are holding an ECR passport (Emigration Check Required) for these airlines: Air Arabia, Air India, Air India Express, Emirates, Etihad Airways, Flydubai, Gulf Air , Indigo, Kuwait Airways, Qatar Airways, Spice Jet, Sri Lankan Air. For Oman Air Ok to board is mandatory in all cases."
            },
            {
                id: "faq-22",
                title: "Can the UAE Visa be used to visit other cities like Sharjah, Abu Dhabi, and others ?",
                text: "Yes, the UAE Visa is applicable to visit all cities in the 7 emirates within UAE including Sharjah, Abu Dhabi, Fujairah, Ajman and others."
            },
        ];
        console.log($scope.faqs);



    });
</script>
@endpush
