@extends('layouts.admin')

@push('css')
<link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
@endpush

@section('content')
<div class="row">
    <div class="col-md-8 mx-auto">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title" ng-init="action='Add'">@{{ action }} Pricing</h4>
                {{-- <p class="card-category">Complete your profile</p> --}}
            </div>
            <div class="card-body">
                <div class="alert alert-danger" ng-show="error_messages">
                    <span ng-repeat="error_message in error_messages">@{{ error_message }}</span>
                </div>
                <form id="pricing_form">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="citizenship">Citizenship</label>
                            <div class="form-group bmd-form-group">
                                <select id="citizenship" class="form-control" ng-model="price.citizenship"
                                    ng-class="{'invalid': errors.citizenship }">
                                    <option ng-repeat="country in countries" value="@{{ country.code }}">
                                        @{{ country.name }}
                                    </option>
                                </select>
                                <div class="invalid-feedback" ng-show="errors.citizenship">
                                    @{{ errors.citizenship[0] }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Residence</label>
                            <div class="form-group bmd-form-group">
                                <select class="form-control" ng-model="price.residence"
                                    ng-class="{'invalid': errors.residence }">
                                    <option ng-repeat="country in countries" value="@{{ country.code }}">
                                        @{{ country.name }}
                                    </option>
                                </select>
                                <div class="invalid-feedback" ng-show="errors.residence">
                                    @{{ errors.residence[0] }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-6">
                                <label for="">Select Visa Type</label>
                            <div class="form-group bmd-form-group">
                                <select class="form-control" ng-class="{'invalid': errors.visa_type }" ng-init="price.visa_type=1"
                                    ng-model="price.visa_type" ng-options="visatype.id as visatype.name for visatype in visa_types">
                                </select>
                                <div class="invalid-feedback" ng-show="errors.visa_type">
                                    @{{ errors.visa_type[0] }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Entry Type</label>
                            <div class="form-group bmd-form-group">
                                <div class="custom-control custom-radio  custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="single_entry"
                                        ng-model="price.entry_type" value="Single Entry">
                                    <label for="single_entry" class="custom-control-label">Single Entry</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="multiple_entry"
                                        ng-model="price.entry_type" value="Multiple Entry">
                                    <label for="multiple_entry" class="custom-control-label">Multiple Entry</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label>Amount (AED)</label>
                                <input type="text" id="amount" class="form-control" ng-model="price.amount">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="savePrice()">Submit</button>
                    <button type="submit" class="btn btn-danger pull-right" ng-show="action=='Edit'" ng-click="cancelEdit()">Cancel</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Pricing</h4>
                {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
                    <code>* is applicable for any value unless it is specified</code>
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Visa Type</th>
                            <th>Entry Type</th>
                            <th>Citizenship</th>
                            <th>Residence</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </thead>
                        <tbody ng-show="pricing">
                            <tr ng-repeat="price in pricing">
                                <td>@{{ price.id }}</td>
                                <td>@{{ price.visatype.name }}</td>
                                <td>@{{ price.entry_type }}</td>
                                <td>@{{ price.citizenship.name ? price.citizenship.name : '*' }}</td>
                                <td>@{{ price.residence.name ? price.residence.name : '*' }}</td>
                                <td class="text-primary">@{{ price.amount }}</td>
                                <td>
                                    <a   ng-click="editPrice(price)" title="Edit">
                                        <i class="material-icons">edit</i></a>
                                    <a   ng-click="deletePrice(price)" title="Delete">
                                        <i class="material-icons">delete</i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, $window) {
        $scope.defaultPrice = {citizenship:'*', residence:'*', visa_type:1, entry_type:'Single Entry'};
        $scope.init = function () {
            $http.get("api/pricing", $scope.visa)
                .then(function (response) {
                    // console.log("SUCCESS", response);
                    $scope.pricing = response.data;
                }, function (response) {
                    if (response.status == -1) toastError('Network Error !!')
                });
            $http.get("api/get_data", $scope.visa)
                .then(function (response) {
                    // console.log("SUCCESS", response);
                    $scope.visa_types = response.data.visa_types;
                    $scope.countries = response.data.countries;
                    $scope.countries.unshift({code: '*', name:'No Matter'});
                    $scope.price = $scope.defaultPrice;
                    console.log("SUCCESS", response);
                }, function (response) {
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()
        $scope.savePrice = function () {
            $http.post("api/pricing", $scope.price)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.price = $scope.defaultPrice;
                    $scope.pricing = null;
                    $scope.pricing = response.data;
                    $scope.error_messages = null;
                    $scope.action = 'Add';
                    toast.success('Successfully Updated');
                }, function (response) {
                    console.error('response', response);
                    
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function(value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    } else toast.error(response.statusText);
                });
        }
        $scope.deletePrice = function (price) {
            $http.delete("api/pricing/" + price.id)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.price = $scope.defaultPrice;
                    $scope.pricing = null;
                    $scope.pricing = response.data;
                    toast.success('Successfully Deleted');
                }, function (response) {
                    console.error('response', response);
                    toast.error(response.statusText);
                    if (response.status == -1)
                        toast.error('Network Error !!');
                });
        }
        $scope.editPrice = function (price) {
            console.log('price', price);
            $scope.price = angular.copy(price);
            $scope.price.citizenship = price.citizenship_id;
            $scope.price.residence = price.residence_id;
            $scope.price.visa_type = price.visa_type_id;
            $scope.action = 'Edit';
            $('#amount').focus();
        }
        $scope.cancelEdit = function () {
            $scope.action = 'Add';
            $scope.price = $scope.defaultPrice;
        }
    });
</script>
@endpush

@push('css')
<style>
    p {
        margin-top: 0;
        margin-bottom: 0rem;
    }

</style>
@endpush
