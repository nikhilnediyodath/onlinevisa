@extends('layouts.landing')

@section('heading', 'Get in Touch')

@section('meta_tags')
    <meta property="og:description" content="Discover the best travel services in Abu Dhabi with VisaByMakto. We offer expert visa services to make your travel experience seamless and memorable" />
    <meta property="og:title" content="Travel Services Abu Dhabi | VisaByMakto - Your Guide" />
    <meta name="description" content="Explore Abu Dhabi with our comprehensive travel services. Contact VisaByMakto for expert visa services and start your unforgettable journey today!" />
    <meta name="keywords" content="Travel services abu dhabi, visa service abu dhabi">
    <meta name="title" content="Best Travel Services Abu Dhabi | Contact Us | VisaByMakto">
@endsection

@section('content')
<div class="row get-in-touch">
    <div class="col-md-6">
        <h4><i class="fa fa-envelope-o" aria-hidden="true"></i> Contact form</h4>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Your Name" ng-model="contact.name">
            <div class="invalid-feedback" ng-show="errors.name">@{{ errors.name[0] }}</div>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Your Email" ng-model="contact.email">
            <div class="invalid-feedback" ng-show="errors.email">@{{ errors.email[0] }}</div>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Subject" ng-model="contact.subject">
            <div class="invalid-feedback" ng-show="errors.subject">@{{ errors.subject[0] }} </div>
        </div>
        <div class="form-group">
            <textarea class="form-control" rows="12" ng-model="contact.message" placeholder="Message"></textarea>
            <div class="invalid-feedback" ng-show="errors.message">@{{ errors.message[0] }}</div>
        </div>
        <button id="btn-send" class="btn btn-large btn-success" ng-click="sendMessage()">Send a message</button>
    </div>
    <div class="col-md-6" style="text-align: initial;">
        <h4><i class="fa fa-user-o" aria-hidden="true"></i> Get in touch</h4>
        <p><span><i class="fa fa-phone" aria-hidden="true"></i> <strong>Phone : </strong>{{ config('app.contact') }}</span></p>
        <p><span><i class="fa fa-map-marker" aria-hidden="true"></i> <strong>Address : </strong>{{ config('app.address') }}</span></p>
        <p><span><i class="fa fa-envelope" aria-hidden="true"></i> <strong>Email : </strong>{{ config('app.email') }}</span></p>
        <iframe src="{{ config('app.map_embed') }}" width="100%" height="300" allowfullscreen=""></iframe>
    </div>
</div>
@endsection

@push('js')
<script>
    app.controller('myCtrl', function ($scope, $http, $window) {
        $scope.sendMessage = function () {
            $('#btn-send').addClass('disabled');
            $http.post("api/contact", $scope.contact)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $('#btn-send').removeClass('disabled');
                    toastScucess('Your message has been sent');
                    $scope.errors = null;
                }, function (response) {
                    console.error("ERROR", response);
                    $('#btn-send').removeClass('disabled');
                    if (response.status == -1) {
                        toastError('Network Error !!');
                        return;
                    } else if (response.status == 422) {
                        $scope.errors = response.data.errors;
                        toastError(response.data.message);
                    } else
                        toastError('Oops! Something went wrong');
                });
        }
    });
</script>
@endpush