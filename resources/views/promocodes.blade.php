@extends('layouts.admin')

@push('css')
<link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
@endpush

@section('content')
<div class="row">
        <div class="col-md-8 mx-auto">
                <div class="card">
                  <div class="card-header card-header-primary">
                    <h4 class="card-title">Create Promocode</h4>
                    {{-- <p class="card-category">Complete your profile</p> --}}
                  </div>
                  <div class="card-body">
                        <div class="alert alert-danger" ng-show="error_messages">
                                <span ng-repeat="error_message in error_messages">@{{ error_message }}</span>
                        </div>
                    <form>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group bmd-form-group">
                            <label class="bmd-label-floating">Promocode</label>
                            <input type="text" class="form-control" ng-model="promocode.name">
                          </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Discount Type</label>
                            <div class="form-group">
                                <div class="custom-control custom-radio  custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="single_entry" ng-model="promocode.discount_type"
                                        value="Fixed Amount">
                                    <label for="single_entry" class="custom-control-label">Fixed Amount</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline" >
                                    <input type="radio" class="custom-control-input" id="multiple_entry" ng-model="promocode.discount_type"
                                        value="Percentage">
                                    <label for="multiple_entry" class="custom-control-label">Percentage</label>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                            <label class="bmd-label-floating">Amount/Percentage</label>
                            <input type="number" class="form-control" ng-model="promocode.amount">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Minimum applicable amount</label>
                                <input type="number" class="form-control" ng-model="promocode.minimum_amt">
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                            <label class="bmd-label-floating">Limit per user (Number of times)</label>
                            <input type="number" class="form-control" ng-model="promocode.limit">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Maximum applicable amount</label>
                                <input type="number" class="form-control" ng-model="promocode.maximum_amt">
                            </div>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary pull-right" ng-click="savePromocode()">Submit</button>
                      <div class="clearfix"></div>
                    </form>
                  </div>
                </div>
              </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Promocodes</h4>
                {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Promocode</th>
                            <th>Discount Type</th>
                            <th>Constraints</th>
                            <th>Used</th>
                            <th>Amount/Percent</th>
                            <th>Status</th>
                            <th>Block</th>
                        </thead>
                        <tbody ng-show="promocodes">
                            <tr ng-repeat="promocode in promocodes">
                                <td>@{{ promocode.id }}</td>
                                <td>@{{ promocode.name }}</td>
                                <td>@{{ promocode.discount_type }}</td>
                                <td>@{{ promocode.constraints }}</td>
                                <td>@{{ promocode.count }}</td>
                                <td class="text-primary">@{{ promocode.amount }}</td>
                                <td>@{{ promocode.active }}</td>
                                <td><a   ng-click="blockPromocode(promocode)" ng-show="promocode.active=='Active'" title="Block">
                                    <i class="material-icons">highlight_off</i></a>
                                    <a   ng-click="unblockPromocode(promocode)" ng-show="promocode.active=='Blocked'"  title="Unblock">
                                            <i class="material-icons">restore</i></a>
                                    <a   ng-click="deletePromocode(promocode)" ng-show="promocode.count==0"  title="Delete">
                                            <i class="material-icons">delete</i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, $window) {
        $scope.init = function () {
            $http.get("api/promocodes", $scope.visa)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.promocodes = response.data;
                }, function (response) {
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()

        $scope.savePromocode = function () {
            $http.post("api/promocodes", $scope.promocode)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.promocode = null;
                    $scope.promocodes = null;
                    $scope.promocodes = response.data;
                    $scope.error_messages = null;
                    toast.success('Successfully Updated');
                }, function (response) {
                    console.error('response', response);
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function(value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    } else toast.error(response.statusText);
                });
        }

        $scope.blockPromocode = function (promocode) {
            $scope.promocode = angular.copy(promocode);
            $scope.promocode.active = 'Blocked';
            $scope.savePromocode();
        }
        $scope.unblockPromocode = function (promocode) {
            $scope.promocode = angular.copy(promocode);
            $scope.promocode.active = 'Active';
            $scope.savePromocode();
        }

        $scope.deletePromocode = function (promocode) {
            $http.delete("api/promocode/" + promocode.id)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.promocode = $scope.visa_type;
                    $scope.promocodes = null;
                    $scope.promocodes = response.data;
                    toast.success('Successfully Deleted');
                }, function (response) {
                    console.error('response', response);
                    toast.error(response.statusText);
                    if (response.status == -1)
                        toast.error('Network Error !!');
                });
        }
    });
</script>
@endpush

@push('css')
    <style>
    p {
    margin-top: 0;
    margin-bottom: 0rem;
}
    </style>
@endpush
