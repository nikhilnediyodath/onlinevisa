@extends('layouts.admin')

@push('css')
<link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
@endpush

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Summary (This Month)</h4>
                {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <tr>
                                <th>SN</th>
                                <th>Status</th>
                                <th>Application</th>
                                <th>Visas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Processing</td>
                                <td>@{{ data.summary.this_month.processing_count }}</td>
                                <td>@{{ data.summary.this_month.processing_visas }}</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Suspended</td>
                                <td>@{{ data.summary.this_month.suspended_count }}</td>
                                <td>@{{ data.summary.this_month.suspended_visas }}</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Rejected</td>
                                <td>@{{ data.summary.this_month.rejected_count }}</td>
                                <td>@{{ data.summary.this_month.rejected_visas }}</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Approved</td>
                                <td>@{{ data.summary.this_month.approved_count }}</td>
                                <td>@{{ data.summary.this_month.approved_visas }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Summary (Lifetime)</h4>
                {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <tr>
                                <th>SN</th>
                                <th>Status</th>
                                <th>Application</th>
                                <th>Visas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Processing</td>
                                <td>@{{ data.summary.lifetime.processing_count }}</td>
                                <td>@{{ data.summary.lifetime.processing_visas }}</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Suspended</td>
                                <td>@{{ data.summary.lifetime.suspended_count }}</td>
                                <td>@{{ data.summary.lifetime.suspended_visas }}</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Rejected</td>
                                <td>@{{ data.summary.lifetime.rejected_count }}</td>
                                <td>@{{ data.summary.lifetime.rejected_visas }}</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Approved</td>
                                <td>@{{ data.summary.lifetime.approved_count }}</td>
                                <td>@{{ data.summary.lifetime.approved_visas }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
@endpush
