@extends('layouts.admin')

@section('content')

<div class="row">

    @isset($mailTemplate)
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title">{{ $mailTemplate->name }}</h4>
                    <a class="btn btn-sm btn-social btn-link text-white" data-toggle="modal"
                        data-target="#exampleModalLong">
                        <i class="material-icons">help_outline</i></a>
                </div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul class="m-0">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ url('mail_templates') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{ $mailTemplate->id }}">
                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating">Subject</label>
                        <input type="text" name="subject" value="{{ $mailTemplate->subject }}"
                            placeholder="Enter subject" class="form-control" id="exampleEmail">
                    </div>
                    <textarea name="text" id="editor" cols="30" rows="10">{{ $mailTemplate->text }}</textarea>
                    <button type="submit" class="btn btn-success">Submit</button>
                    <a href="{{ url('mail_templates') }}" class="btn btn-danger">Close</a>
                </form>
            </div>
        </div>
    </div>
    @endisset

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Email templates</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Subject</th>
                            <th>Edit</th>
                        </thead>
                        <tbody>
                            @foreach (\App\Models\MailTemplate::all() as $mail)
                            <tr>
                                <td>{{ $mail->id }}</td>
                                <td>{{ $mail->name }}</td>
                                <td>{{ $mail->subject }}</td>
                                <td><a href="{{ url('mail_templates/'.$mail->id) }}"><i class="material-icons">edit</i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Instructions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                You can use the following keywords in your mail template, It variables will be replaced with it's
                original value while sending the email.
                These keywords are case sensitive and reserved (Can't use as it is).
                <br><br>
                <h5><b>Application</b></h5>
                <ol>
                    <li>APPLICANT_NAME</li>
                    <li>REFERENCE_NO</li>
                    <li>EMAIL_ADDRESS</li>
                    <li>TOTAL_AMOUNT (Before apply promocode)</li>
                    <li>PAYABLE_AMOUNT (After apply promocode)</li>
                    <li>MESSAGE (Updated from admin panel)</li>
                    <li>APPLICATION_STATUS</li>
                    <li>PAYMENT_REQUESTED_AT</li>
                    <li>CREATED_AT</li>
                    <li>UPDATED_AT</li>
                </ol>
                <h5><b>Payment</b></h5>
                <ol>
                    <li>TRANSACTION_NO</li>
                    <li>PAYMENT_TRANSACTION_ID</li>
                    <li>REQUEST_AMOUNT</li>
                    <li>CUSTOMER_AMOUNT</li>
                    <li>MERCHANT_AMOUNT</li>
                    <li>PAYMENT_STATUS</li>
                    <li>PAYMENT_RECEIVED_AT</li>
                    <li>PAYMENT_DETAILS_UPDATED_AT</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });

</script>
@endpush