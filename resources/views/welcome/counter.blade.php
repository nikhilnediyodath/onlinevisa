<div id="facts" class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
    <div class="container">
        <div class="row counters">
            <div class="col-lg-3 col-6 text-center">
                <span class="counter-up">{{ App\Models\Settings::find(1)->text }}</span>
                <p>{{ App\Models\Settings::find(1)->name }}</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
                <span class="counter-up">{{ App\Models\Settings::find(2)->text }}</span>
                <p>{{ App\Models\Settings::find(2)->name }}</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
                <span class="counter-up">{{ App\Models\Settings::find(3)->text }}</span>
                <p>{{ App\Models\Settings::find(3)->name }}</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
                <span class="counter-up">{{ App\Models\Settings::find(4)->text }}</span>
                <p>{{ App\Models\Settings::find(4)->name }}</p>
            </div>
        </div>
    </div>
</div>
