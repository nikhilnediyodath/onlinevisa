    <style>
        .visa-form #response-form{
            border: 1px solid #e7e7e7;
            box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
            font-family: 'Montserrat','Archivo', sans-serif !important;
        }
        .visa-form #response-form p{
            font-family: 'Montserrat','Archivo', sans-serif !important;
        }
        #response-form h2{
            color: white;
        }
        #response-form p.value{
            color: white;
            font-weight: 900;
            font-size: 14px;
        }
        
        #response-form p.label{
            color: white;
        }
        #response-form .blue-cell{
            border-bottom: 1px solid #ffffff80;
            margin-bottom: 1rem!important;
            padding-bottom: 1rem!important;
        }
        #response-form .footer{
            border-top: 1px solid #e7e7e7;
            position: absolute;
            border-top: 1px solid #e7e7e7;
            width: 85%;
            bottom: 15px;
        }
    </style>
<div class="col-xl-10 offset-xl-1 pt-3">
    {{-- Success form --}}
    <div id="response-form" ng-show="payment_success || application_submitted" class="row mx-md-5 my-5">
        <div class="col-sm-4 py-4 px-xl-4" style="background-color:#28a728; color:white;">
            <div class="row blue-cell">
                <p class="mb-0 label"> Receipt for</p>
                <p class="value">@{{ application.visas[0].full_name }}</p>
            </div>
            <div class="row blue-cell">
                <div class="col-2"><i class="material-icons">account_balance_wallet</i></div>
                <div class="col-10">
                    <p class="label">Amount :</p>
                    <p class="value">@{{ application.amount }} AED</p>
                </div>
            </div>
            <div class="row blue-cell" ng-show="payment_success">
                <div class="col-2"><i class="material-icons">calendar_today</i></div>
                <div class="col-10">
                    <p class="label">Date :</p>
                    <p class="value">@{{ application.payment.updated_at.substr(0,10) }}</p>
                </div>
            </div>
            <div class="row blue-cell" ng-show="application_submitted">
                <div class="col-2"><i class="material-icons">calendar_today</i></div>
                <div class="col-10">
                    <p class="label">Date :</p>
                    <p class="value">@{{ application.updated_at.substr(0,10) }}</p>
                </div>
            </div>
            <div class="row blue-cell">
                <div class="col-2"><i class="material-icons">assignment</i></div>
                <div class="col-10">
                    <p class="label">Reference No :</p>
                    <p class="value">@{{ application.reference }}</p>
                </div>
            </div>
            <div class="row blue-cell" ng-show="payment_success">
                <div class="col-2"><i class="material-icons">list_alt</i></div>
                <div class="col-10">
                    <p class="label">Transaction No :</p>
                    <p class="value">@{{ application.payment.transaction_no }}</p>
                </div>
            </div>
        </div>

        <div class="col-sm-8 py-4 px-xl-4" style="background: #fbfbfb;">
            <div class="container-fluid h-100">
                <div class="d-flex">
                    <div class="ml-auto">
                        <p style="color:grey"><small>@{{ application.payment.updated_at }}</small></p>
                    </div>            
                </div>
                <hr>
                <div class="p-4">
                    <h2 class="text-success">Success!</h2>
                    <p style="line-height: 2rem;">{{ $success_message }}</p>
                </div>
                <div class="footer">
                    <div class="position-relative d-flex w-100">
                        <a class="font-weight-bold" href="#" style="color:#2481E3;font-size:12px;">{{ url('') }}</a>
                        <div class="ml-auto" ng-show="payment_success">
                            <p style="color:grey"><small>Payment ID: @{{ application.payment.payment_transaction_id }}</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- End of success form --}}

    
    {{-- Error form --}}
    <div id="response-form" ng-show="payment_error==true" class="row mx-md-5 my-5">
            <div class="col-sm-4 py-4 px-xl-4" style="background-color:#ea1f32; color:white;">
                <div class="row blue-cell">
                    <p class="mb-0 label"> Receipt for</p>
                    <p class="value">@{{ application.visas[0].full_name }}</p>
                </div>
                <div class="row blue-cell">
                    <div class="col-2"><i class="material-icons">account_balance_wallet</i></div>
                    <div class="col-10">
                        <p class="label">Amount :</p>
                        <p class="value">@{{ application.amount }} AED</p>
                    </div>
                </div>
                <div class="row blue-cell">
                    <div class="col-2"><i class="material-icons">calendar_today</i></div>
                    <div class="col-10">
                        <p class="label">Date :</p>
                        <p class="value">@{{ application.payment.updated_at.substr(0,10) }}</p>
                    </div>
                </div>
                <div class="row blue-cell">
                    <div class="col-2"><i class="material-icons">assignment</i></div>
                    <div class="col-10">
                        <p class="label">Reference No :</p>
                        <p class="value">@{{ application.reference }}</p>
                    </div>
                </div>
            </div>
    
            <div class="col-sm-8 py-4 px-xl-4" style="background: #fbfbfb;">
                <div class="container-fluid h-100">
                    <div class="d-flex">
                        <div class="ml-auto">
                            <p style="color:grey"><small>@{{ application.payment.updated_at }}</small></p>
                        </div>            
                    </div>
                    <hr>
                    <div class="p-4">
                        <h2 class="text-danger">Error!</h2>
                        <p style="line-height: 2rem;">Something went wrong with your payment.
                            If problem persists contact {{ config('app.contact') }}</p>
                    </div>
                    <div class="footer">
                        <div class="position-relative d-flex w-100">
                            <a class="font-weight-bold" href="#" style="color:#2481E3;font-size:12px;">{{ url('') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End of Error form --}}
</div>