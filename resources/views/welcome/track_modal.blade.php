<!-- Track Modal -->
<div class="modal fade" id="trackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
        <div class="modal-content">
            <div class="container justify-content-end">
                <button type="button" class="close mt-2" data-dismiss="modal" aria-label="Close"
                    style="text-align: end; width: 21px;">
                    <span aria-hidden="true" style="font-size: larger;"><b>&times;</b></span>
                </button>
            </div>
            <div class="container visa-form px-2 px-lg-5 mt-n4">
                <h4 class="title">Track Application</h4>
                <!-- <p class="modal-sub-title my-3">Tra</p> -->
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Enter your reference number"
                        ng-model="track.reference_number" ng-class="{'invalid': errors.reference_number }">
                    <div class="invalid-feedback" ng-show="errors.reference_number">
                        @{{ errors.reference_number[0] }}
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center">
                    <a href="#" class="button" ng-click="trackApplication()" id="btn-track">Track Your Application</a>
                </div>
                {{-- Time-line --}}
                <div class="row" ng-show="track_application">
                    <div class="col-md-12">
                        <div class="card-timeline card-plain">
                            <div class="card-body">
                                <ul class="timeline">
                                    {{-- 1. Application received --}}
                                    <li>
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">card_travel</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Application Received</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>We have received your application on @{{ track_application.created_at }}</p>
                                            </div>
                                        </div>
                                    </li>

                                    {{-- 2. Documents Requird --}}
                                    <li ng-class="{'d-none': track_application.status_code != 2}">
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">attach_file</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Documents Required</span>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>We need some more documents to complete your application. We have sent an email to your registered email which shows more detail</p>
                                            </div>
                                        </div>
                                    </li>

                                    {{-- 3. Application rejected by Makto --}}
                                    <li ng-class="{'danger': track_application.status_code == 3, 'd-none': track_application.status_code != 3}">
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">cancel</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Application Rejected</span>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>Your application is rejected by Makto</p>
                                                <p>We have sent a detailed email to your registered email</p>
                                            </div>
                                        </div>
                                    </li>


                                    {{-- 4. Payment Requested --}}
                                    <li ng-class="{'disabled': track_application.status_code < 4, 'd-none': track_application.status_code == 3 }">
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">attach_money</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Payment Requested</span>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>We have send an email with the payment link to the email your registered email at @{{ track_application.payment_requested_at }}</p>
                                            </div>
                                        </div>
                                    </li>

                                    {{-- 5. Payment Error --}}
                                    <li ng-class="{'danger': track_application.status_code == 5, 'd-none': track_application.status_code != 5}">
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">cancel</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Application Rejected</span>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>Your application is rejected by immigration</p>
                                            </div>
                                        </div>
                                    </li>

                                    {{-- 6. Payment Received --}}
                                    <li ng-class="{'disabled': track_application.status_code < 6, 'd-none': track_application.status_code == 3 }">
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">payment</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Payment Received</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>Your payment received at @{{ track_application.payment.payment_received_at }}</p>
                                            </div>
                                        </div>
                                    </li>

                                    {{-- 7. Payment Error --}}
                                    <li ng-class="{'danger': track_application.status_code == 7, 'd-none': track_application.status_code != 7}">
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">cancel</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Application Rejected</span>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>Your application is rejected by immigration</p>
                                            </div>
                                        </div>
                                    </li>

                                    {{-- 8. Visa Ready --}}
                                    <li ng-class="{'disabled': track_application.status_code < 8,'d-none': (track_application.status_code == 3 || track_application.status_code == 7)}">
                                        <div class="bullet">&#8226;</div>
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="timeline-badge">
                                                    <i class="material-icons">flight_takeoff</i>
                                                </div>
                                            </div>
                                            <div class="col-11">
                                                <div class="timeline-heading">
                                                    <span class="badge badge-pill badge-success">Visa Delivered</span>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-body">
                                                <p>Your visas sent to registered email address</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Time-line --}}
            </div><!-- /.container -->
        </div>
    </div>
</div>
<!-- End of Track modal -->