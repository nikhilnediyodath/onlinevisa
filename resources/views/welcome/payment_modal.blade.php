<!-- Response Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document" style="max-width: 1140px;">
        <div class="modal-content">
            <button type="button" class="close mt-2 position-absolute" data-dismiss="modal" aria-label="Close" style="right: 15px; width:21px;">
                <span aria-hidden="true" style="font-size: larger;"><b>&times;</b></span>
            </button>
            <div class="container visa-form px-0 px-lg-1 px-xl-5">
                <div class="tab-content" id="myTabContent">
                    <!-- Response -->
                    <div class="tab-pane show active" ng-class="{'show active' : tab_index == 6}" id="response" role="tabpanel" aria-labelledby="response">
                        @include('welcome.response', [
                            'success_message' => 'Your payment has been successfully received. We will notify you of further updates.'
                        ])
                    </div>
                    <!-- End of Response -->

                </div><!-- /.tab-content -->
            </div><!-- /.container -->
        </div>
    </div>
</div>
<!-- End of Response modal -->