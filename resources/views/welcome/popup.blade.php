<!-- Track Modal -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 600px;">
        <div class="modal-content intro">
            <div class="container" style="z-index:2;">
                <button type="button" class="btn-cls close mt-2" data-dismiss="modal" aria-label="Close"
                    style="text-align: end; width: 21px;">
                    <span aria-hidden="true" style="font-size: larger;"><b>&times;</b></span>
                </button>
            </div>
            <div class="mt-n5" style=" background-image: url('popup.png');
                width:100%;
                padding-top:100%;
                background-size: cover;
                background-position: center;
                z-index: 1;">
                </div>
        </div>
    </div>
</div>
<!-- End of Track modal -->
@push('css')
<style>
    .modal-content.intro{
        background-color: #fff0;
        border: 1px solid rgba(0, 0, 0, 0);
    }
    .modal-content.intro .btn-cls b{
    color: #fff;
}
</style>    
@endpush

@push('js')
<script>
    // $('#popup').modal('show')
</script>
@endpush