<style>
    .timeline-item{
        width: calc(100%/7);
        position: relative;
    }
    .timeline-item-first,.timeline-item-last{
        width: calc(100%/14);
    }
</style>
<!-- Apply Modal -->
<div class="modal fade" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document" style="max-width: 1140px;">
        <div class="modal-content">

            <button type="button" class="close mt-2 position-absolute" data-dismiss="modal" aria-label="Close"
                style="right: 15px; width:21px;">
                <span aria-hidden="true" style="font-size: larger;"><b>&times;</b></span>
            </button>
            <label class="kart-total" ng-show="application.amount">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;@{{ application.amount }}&nbsp;<small>AED</small>
            </label>

            <div class="container justify-content-end">
            </div>
            <div class="container visa-form px-0 px-lg-1 px-xl-5">
                {{-- <h4 style="" class="my-3 title">@{{ title[tab_index] }}</h4> --}}
                <div class="row timeline-horizontal pt-5 mt-5 pb-3 justify-content-center">
                    <div class="timeline-item timeline-item-first"><div class="timeline-terminator"></div></div>
                    <div class="timeline-item" ng-class="{'active' : tab_index >= 0,'current' : tab_index == 0}">
                        <div class="bullet">1</div>
                        <div class="timeline-title">Instructions</div>
                    </div>
                    <div class="timeline-item" ng-class="{'active' : tab_index >= 1,'current' : tab_index == 1}">
                        <div class="bullet">2</div>
                        <div class="timeline-title">Visa Details</div>
                    </div>
                    <div class="timeline-item" ng-class="{'active' : tab_index >= 2,'current' : tab_index == 2}">
                        <div class="bullet">3</div>
                        <div class="timeline-title">Personal Details</div>
                    </div>
                    <div class="timeline-item" ng-class="{'active' : tab_index >= 3,'current' : tab_index == 3}">
                        <div class="bullet">4</div>
                        <div class="timeline-title">Passport Details</div>
                    </div>
                    <div class="timeline-item" ng-class="{'active' : tab_index >= 4,'current' : tab_index == 4}">
                        <div class="bullet">5</div>
                        <div class="timeline-title">Summary</div>
                    </div>
                    <div class="timeline-item" ng-class="{'active' : tab_index >= 5,'current' : tab_index == 5}">
                        <div class="bullet">6</div>
                        <div class="timeline-title">Verification</div>
                    </div>
                    <div class="timeline-item timeline-item-last" ng-class="{'active' : tab_index >= 6}">
                        <div class="timeline-terminator timeline-last-item"></div>
                        <div class="bullet">7</div>
                        <div class="timeline-title">Confirmation</div>
                    </div>
                </div>

                <div class="tab-content" id="myTabContent">

                    <!-- Instructions -->
                    <div class="tab-pane fade" ng-class="{'show active' : tab_index == 0}" id="instructions" role="tabpanel"
                        aria-labelledby="instructions-tab">
                        <div class="row mt-3">
                            <div class="col">
                                <div class="row justify-content-center">
                                    <div class="col-md-10 col-lg-10 col-xl-10">
                                        <div class="form-group">
                                            <p class="modal-sub-title p-1" style="color: #444;">Instructions</p>
                                            <ul>
                                                <li>Your passport must be valid for 6 months from the visa application date. The passport should not be in a handwritten format</li>
                                                <li>Depending on your nationality, you may be requested to provide additional documents in support of their visa application - our team will get in touch with you in your registered email ID and/or phone number if there are further requirements. If these documents cannot be provided, the visa application may be rejected. The visa fee is <span>non-refundable</span> under any circumstances</li>
                                                <li>Documents must be submitted in JPEG/PNG/PDF format and may not exceed 4MB</li>
                                                <li>Make sure to provide clear and full images of the required documents to avoid unnecessary delays in processing your electronic visa application</li>
                                                <li>Each person traveling needs to obtain their own electronic visa, including children</li>
                                                <li><b>Normal Delivery: Delivered within 5 working days excluding the visa application date</b></li>
                                                <li><b>Urgent Delivery: Delivered within 2 working days excluding the visa application date</b></li>
                                                <li>6x6cm photograph in colour (minimum of 600x600 pixels) with a white background (at least 80% of your face should be visible)</li>
                                                <li>Colour copy of your passport showing your full name, nationality, date and place of birth, passport number, date of issue and expiry, name of father / legal guardian, and the name of your mother</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer d-flex justify-content-center">
                                    <a href="#" class="button btn-nav" ng-click="tab_index=1">Next</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Instructions -->

                    <!-- visa details -->
                    <div class="tab-pane fade" ng-class="{'show active' : tab_index == 1}" id="visa_details" role="tabpanel"
                        aria-labelledby="instructions-tab">
                        <div class="row mt-3">
                            <div class="col">
                                <div class="row justify-content-center">
                                    <div class="col-md-6 col-lg-5 col-xl-6">
                                        <div class="total-price" role="alert" ng-show="price">
                                            <small>This visa costs<span class="cost"> AED @{{ price }}</span></small>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group date" data-provider="datepicker">
                                                <input type="text" class="form-control"
                                                    ng-class="{'invalid': errors.arrival_date }"
                                                    ng-model="visa.arrival_date"
                                                    placeholder="Select your date of arrival">
                                                <div class="input-group-addon">
                                                    <span class="input-group-addon"><i
                                                            class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="invalid-feedback" ng-show="errors.arrival_date">
                                                @{{ errors.arrival_date[0] }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <select class="form-control" ng-model="visa.citizenship" ng-change="getPrice()"
                                                ng-class="{'invalid': errors.citizenship }">
                                                <option value="">Select your citizenship</option>
                                                <option ng-repeat="country in countries" value="@{{ country.code }}">@{{ country.name }}</option>
                                            </select>
                                            <div class="invalid-feedback" ng-show="errors.citizenship">
                                                @{{ errors.citizenship[0] }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" ng-model="visa.residence" ng-change="getPrice()"
                                                ng-class="{'invalid': errors.residence }">
                                                <option value="">Select your country of residence</option>
                                                <option ng-repeat="country in countries" value="@{{ country.code }}">@{{ country.name }}</option>
                                            </select>
                                            <div class="invalid-feedback" ng-show="errors.residence">
                                                @{{ errors.residence[0] }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" ng-class="{'invalid': errors.visa_type }" ng-change="getPrice()"
                                                ng-model="visa.visa_type">
                                                <option value="">Select visa type</option>
                                                <option ng-repeat="visatype in visa_types" value="@{{ visatype.id }}">@{{ visatype.name }}</option>
                                            </select>
                                            <div class="invalid-feedback" ng-show="errors.visa_type">
                                                @{{ errors.visa_type[0] }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col pr-0">
                                                    <div class="custom-control custom-radio  custom-control-inline">
                                                        <input type="radio" class="custom-control-input" id="single_entry" ng-model="visa.entry_type"
                                                            value="Single Entry" ng-change="getPrice()">
                                                        <label for="single_entry" class="custom-control-label">Single Entry</label>
                                                    </div>
                                                </div>
                                                <div class="col pr-0">
                                                    <div class="custom-control custom-radio custom-control-inline delivery" >
                                                        <input type="radio" class="custom-control-input" id="multiple_entry" ng-model="visa.entry_type"
                                                            value="Multiple Entry" ng-change="getPrice()">
                                                        <label for="multiple_entry" class="custom-control-label">Multiple Entry</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col pr-0">
                                                    <div class="custom-control custom-radio  custom-control-inline n-delivery">
                                                        <input type="radio" class="custom-control-input" id="normal_delivery" ng-model="visa.delivery_type"
                                                            value="Normal Delivery" ng-change="getPrice()">
                                                        <label for="normal_delivery" class="custom-control-label">Normal Delivery</label>
                                                        {{-- <span class="normal">Delivery within 4 days</span>  --}}
                                                    </div>
                                                </div>
                                                <div class="col pr-0">
                                                    <div class="custom-control custom-radio custom-control-inline u-delivery" >
                                                        <input type="radio" class="custom-control-input" id="urgent_delivery" ng-model="visa.delivery_type"
                                                            value="Urgent Delivery" ng-change="getPrice()">
                                                        <label for="urgent_delivery" class="custom-control-label">Urgent Delivery</label>
                                                        {{-- <span class="urgent">Delivery within 2 days</span> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="modal-footer d-flex justify-content-center">
                                    <a href="#" class="button btn-nav" ng-click="tab_index=tab_index-1">Previous</a>
                                    <a href="#" class="button btn-nav" ng-click="nextTab('visa_details')">Next</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of visa details -->

                    <!-- Personal Details -->
                    <div class="tab-pane fade" ng-class="{'show active' : tab_index == 2}" id="personal-details" role="tabpanel"
                        aria-labelledby="personal-details">
                            <div class="row mt-3">
                                <div class="col-md-6 col-lg-4 col-xl-4">
                                    <div class="d-flex justify-content-around mb-2">
                                        <div class="card p-1">
                                            <iframe class="card-img-top" ng-src="@{{ visa.user_image.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">Applicant Photo</span>
                                            </div>
                                        </div>
                                        <div class="card p-1"
                                            ng-show="visa.citizenship=='IQ' || visa.citizenship=='IR' || visa.citizenship=='KW'">
                                            <iframe class="card-img-top" ng-src="@{{ visa.national_id.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">National ID</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group custom-file">
                                        <input id="customFile" class="form-control-file custom-file-input" type="file"
                                            onchange="angular.element(this).scope().fileUpload(event, 'user_image', 'img-user')">
                                        <label class="custom-file-label" ng-class="{'invalid': errors['user_image.id'], 'valid': success.user_image }" for="customFile">Choose Photo</label>
                                        <div class="invalid-feedback" ng-show="errors['user_image.id']">@{{ errors['user_image.id'][0] }}</div>
                                        <small>6x6cm photograph in colour (minimum of 600x600 pixels) with a white background (at least 80% of face should be visible)</small>
                                    </div>
                                    <div class="form-group custom-file mt-2" ng-show="visa.citizenship=='IQ' || visa.citizenship=='IR' || visa.citizenship=='KW'">
                                        <input class="form-control-file custom-file-input" type="file" id="customFile1"
                                            onchange="angular.element(this).scope().fileUpload(event, 'national_id', 'img-user-id')">
                                        <label class="custom-file-label" ng-class="{'invalid': errors['national_id.id'], 'valid': success.national_id }" for="customFile1">Choose National ID</label>
                                        <div class="invalid-feedback" ng-show="errors['national_id.id']">
                                            @{{ errors['national_id.id'][0] }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Enter your firstname"
                                            ng-model="visa.first_name" ng-class="{'invalid': errors.first_name }">
                                        <div class="invalid-feedback" ng-show="errors.first_name">
                                            @{{ errors.first_name[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group" ng-hide="visa.citizenship=='PK' && visa.gender=='Male'">
                                        <input class="form-control" type="text" placeholder="Enter your middlename (optional)"
                                            ng-model="visa.middle_name" ng-class="{'invalid': errors.middle_name }">
                                        <div class="invalid-feedback" ng-show="errors.middle_name">
                                            @{{ errors.middle_name[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group" ng-hide="visa.citizenship=='IN'">
                                        <input class="form-control" type="text" placeholder="Enter your lastname/surname (optional)"
                                            ng-model="visa.last_name" ng-class="{'invalid': errors.last_name }">
                                        <div class="invalid-feedback" ng-show="errors.last_name">
                                            @{{ errors.last_name[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group date" data-provider="datepicker">
                                            <input type="text" class="form-control" ng-model="visa.date_of_birth"
                                                ng-class="{'invalid': errors.date_of_birth }"
                                                placeholder="Select your date of birth">
                                            <div class="input-group-addon">
                                                <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" ng-show="errors.date_of_birth">
                                            @{{ errors.date_of_birth[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Enter your mother's name"
                                            ng-model="visa.mother_name" ng-class="{'invalid': errors.mother_name }">
                                        <div class="invalid-feedback" ng-show="errors.mother_name">
                                            @{{ errors.mother_name[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Enter your father's name"
                                            ng-model="visa.father_name" ng-class="{'invalid': errors.father_name }">
                                        <div class="invalid-feedback" ng-show="errors.father_name">
                                            @{{ errors.father_name[0] }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-xl-4">
                                    <div class="form-group mt-xl-4">
                                        <div class="row">
                                            <div class="col pr-0">
                                                <div class="custom-control custom-radio  custom-control-inline">
                                                    <input type="radio" id="male" class="custom-control-input" ng-model="visa.gender"
                                                        value="Male">
                                                    <label for="male" class="custom-control-label">Male</label>
                                                </div>
                                            </div>
                                            <div class="col pr-0">
                                                <div class="custom-control custom-radio custom-control-inline" >
                                                    <input type="radio" id="female" class="custom-control-input" ng-model="visa.gender"
                                                        value="Female">
                                                    <label for="female" class="custom-control-label">Female</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-xl-3 mb-xl-0">
                                        <div class="row">
                                            <div class="col pr-0">
                                                <div class="custom-control custom-radio  custom-control-inline">
                                                    <input type="radio" id="single" class="custom-control-input" ng-model="visa.marital_status"
                                                        value="Single">
                                                    <label for="single" class="custom-control-label">Single</label>
                                                </div>
                                            </div>
                                            <div class="col pr-0">
                                                <div class="custom-control custom-radio custom-control-inline" >
                                                    <input type="radio" id="married" class="custom-control-input" ng-model="visa.marital_status"
                                                        value="Married">
                                                    <label for="married" class="custom-control-label">Married</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" ng-show="visa.marital_status=='Married'">
                                        <input class="form-control" type="text" ng-model="visa.spouse_name"
                                            placeholder="Enter your spouse name" ng-class="{'invalid': errors.spouse_name }">
                                            <div class="invalid-feedback" ng-show="errors.spouse_name">
                                                @{{ errors.spouse_name[0] }}
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" ng-model="visa.place_of_birth"
                                            ng-class="{'invalid': errors.place_of_birth }"
                                            placeholder="Enter your place of birth (as in passport)">
                                            <div class="invalid-feedback" ng-show="errors.place_of_birth">
                                                @{{ errors.place_of_birth[0] }}
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" ng-model="visa.religion"
                                            placeholder="Enter your religion" ng-class="{'invalid': errors.religion }">
                                            <div class="invalid-feedback" ng-show="errors.religion">
                                                @{{ errors.religion[0] }}
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" ng-model="visa.first_language"
                                            ng-class="{'invalid': errors.first_language }">
                                            <option value="">Select your first language</option>
                                            <option ng-repeat="language in languages" value="@{{ language.id }}">@{{ language.name }}</option>
                                        </select>
                                        <div class="invalid-feedback" ng-show="errors.first_language">
                                            @{{ errors.first_language[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" ng-model="visa.second_language"
                                            ng-class="{'invalid': errors.second_language }">
                                            <option value="">Select your second language</option>
                                            <option ng-repeat="language in languages" value="@{{ language.id }}">@{{ language.name }}</option>
                                        </select>
                                        <div class="invalid-feedback" ng-show="errors.second_language">
                                            @{{ errors.second_language[0] }}
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <a href="#" class="button btn-nav" ng-click="tab_index=tab_index-1">Previous</a>
                                <a href="#" class="button btn-nav" ng-click="nextTab('personal_details')">Next</a>
                            </div>
                    </div>
                    <!-- End of Personal Details -->

                    <!-- Passport Details -->
                    <div class="tab-pane fade" ng-class="{'show active' : tab_index == 3}" id="passport-details" role="tabpanel"
                        aria-labelledby="passport-details">
                            <div class="row mt-3">
                                <div class="col-md-6 col-lg-4 col-xl-4">
                                    <div class="d-flex justify-content-around mb-2">
                                        <div class="card p-1">
                                            <iframe class="card-img-top" ng-src="@{{ visa.passport_first_page.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">Passport First Page</span>
                                            </div>
                                        </div>
                                        <div class="card p-1">
                                            <iframe class="card-img-top" ng-src="@{{ visa.passport_last_page.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">Passport Last Page</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group custom-file">
                                        <input id="customFile2" class="form-control-file custom-file-input" type="file"
                                            onchange="angular.element(this).scope().fileUpload(event, 'passport_first_page', 'img-passport1')">
                                        <label class="custom-file-label" ng-class="{'invalid': errors['passport_first_page.id'], 'valid': success.passport_first_page }" for="customFile2">
                                            Choose passport first page
                                        </label>
                                        <div class="invalid-feedback" ng-show="errors['passport_first_page.id']">
                                            @{{ errors['passport_first_page.id'][0] }}
                                        </div>
                                    </div>
                                    <div class="form-group custom-file">
                                        <input id="customFile3" class="form-control-file custom-file-input" type="file"
                                            onchange="angular.element(this).scope().fileUpload(event, 'passport_last_page', 'img-passport2')">
                                        <label class="custom-file-label" ng-class="{'invalid': errors['passport_last_page.id'], 'valid': success.passport_last_page }" for="customFile3">
                                            Choose passport last page
                                        </label>
                                        <div class="invalid-feedback" ng-show="errors['passport_last_page.id']">
                                            @{{ errors['passport_last_page.id'][0] }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" ng-model="visa.passport_number"
                                            placeholder="Enter Passport Number" ng-class="{'invalid': errors.passport_number }">
                                        <div class="invalid-feedback" ng-show="errors.passport_number">
                                            @{{ errors.passport_number[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group date" data-provider="datepicker">
                                            <input type="text" class="form-control" ng-model="visa.passport_issue_date"
                                                ng-class="{'invalid': errors.passport_issue_date }"
                                                placeholder="Select date of passport issue">
                                            <div class="input-group-addon">
                                                <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" ng-show="errors.passport_issue_date">
                                            @{{ errors.passport_issue_date[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group date" data-provider="datepicker">
                                            <input type="text" class="form-control" ng-model="visa.passport_expiry_date"
                                                ng-class="{'invalid': errors.passport_expiry_date }"
                                                placeholder="Select date of passport expiry">
                                            <div class="input-group-addon">
                                                <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" ng-show="errors.passport_expiry_date">
                                            @{{ errors.passport_expiry_date[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" ng-model="visa.passport_issue_place"
                                            placeholder="Place of passport issue" ng-class="{'invalid': errors.passport_issue_place }">
                                        <div class="invalid-feedback" ng-show="errors.passport_issue_place">
                                            @{{ errors.passport_issue_place[0] }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-xl-4">
                                    <p class="mr-auto p-2 modal-sub-title">Additional Documents</p>
                                    <div class="form-group custom-file mt-xl-3">
                                        <input id="customFile3" class="form-control-file custom-file-input" type="file"
                                            onchange="angular.element(this).scope().fileUpload(event, 'attachments')">
                                        <label class="custom-file-label" ng-class="{'invalid': errors.attachments, 'valid': success.attachments }" for="customFile3">
                                            Attach Additional Document
                                        </label>
                                        <div class="invalid-feedback" ng-show="errors.attachments">
                                            @{{ errors.attachments[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" ng-model="doc_type"
                                            ng-class="{'invalid': errors.doc_type }">
                                            <option value="">Select documet type</option>
                                            <option ng-repeat="doc_type in data.doc_types" value="@{{ doc_type }}">@{{ doc_type }}</option>
                                        </select>
                                        <div class="invalid-feedback" ng-show="errors.doc_type">
                                            @{{ errors.doc_type[0] }}
                                        </div>
                                    </div>
                                    <table class="table table-borderless">
                                        <p ng-show="visa.attachments.length > 0" class="mt-3">Attachments</p>
                                        <tbody>
                                            <tr ng-repeat="file in visa.attachments">
                                                <td class="px-0 py-1"><a href="@{{ file.url }}" target="_blank"><span style="font-size: 13px;">@{{ file.doc_type }}</span></a></td>
                                                <td class="py-1"><a   ng-click="deleteFile(file)"><i class="fa fa-trash" aria-hidden="true"></i></td>
                                            </tr>
                                        </tbody>

                                    </table>
                                    {{-- <div class="row">
                                        <div class="col-4" ng-repeat="file in visa.attachments">
                                            <iframe class="card-img-top" ng-src="@{{ file.url }}" type="@{{ file.mime }}">
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="d-flex justify-content-center add-person">
                                <a href="#" class="px-sm-5" ng-click="nextTab('passport_details', 1)">I want to add more person</a>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <a href="#" class="button btn-nav" ng-click="tab_index=tab_index-1">Previous</a>
                                <a href="#" class="button btn-nav" ng-click="nextTab('passport_details')">Next</a>
                            </div>
                    </div>
                    <!-- End of Passport Details -->

                    <!-- Confirm Details -->
                    <div class="tab-pane fade" ng-class="{'show active' : tab_index == 4}" id="confirm-details" role="tabpanel"
                        aria-labelledby="confirm-details">

                        <div class="container visa-form confirm">
                            <div class="d-flex justify-content-end">
                                {{-- <p class="mr-auto p-2 modal-sub-title">Confirm Details</p> --}}
                                <p class="p-2 position-absolute"><b>@{{ visa_index + 1 }} of @{{ application.visas.length }}</b></p>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-4 px-0 pt-5 pt-md-2">
                                    <div class="d-flex justify-content-around mb-2">
                                        <div class="card p-1">
                                            <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].user_image.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">Applicant Photo</span>
                                            </div>
                                        </div>
                                        <div class="card p-1"
                                            ng-show="visa.citizenship=='IQ' || visa.citizenship=='IR' || visa.citizenship=='KW'">
                                            <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].national_id.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">National ID</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-around mb-2">
                                        <div class="card p-1">
                                            <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].passport_first_page.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">Passport First Page</span>
                                            </div>
                                        </div>
                                        <div class="card p-1">
                                            <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].passport_last_page.thumb | trusted }}"></iframe>
                                            <div class="card-body p-0 row justify-content-center align-items-end">
                                                <span class="badge badge-secondary mt-1">Passport Last Page</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <p class="mt-2 mb-1"><b>Personal Details</b>
                                        <a href="#" class="btn-edit" ng-click="editVisa(application.visas[visa_index], 2)">
                                                <i class="material-icons px-2 btn-edit">edit</i></a></p>
                                    <p class="personal-d">First Name : @{{ application.visas[visa_index].first_name }}</p>
                                    <p class="personal-d">Middle Name : @{{ application.visas[visa_index].middle_name }}</p>
                                    <p class="personal-d">Last Name : @{{ application.visas[visa_index].last_name }}</p>
                                    <p class="personal-d">Citizenship : @{{ application.visas[visa_index].citizen.name }}</p>
                                    <p class="personal-d">Country of Residence : @{{ application.visas[visa_index].residency.name }}</p>
                                    <p class="personal-d">Gender : @{{ application.visas[visa_index].gender }}</p>
                                    <p class="personal-d">Marital Status : @{{ application.visas[visa_index].marital_status }}</p>
                                    <p class="personal-d" ng-show="application.visas[visa_index].marital_status=='Married'">Spouse Name : @{{ application.visas[visa_index].spouse_name }}</p>
                                    <p class="personal-d">Mother's Name : @{{ application.visas[visa_index].mother_name }}</p>
                                    <p class="personal-d">Father's Name : @{{ application.visas[visa_index].father_name }}</p>
                                    <p class="personal-d">Date of Birth : @{{ application.visas[visa_index].date_of_birth.substr(0,10) }}</p>
                                    <p class="personal-d">Place of Birth : @{{ application.visas[visa_index].place_of_birth }}</p>
                                    <p class="personal-d">Religion : @{{ application.visas[visa_index].religion }}</p>
                                    <p class="personal-d">First Language : @{{ application.visas[visa_index].language_one.name }}</p>
                                    <p class="personal-d">Second Language : @{{ application.visas[visa_index].language_two.name }}</p>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <p class="mt-2 mb-1"><b>Passport Details</b>                                    
                                        <a href="#" class="btn-edit" ng-click="editVisa(application.visas[visa_index], 3)">
                                                <i class="material-icons px-2 btn-edit">edit</i></a></p>
                                    <p class="passport-d">Passport Number : @{{ application.visas[visa_index].passport_number }}</p>
                                    <p class="passport-d">Date of Issue : @{{ application.visas[visa_index].passport_issue_date.substr(0,10) }}</p>
                                    <p class="passport-d">Place of Issue : @{{ application.visas[visa_index].passport_issue_place }}</p>
                                    <p class="passport-d">Date of expiry : @{{ application.visas[visa_index].passport_expiry_date.substr(0,10) }}</p>
                                    <p class="mt-2 mb-1"><b>Visa Details</b>                                    
                                        <a href="#" class="btn-edit" ng-click="editVisa(application.visas[visa_index], 1)">
                                                <i class="material-icons px-2 btn-edit">edit</i></a></p>
                                    <p class="visa-d">Visa Type : @{{ application.visas[visa_index].visatype.name }}</p>
                                    <p class="visa-d">Date of arrival : @{{ application.visas[visa_index].arrival_date.substr(0,10) }}</p>
                                    <p class="visa-d">Entry type : @{{ application.visas[visa_index].entry_type }}</p>
                                    <p class="visa-d">Delivery type : @{{ application.visas[visa_index].delivery_type }}</p>
                                    <p class="visa-d">Cost : @{{ application.visas[visa_index].price }}</p>
                                    
                                    <label class="checkbox-container my-0 mt-3">I confirm that the provided information is  correct
                                        <input type="checkbox" ng-model="confirmed" ng-init="confirmed=false">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <p></p>
                            <div class="modal-footer d-flex justify-content-center">
                                <a class="button btn-nav" href="#" ng-hide="visa_index==0" ng-click="confirmPrevious()">Previous</a>
                                <a class="button btn-nav" href="#" ng-click="confirmNext()">Next</a>
                            </div>
                        </div><!-- /.container -->
                    </div>
                    <!-- End of Confirm Details -->

                    <!-- Payment -->
                    <div class="tab-pane fade" ng-class="{'show active' : tab_index == 5}" id="promocode" role="tabpanel" aria-labelledby="promocode">
                        <div class="row mx-0">
                            <div class="col-xl-8 col-lg-8 mx-auto mt-3">
                                {{-- <h6 class="my-3" id="actual-price">Your application costs <b><span class="cost">@{{ application.total_amt }}{{ config('app.currency') }}</span></b></h6> --}}
                                
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <input class="form-control" type="text" ng-model="phone"
                                            placeholder="Enter phone number (optional)" ng-class="{'invalid': errors.phone }">
                                        <div class="invalid-feedback" ng-show="errors.phone">
                                            @{{ errors.phone[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-5 mx-2">
                                        <a   class="button btn-pay" id="btn-submit-phone" ng-click="submitPhone()">Save Phone
                                            <i class="fa fa-check-circle" aria-hidden="true" style="font-size: large;" ng-show="phone_saved==true"></i></a>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="form-group col-sm-6">
                                        <input class="form-control" type="text" ng-model="application.email" ng-change="emailChanged()"
                                            placeholder="Enter your email" ng-class="{'invalid': errors.email }">
                                            <div class="invalid-feedback" ng-show="errors.email">
                                                @{{ errors.email[0] }}
                                            </div>
                                    </div>
                                    <div class="form-group col-sm-5 mx-2 mb-n2">
                                        <a   class="button btn-pay" id="btn-request-otp" ng-click="requestOTP()">Request OTP
                                            <i class="fa fa-check-circle" aria-hidden="true" style="font-size: large;" ng-show="otp_sent==true"></i></a>
                                        <p class="info-small btn-pay" style="color:red;">Check your spam folder before request again</p>                                      
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <input class="form-control" type="text" ng-model="otp"
                                            placeholder="Enter your OTP received via email" ng-class="{'invalid': errors.otp }">
                                        <div class="invalid-feedback" ng-show="errors.otp">
                                            @{{ errors.otp[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-5 mx-2">
                                        <a   class="button btn-pay" id="btn-submit-otp" ng-click="submitOTP()">Submit OTP
                                            <i class="fa fa-check-circle" aria-hidden="true" style="font-size: large;" ng-show="otp_verified==true"></i></a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <input class="form-control" type="text" ng-model="promocode"
                                            placeholder="Enter promocode if any" ng-class="{'invalid': errors.promocode }">
                                        <div class="invalid-feedback" ng-show="errors.promocode">
                                            @{{ errors.promocode[0] }}
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-5 mx-2">
                                        <a   class="button btn-pay" id="btn-apply-promocode" ng-click="applyPromocode()">Apply Promocode
                                            <i class="fa fa-check-circle" aria-hidden="true" style="font-size: large;" ng-show="promo_applied==true"></i></a>
                                    </div>
                                </div>
                                <div class="alert alert-success" role="alert" ng-show="promo_applied">
                                    Promocode <b>@{{ promocode }}</b> Applied Successfully
                                </div>
                                {{-- <h6 class="my-3" id="payable-price">Amount Payable <b><span class="cost">@{{ application.amount }}{{ config('app.currency') }}</span></b></h6> --}}

                                <div class="d-flex justify-content-center">
                                    <small><p>By Clicking PROCEED You Agree To Our <a target="_blank" href="{{ url('terms') }}"> Terms And Conditions</a></p></small>
                                </div>

                                <div class="modal-footer d-flex justify-content-center">
                                    <a href="#" class="button btn-nav"
                                        ng-click="tab_index=3">Previous</a>
                                    <a href="#" class="button btn-nav center" id="btn-proceed-payment" ng-click="submitApplication()">Proceed</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Payment -->

                    <!-- Response -->
                    <div class="tab-pane fade" ng-class="{'show active' : tab_index == 6}" id="response" role="tabpanel" aria-labelledby="response">
                        @include('welcome.response', [
                            'success_message' => 'Your request/application has been successfully received. We will notify you of further updates. Please note your reference number for future reference'
                        ])
                    </div>
                    <!-- End of Response -->

                </div><!-- /.tab-content -->
            </div><!-- /.container -->
        </div>
    </div>
</div>
<!-- End of Apply modal -->