<div class="container">
    <div class="row">
        <div class="col-lg-3 col-6 my-3">
                <img src="{{ asset('landing/images/eco.jpg') }}" alt="" style="width:100%">
        </div>
        <div class="col-lg-3 col-6 my-3">
                <img src="{{ asset('landing/images/expo.jpg') }}" alt="" style="width:100%">
        </div>
        <div class="col-lg-3 col-6 my-3">
                <img src="{{ asset('landing/images/cul.jpg') }}" alt="" style="width:100%">
        </div>
        <div class="col-lg-3 col-6 my-3">
                <img src="{{ asset('landing/images/hum.jpg') }}" alt="" style="width:100%">
        </div>
    </div>
</div>