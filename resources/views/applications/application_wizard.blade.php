<div class="col-md-12 col-12 mr-auto ml-auto">
    <!--      Wizard container        -->
    <div class="wizard-container" id="application">
        <div class="card card-wizard active" data-color="rose" id="wizardProfile">
            <button type="button" class="close mt-2 position-absolute" style="right: 15px;"
                onclick="$('#application').fadeOut()">
                <span aria-hidden="true" style="font-size: larger;"><b>&times;</b></span>
            </button>
            <form action="" method="" novalidate="novalidate">
                <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                <div class="card-header text-center">
                    <h3 class="card-title">
                        Application details
                    </h3>
                    <h5 class="card-description">This will let you know all about the application.</h5>
                </div>
                <div class="wizard-navigation">
                    <ul class="nav nav-pills">
                        <li class="nav-item" style="width: 33.3333%;">
                            <a class="nav-link active show" href="#about" data-toggle="tab" role="tab"
                                aria-selected="true">
                                General
                            </a>
                        </li>
                        <li class="nav-item" style="width: 33.3333%;">
                            <a class="nav-link" href="#account" data-toggle="tab" role="tab" aria-selected="false">
                                Visa Details
                            </a>
                        </li>
                        <li class="nav-item" style="width: 33.3333%;">
                            <a class="nav-link" href="#address" data-toggle="tab" role="tab" aria-selected="false">
                                Action
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">

                        {{-- General --}}
                        <div class="tab-pane active show" id="about">
                            {{-- <h5 class="info-text"> Let's start with the basic information (with validation)</h5> --}}
                            <div class="row justify-content-around">
                                <div class="col-sm-6">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th colspan="2">
                                                    <h6 class="card-title">Application Details</h6>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>ID</td>
                                                <td> @{{ application.id }}</td>
                                            </tr>
                                            <tr>
                                                <td>Reference No</td>
                                                <td> @{{ application.reference }}</td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>@{{ application.email }} 
                                                    <button class="btn p-0 btn-link btn-success" title="Email Verified" ng-show="application.email_verified_at"><i class="fa fa-check"></i></button>
                                                    <button class="btn p-0 btn-link btn-danger" title="Not Verified" ng-show="application.email_verified_at==null && application.email"><i class="fa fa-times"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Promocode Applied</td>
                                                <td> @{{ application.promocode.name }}</td>
                                            </tr>
                                            <tr>
                                                <td>Application Status</td>
                                                <td> @{{ application.status }}</td>
                                            </tr>
                                            <tr>
                                                <td>Last Updated</td>
                                                <td> @{{ application.updated_at }}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="col-sm-6">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <h6 class="card-title">Payment Details</h6>
                                                </th>
                                                <th>
                                                    <span class="input-group-btn pull-right" ng-click="paymentEdit(application.payment)" ng-hide="payment_edit">
                                                        <label class="btn btn-fab btn-round btn-primary"><i class="material-icons">edit</i></label>
                                                    </span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>ID</td>
                                                <td> @{{ application.payment.id }}</td>
                                            </tr>
                                            <tr>
                                                <td>Transaction No</td>
                                                <td>
                                                    <p ng-hide="payment_edit">@{{ application.payment.transaction_no }}</p>
                                                    <div class="form-group bmd-form-group" ng-show="payment_edit">
                                                        <input type="text" class="form-control" ng-model="payment_edit.transaction_no">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Payment Id</td>
                                                <td>
                                                    <p ng-hide="payment_edit">@{{ application.payment.payment_transaction_id }}</p>
                                                    <div class="form-group bmd-form-group" ng-show="payment_edit">
                                                        <input type="text" class="form-control" ng-model="payment_edit.payment_transaction_id">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Request Amount</td>
                                                <td>
                                                    <p ng-hide="payment_edit">@{{ application.payment.request_amount }}</p>
                                                    <div class="form-group bmd-form-group" ng-show="payment_edit">
                                                        <input type="text" class="form-control" ng-model="payment_edit.request_amount">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Customer Amount</td>
                                                <td>
                                                    <p ng-hide="payment_edit">@{{ application.payment.customer_amount }}</p>
                                                    <div class="form-group bmd-form-group" ng-show="payment_edit">
                                                        <input type="text" class="form-control" ng-model="payment_edit.customer_amount">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Merchant Amount</td>
                                                <td>
                                                    <p ng-hide="payment_edit">@{{ application.payment.merchant_amount }}</p>
                                                    <div class="form-group bmd-form-group" ng-show="payment_edit">
                                                        <input type="text" class="form-control" ng-model="payment_edit.merchant_amount">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Payment Status</td>
                                                <td>
                                                    <span ng-show="application.payment.status=='success' && !payment_edit"
                                                        class="badge badge-success">Success</span>
                                                    <span ng-show="application.payment.status=='pending' && !payment_edit"
                                                        class="badge badge-warning">Pending</span>
                                                    <span ng-show="application.payment.status=='error' && !payment_edit"
                                                        class="badge badge-danger">Error</span>
                                                    
                                                    <select ng-model="payment_edit.status" class="form-control" ng-show="payment_edit">
                                                        <option value="pending">pending</option>
                                                        <option value="success">success</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Last Updated</td>
                                                <td> @{{ application.payment.updated_at }}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">             
                                                    <button class="btn btn-primary pull-right" ng-show="payment_edit"  ng-click="savePayment()">Save</button>
                                                    <button class="btn btn-danger pull-right" ng-show="payment_edit" ng-click="payment_edit=null">Cancel</button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                        {{-- End of General --}}

                        {{-- Visa Details --}}
                        <div class="tab-pane" id="account">
                            <div class="d-flex justify-content-center" ng-show="application.visas.length > 1">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info" ng-show="application.visas.length > 1"
                                        ng-class="{'active': visa_index == 0}" ng-click="visa_index=0">1</button>
                                    <button type="button" class="btn btn-info" ng-show="application.visas.length > 1"
                                        ng-class="{'active': visa_index == 1}" ng-click="visa_index=1">2</button>
                                    <button type="button" class="btn btn-info" ng-show="application.visas.length > 2"
                                        ng-class="{'active': visa_index == 2}" ng-click="visa_index=2">3</button>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-6 col-lg-4">
                                    <div class="d-flex justify-content-around mb-2">
                                        <div class="card p-1">
                                            <a href="@{{ uploads+'/'+application.visas[visa_index].user_image }}" target="_blank">
                                                <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].user_image.thumb | trusted }}"></iframe>
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">Applicatnt Photo</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="card p-1"
                                            ng-show="visa.citizenship=='IQ' || visa.citizenship=='IR' || visa.citizenship=='KW'">
                                            <a href="@{{ uploads+'/'+application.visas[visa_index].national_id }}" target="_blank">
                                                <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].national_id.thumb | trusted }}"></iframe>
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">National ID</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="d-flex justify-content-around mb-2">
                                        <div class="card p-1">
                                            <a href="@{{ uploads+'/'+application.visas[visa_index].passport_first_page }}" target="_blank">
                                                <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].passport_first_page.thumb | trusted }}"></iframe>
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">Passport First Page</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="card p-1">
                                            <a href="@{{ uploads+'/'+application.visas[visa_index].passport_last_page }}" target="_blank">
                                            <iframe class="card-img-top" ng-src="@{{ application.visas[visa_index].passport_last_page.thumb | trusted }}"></iframe>
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">Passport Last Page</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container confirm">
                                <div class="row">
                                    <div class="col-md-12 my-2">
                                        <h6 class="card-title">Personal Details</h6>
                                    </div>
                                    <p class="col-md-6 col-xl-4">First Name :
                                        @{{ application.visas[visa_index].first_name }}</p>
                                    <p class="col-md-6 col-xl-4">Middle Name :
                                        @{{ application.visas[visa_index].middle_name }}</p>
                                    <p class="col-md-6 col-xl-4">Last Name :
                                        @{{ application.visas[visa_index].last_name }}</p>
                                    <p class="col-md-6 col-xl-4">Citizenship :
                                        @{{ application.visas[visa_index].citizen.name }}</p>
                                    <p class="col-md-6 col-xl-4">Country of Residence :
                                        @{{ application.visas[visa_index].residency.name }}</p>
                                    <p class="col-md-6 col-xl-4">Gender :
                                        @{{ application.visas[visa_index].gender }}</p>
                                    <p class="col-md-6 col-xl-4">Marital Status :
                                        @{{ application.visas[visa_index].marital_status }}</p>
                                    <p class="col-md-6 col-xl-4">Spouse Name :
                                        @{{ application.visas[visa_index].spouse_name }}</p>
                                    <p class="col-md-6 col-xl-4">Mother's Name :
                                        @{{ application.visas[visa_index].mother_name }}</p>
                                    <p class="col-md-6 col-xl-4">Father's Name :
                                        @{{ application.visas[visa_index].father_name }}</p>
                                    <p class="col-md-6 col-xl-4">Date of Birth :
                                        @{{ application.visas[visa_index].date_of_birth.substr(0,10) }}</p>
                                    <p class="col-md-6 col-xl-4">Place of Birth :
                                        @{{ application.visas[visa_index].place_of_birth }}</p>
                                    <p class="col-md-6 col-xl-4">Religion :
                                        @{{ application.visas[visa_index].religion }}</p>
                                    <p class="col-md-6 col-xl-4">First Language :
                                        @{{ application.visas[visa_index].language_one.name }}</p>
                                    <p class="col-md-6 col-xl-4">Second Language :
                                        @{{ application.visas[visa_index].language_two.name }}</p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 my-2">
                                        <h6 class="card-title">Passport Details</h6>
                                    </div>
                                    <p class="col-md-6 col-xl-4">Passport Number :
                                        @{{ application.visas[visa_index].passport_number }}</p>
                                    <p class="col-md-6 col-xl-4">Date of Issue :
                                        @{{ application.visas[visa_index].passport_issue_date.substr(0,10) }}</p>
                                    <p class="col-md-6 col-xl-4">place of Issue :
                                        @{{ application.visas[visa_index].passport_issue_place }}</p>
                                    <p class="col-md-6 col-xl-4">Date of expiry :
                                        @{{ application.visas[visa_index].passport_expiry_date.substr(0,10) }}</p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 my-2">
                                        <h6 class="card-title">Visa Details</h6>
                                    </div>
                                    <p class="col-md-6 col-xl-4">Visa Type :
                                        @{{ application.visas[visa_index].visatype.name }}</p>
                                        <p class="col-md-6 col-xl-4">Date of arrival :
                                            @{{ application.visas[visa_index].arrival_date.substr(0,10) }}</p>
                                    <p class="col-md-6 col-xl-4">Entry type :
                                        @{{ application.visas[visa_index].entry_type }}</p>
                                    <p class="col-md-6 col-xl-4">Delivery type :
                                        @{{ application.visas[visa_index].delivery_type }}</p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 my-2">
                                        <h6 class="card-title">Attachments</h6>
                                    </div>
                                    <p class="col-md-6 col-xl-4" ng-repeat="file in application.visas[visa_index].attachments">
                                        <a href="@{{ file.url }}" target="_blank">...@{{ file.name.substr(16) }}</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        {{-- End of Visa Details --}}

                        {{-- Action --}}
                        <div class="tab-pane" id="address">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <h6 class="card-title">Application</h6>
                                                </th>
                                                <th>
                                                    <span class="input-group-btn pull-right" ng-click="edit_response=true" ng-hide="edit_response">
                                                        <label class="btn btn-fab btn-round btn-primary"><i class="material-icons">edit</i></label>
                                                    </span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><h6 class="text-gray">Status</h6></td>
                                                <td>
                                                    <p ng-hide="edit_response">@{{ application.status }}</p>
                                                    <select ng-model="application.status" class="form-control" ng-show="edit_response">
                                                        @foreach (config('data.application_status') as $status)
                                                            <option value="{{ $status }}">{{ $status }}</option>                                                            
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><h6 class="text-gray">Message</h6></td>
                                                <td>
                                                    <p ng-hide="edit_response">@{{ application.message }}</p>
                                                    <div class="form-group bmd-form-group" ng-show="edit_response">
                                                        <textarea class="form-control" ng-model="application.message" rows="3" autocomplete="off" spellcheck="false"></textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><h6 class="text-gray">Comment</h6></td>
                                                <td>
                                                    <p ng-hide="edit_response">@{{ application.comment }}</p>
                                                    <div class="form-group bmd-form-group" ng-show="edit_response">
                                                        <textarea class="form-control" ng-model="application.comment" rows="3" autocomplete="off" spellcheck="false"></textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-show="application.status=='Rejected'">
                                                <td><h6 class="text-gray">Refund</h6></td>
                                                <td>
                                                    <a href="{{ config('foloosi.refund_url') }}@{{ application.payment.transaction_no }}" target="_blank" class="btn btn-info">Refund</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    {{-- <div class="form-check pull-right" ng-show="edit_response">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" ng-model="application.send_mail">Notify user via email
                                                            <span class="form-check-sign mt-3">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div> --}}
                                                </td>
                                                <td>             
                                                    <button class="btn btn-primary pull-right" ng-show="edit_response"  ng-click="saveApplication()">Save</button>
                                                    <button class="btn btn-danger pull-right" ng-show="edit_response" ng-click="edit_response=false">Cancel</button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th colspan="2">
                                                    <h6 class="card-title">Attachments</h6>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="file in application.attachments">
                                                <td><a href="@{{ file.url }}" target="_blank">@{{ file.name }}</a></td>
                                                <td><a   ng-click="deleteFile(file)"><i class="material-icons">delete</i></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="form-group form-file-upload form-file-multiple">
                                        <input id="file-upload" class="form-control" type="file" onchange="angular.element(this).scope().fileUpload(event, 'file')">
                                        <div class="input-group">
                                            <input type="text" ng-model="file.name" onclick="$('#file-upload-label').click()"  class="form-control inputFileVisible" placeholder="Choose file to upload">
                                            <span class="input-group-btn">
                                                <label id="file-upload-label" for="file-upload" class="btn btn-fab btn-round btn-primary"><i class="material-icons">attach_file</i></label>
                                            </span>
                                        </div>
                                        <div class="invalid-feedback">@{{ errors.file[0] }}</div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="card ghostwhite">
                                        {{-- <div class="card-body px-3 text-left">
                                            <h4 class="text-info">Send Payment Link</h4>
                                            <p>Deleting the application will delete its related resources including accounts, comments etc.</p>
                                            <a href="#" class="btn btn-info">Send</a>
                                        </div> --}}
                                        
                                        <div class="card-body px-3 text-left">
                                            <h4 class="text-info">Send Notification</h4>
                                            
                                            <select ng-model="notification.template" class="form-control">
                                                <option value="">Choose Notification</option>
                                                @foreach (\App\Models\MailTemplate::all() as $mail)
                                                    <option value="{{ $mail->id }}">{{ $mail->name }}</option>                                                            
                                                @endforeach
                                            </select>
                                            <textarea class="form-control" ng-model="notification.comments" cols="30" rows="10" placeholder="Enter if any comments"></textarea>
                                            <a href="#" ng-click="notify()" class="btn btn-info">Send</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card ghostwhite">
                                        <div class="card-body px-3 text-left">
                                            <h4 class="text-danger">Delete Application</h4>
                                            <p>Deleting the application will delete its related resources including accounts, comments etc.</p>
                                            <a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- End of Action --}}
                    </div>
                </div>
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd disabled"
                            name="previous" value="Previous">
                    </div>
                    <div class="ml-auto">
                        <input type="button" class="btn btn-next btn-fill btn-rose btn-wd" name="next" value="Next"
                            style="">
                        <input type="button" class="btn btn-finish btn-fill btn-rose btn-wd" name="finish"
                            value="Close" style="display: none;" onclick="$('#application').fadeOut()">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
    <!-- wizard container -->
</div>
{{-- Delete Modal --}}
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Do you want to delete this application? This process cannot be undone
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" ng-click="deleteApplication()" class="btn btn-danger">Delete</button>
        </div>
      </div>
    </div>
  </div>
{{-- End of Delete Modal --}}
@push('js')

<script>
    $(document).ready(function () {
        // Initialise the wizard
        // $('#application').hide();
        demo.initMaterialWizard();
        setTimeout(function () {
            $('.card.card-wizard').addClass('active');
        }, 600);
    });
    demo = {
        initMaterialWizard: function () {
            // Code for the Validator
            var $validator = $('.card-wizard form').validate({
                rules: {
                    // firstname: {
                    //     required: true,
                    //     minlength: 3
                    // },
                    // lastname: {
                    //     required: true,
                    //     minlength: 3
                    // },
                    // email: {
                    //     required: true,
                    //     minlength: 3,
                    // }
                },

                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass(
                        'has-danger');
                },
                success: function (element) {
                    $(element).closest('.form-group').removeClass('has-danger').addClass(
                        'has-success');
                },
                errorPlacement: function (error, element) {
                    $(element).append(error);
                }
            });



            // Wizard Initialization
            $('.card-wizard').bootstrapWizard({
                'tabClass': 'nav nav-pills',
                'nextSelector': '.btn-next',
                'previousSelector': '.btn-previous',

                onNext: function (tab, navigation, index) {
                    var $valid = $('.card-wizard form').valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                },

                onInit: function (tab, navigation, index) {
                    //check number of tabs and fill the entire row
                    var $total = navigation.find('li').length;
                    var $wizard = navigation.closest('.card-wizard');

                    $first_li = navigation.find('li:first-child a').html();
                    $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
                    $('.card-wizard .wizard-navigation').append($moving_div);

                    refreshAnimation($wizard, index);

                    $('.moving-tab').css('transition', 'transform 0s');
                },

                onTabClick: function (tab, navigation, index) {
                    var $valid = $('.card-wizard form').valid();

                    if (!$valid) {
                        return false;
                    } else {
                        return true;
                    }
                },

                onTabShow: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;

                    var $wizard = navigation.closest('.card-wizard');

                    // If it's the last tab then hide the last button and show the finish instead
                    if ($current >= $total) {
                        $($wizard).find('.btn-next').hide();
                        $($wizard).find('.btn-finish').show();
                    } else {
                        $($wizard).find('.btn-next').show();
                        $($wizard).find('.btn-finish').hide();
                    }

                    button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                    setTimeout(function () {
                        $('.moving-tab').text(button_text);
                    }, 150);

                    var checkbox = $('.footer-checkbox');

                    if (!index == 0) {
                        $(checkbox).css({
                            'opacity': '0',
                            'visibility': 'hidden',
                            'position': 'absolute'
                        });
                    } else {
                        $(checkbox).css({
                            'opacity': '1',
                            'visibility': 'visible'
                        });
                    }

                    refreshAnimation($wizard, index);
                }
            });


            // Prepare the preview for profile picture
            $("#wizard-picture").change(function () {
                readURL(this);
            });

            $('[data-toggle="wizard-radio"]').click(function () {
                wizard = $(this).closest('.card-wizard');
                wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
                $(this).addClass('active');
                $(wizard).find('[type="radio"]').removeAttr('checked');
                $(this).find('[type="radio"]').attr('checked', 'true');
            });

            $('[data-toggle="wizard-checkbox"]').click(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).find('[type="checkbox"]').removeAttr('checked');
                } else {
                    $(this).addClass('active');
                    $(this).find('[type="checkbox"]').attr('checked', 'true');
                }
            });

            $('.set-full-height').css('height', 'auto');

            //Function to show image before upload

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(window).resize(function () {
                $('.card-wizard').each(function () {
                    $wizard = $(this);

                    index = $wizard.bootstrapWizard('currentIndex');
                    refreshAnimation($wizard, index);

                    $('.moving-tab').css({
                        'transition': 'transform 0s'
                    });
                });
            });

            function refreshAnimation($wizard, index) {
                $total = $wizard.find('.nav li').length;
                $li_width = 100 / $total;

                total_steps = $wizard.find('.nav li').length;
                move_distance = $wizard.width() / total_steps;
                index_temp = index;
                vertical_level = 0;

                mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    vertical_level = parseInt(index / 2);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        },

    }

</script>
@endpush
