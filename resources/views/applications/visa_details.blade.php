<div class="col-md-12 col-12 mr-auto ml-auto">
        <!--      Wizard container        -->
        <div class="wizard-container" id="application" style="display:none;">
            <div class="card card-wizard active" data-color="rose" id="wizardProfile">
                <form action="" method="" novalidate="novalidate">
                    <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->

                    <div class="container justify-content-end">
                        <button type="button" class="close mt-2" onclick="$('#application').fadeOut()"
                            style="text-align: end;">
                            <span aria-hidden="true" style="font-size: larger;"><b>&times;</b></span>
                        </button>
                    </div>
                    <h3 class="card-title">
                        Application details
                    </h3>
                    <div class="card-header text-center">
                        <div class="wizard-navigation">
                            <ul class="nav nav-pills">
                                <li class="nav-item" style="width: 33.3333%;">
                                    <a class="nav-link active" href="#about" data-toggle="tab" role="tab">
                                        About
                                    </a>
                                </li>
                                <li class="nav-item" style="width: 33.3333%;">
                                    <a class="nav-link" href="#account" data-toggle="tab" role="tab">
                                        Account
                                    </a>
                                </li>
                                <li class="nav-item" style="width: 33.3333%;">
                                    <a class="nav-link" href="#address" data-toggle="tab" role="tab">
                                        Address
                                    </a>
                                </li>
                            </ul>
                            <div class="moving-tab"
                                style="width: 222.443px; transform: translate3d(-8px, 0px, 0px); transition: transform 0s ease 0s;">
                                About
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show">

                                <div class="d-flex justify-content-center">
                                    <p class="p-2"><b>@{{ visa_index + 1 }} of @{{ application.visas.length }}</b></p>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="d-flex justify-content-around mb-2">
                                            <div class="card p-1">
                                                <img id="img-passport1" class="card-img-top"
                                                    ng-src="@{{ uploads+'/'+application.visas[visa_index].user_image }}">
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">Applicatnt Photo</span>
                                                </div>
                                            </div>
                                            <div class="card p-1"
                                                ng-show="visa.citizenship=='IQ' || visa.citizenship=='IR' || visa.citizenship=='KW'">
                                                <img id="img-passport2" class="card-img-top"
                                                    ng-src="@{{ uploads+'/'+application.visas[visa_index].national_id }}">
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">National ID</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-4">
                                        <div class="d-flex justify-content-around mb-2">
                                            <div class="card p-1">
                                                <img id="img-passport1" class="card-img-top"
                                                    ng-src="@{{ uploads+'/'+application.visas[visa_index].passport_first_page }}">
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">Passport First Page</span>
                                                </div>
                                            </div>
                                            <div class="card p-1">
                                                <img id="img-passport2" class="card-img-top"
                                                    ng-src="@{{ uploads+'/'+application.visas[visa_index].passport_last_page }}">
                                                <div class="card-body p-0 row justify-content-center align-items-end">
                                                    <span class="badge badge-secondary mt-1">Passport Last Page</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container confirm">
                                    <div class="row">
                                        <div class="col-md-12 my-2">
                                            <p><b>Personal Details</b></p>
                                        </div>
                                        <p class="col-md-6 col-xl-4">First Name :
                                            @{{ application.visas[visa_index].first_name }}</p>
                                        <p class="col-md-6 col-xl-4">Middle Name :
                                            @{{ application.visas[visa_index].middle_name }}</p>
                                        <p class="col-md-6 col-xl-4">Last Name :
                                            @{{ application.visas[visa_index].last_name }}</p>
                                        <p class="col-md-6 col-xl-4">Citizenship :
                                            @{{ application.visas[visa_index].citizenship }}</p>
                                        <p class="col-md-6 col-xl-4">Country of Residence :
                                            @{{ application.visas[visa_index].residence }}</p>
                                        <p class="col-md-6 col-xl-4">E-mail : example@gmail.com</p>
                                        <p class="col-md-6 col-xl-4">Gender :
                                            @{{ application.visas[visa_index].gender }}</p>
                                        <p class="col-md-6 col-xl-4">Marital Status :
                                            @{{ application.visas[visa_index].marital_status }}</p>
                                        <p class="col-md-6 col-xl-4">Spouse Name :
                                            @{{ application.visas[visa_index].spouse_name }}</p>
                                        <p class="col-md-6 col-xl-4">Mother's Name :
                                            @{{ application.visas[visa_index].mother_name }}</p>
                                        <p class="col-md-6 col-xl-4">Father's Name :
                                            @{{ application.visas[visa_index].father_name }}</p>
                                        <p class="col-md-6 col-xl-4">Date of Birth :
                                            @{{ application.visas[visa_index].date_of_birth }}</p>
                                        <p class="col-md-6 col-xl-4">Place of Birth :
                                            @{{ application.visas[visa_index].place_of_birth }}</p>
                                        <p class="col-md-6 col-xl-4">Religion :
                                            @{{ application.visas[visa_index].religion }}</p>
                                        <p class="col-md-6 col-xl-4">First Language :
                                            @{{ application.visas[visa_index].first_language }}</p>
                                        <p class="col-md-6 col-xl-4">Second Language :
                                            @{{ application.visas[visa_index].second_language }}</p>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 my-2">
                                            <p><b>Passport Details</b></p>
                                        </div>
                                        <p class="col-md-6 col-xl-4">Passport Number :
                                            @{{ application.visas[visa_index].passport_number }}</p>
                                        <p class="col-md-6 col-xl-4">Date of Issue :
                                            @{{ application.visas[visa_index].passport_issue_date }}</p>
                                        <p class="col-md-6 col-xl-4">place of Issue :
                                            @{{ application.visas[visa_index].passport_issue_place }}</p>
                                        <p class="col-md-6 col-xl-4">Date of expiry :
                                            @{{ application.visas[visa_index].passport_expiry_date }}</p>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 my-2">
                                            <p><b>Visa Details</b></p>
                                        </div>
                                        <p class="col-md-6 col-xl-4">Date of arrival :
                                            @{{ application.visas[visa_index].arrival_date }}</p>
                                        <p class="col-md-6 col-xl-4">Entry type :
                                            @{{ application.visas[visa_index].entry_type }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="mr-auto">
                                <input type="button" class="btn btn-next btn-fill btn-rose btn-wd" id="previous"
                                    value="Previous" ng-click="confirmPrevious()" ng-hide="visa_index==0">
                            </div>
                            <div class="ml-auto">
                                <input type="button" class="btn btn-next btn-fill btn-rose btn-wd" id="next"
                                    value="Next" ng-click="confirmNext()"
                                    ng-show="(application.visas.length > visa_index+1)">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                </form>
            </div>
        </div>
        <!-- wizard container -->
    </div>