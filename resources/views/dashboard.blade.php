@extends('layouts.admin')

@section('content')
{{-- Cards --}}
<div class="row" ng-show="data">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">library_books</i>
                </div>
                <p class="card-category"><b>Applications</b></p>
                <h3 class="card-title">@{{ data.approved_applications }}/@{{ data.total_applications }}
                    {{-- <small>GB</small> --}}
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">library_books</i>
                    <a href="{{ url('applications') }}">More details</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">store</i>
                </div>
                <p class="card-category"><b>Revenue</b></p>
                <h3 class="card-title">@{{ data.revenue }} AED</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i> Last Month
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                </div>
                <p class="card-category"><b>Visas</b></p>
                <h3 class="card-title">@{{ data.approved_visas }}/@{{ data.total_visas }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">local_offer</i>
                </div>
                <p class="card-category"><b>Promocodes</b></p>
                <h3 class="card-title">@{{ data.promocodes }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                </div>
            </div>
        </div>
    </div>
</div>
{{-- End of Cards --}}
<div class="row" ng-show="data">
    <div class="col-md-6">
        <div class="card ">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons"></i>
                </div>
                <h4 class="card-title">Global Sales by Customer Citizenship</h4>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive table-sales">
                            <table class="table">
                                <tbody>
                                    <tr ng-repeat="sale in data.global_sales_citi">
                                        <td>
                                          <div class="flag"><img src="https://www.countryflags.io/@{{ sale.citizenship }}/shiny/64.png"></div>                                            
                                        </td>
                                        <td>@{{ sale.name }}</td>
                                        <td class="text-right">
                                            @{{ sale.cost }} AED
                                        </td>
                                        <td class="text-right">
                                            @{{ sale.cost/data.total_sales*100 | number:2 }}%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card ">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons"></i>
                </div>
                <h4 class="card-title">Global Sales by Customer Residence</h4>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive table-sales">
                            <table class="table">
                                <tbody>
                                    <tr ng-repeat="sale in data.global_sales_resi">
                                        <td>
                                          <div class="flag"><img src="https://www.countryflags.io/@{{ sale.residence }}/shiny/64.png"></div>                                            
                                        </td>
                                        <td>@{{ sale.name }}</td>
                                        <td class="text-right">
                                            @{{ sale.cost }} AED
                                        </td>
                                        <td class="text-right">
                                            @{{ sale.cost/data.total_sales*100 | number:2 }}%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, $window) {
        $scope.applications = null;
        $scope.init = function () {
            $http.get("api/dashboard_data")
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.data = response.data;
                }, function (response) {
                    console.error("ERROR", response);
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()
    });

</script>
@endpush
