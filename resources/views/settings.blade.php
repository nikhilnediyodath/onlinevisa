@extends('layouts.admin')

@section('content')

<div class="row">

    <div class="col-md-6 mx-auto">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title" ng-init="action='Add'">@{{ action }} Visa Type</h4>
                {{-- <p class="card-category">Complete your profile</p> --}}
            </div>
            <div class="card-body">
                <div class="alert alert-danger" ng-show="error_messages">
                    <span ng-repeat="error_message in error_messages">@{{ error_message }}</span>
                </div>
                <form>
                    <div class="row mt-3">
                        <div class="col-md-12">
                                <label for="">Visa Type Name</label>
                                <input type="text" class="form-control" ng-model="visa_type.name"> 
                        </div>
                        <div class="col-md-12">
                            <label for="">Multiple Entry Amount</label>
                            <input type="text" class="form-control" ng-model="visa_type.multiple_entry_amount">   
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label>Single Entry Amount</label>
                                <input type="text" class="form-control" ng-model="visa_type.single_entry_amount">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="saveVisaType()">Submit</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Visa Types</h4>
                {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Visas</th>
                            <th>Status</th>
                            <th>Action</th>
                        </thead>
                        <tbody ng-show="visa_types">
                            <tr ng-repeat="visa_type in visa_types">
                                <td>@{{ visa_type.id }}</td>
                                <td>@{{ visa_type.name }}</td>
                                <td>@{{ visa_type.count }}</td>
                                <td>@{{ visa_type.status }}</td>
                                <td>
                                    <a   ng-click="blockVisaType(visa_type)" ng-show="visa_type.status=='Active'" title="Block">
                                        <i class="material-icons">highlight_off</i></a>
                                    <a   ng-click="unblockVisaType(visa_type)" ng-show="visa_type.status=='Blocked'" title="Unblock">
                                        <i class="material-icons">restore</i></a>
                                    <a   ng-click="deleteVisaType(visa_type)" ng-show="visa_type.count==0" title="Delete">
                                        <i class="material-icons">delete</i></a>
                                </td>
                            </tr>      
                        </tbody>  
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Visa Counter</h4>
                <p class="card-category">Edit counters shown in landing page</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>Name</th>
                            <th>Value</th>
                            <th>Last Updated</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="counter in counters">
                                <td>@{{ counter.name }}</td>
                                <td>@{{ counter.text }}</td>
                                <td>@{{ counter.updated_at }}</td>
                                <td>
                                    <a   ng-click="editCounter(counter)" title="Edit">
                                        <i class="material-icons">edit</i></a>
                                </td>
                            </tr>      
                        </tbody>  
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 mx-auto" ng-show="counter_edit">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Edit Counter</h4>
                {{-- <p class="card-category">Complete your profile</p> --}}
            </div>
            <div class="card-body">
                <div class="alert alert-danger" ng-show="counter_errors">
                    <span ng-repeat="error_message in counter_errors">@{{ error_message }}</span>
                </div>
                <form>
                    <div class="row mt-3">
                        <div class="col-md-12">
                                <label for="">Name</label>
                                <input type="text" class="form-control" ng-model="counter_edit.name"> 
                        </div>
                        <div class="col-md-12">
                            <label for="">Value</label>
                            <input type="text" class="form-control" ng-model="counter_edit.text">   
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="saveCounter(counter_edit)">Submit</button>
                    <button type="submit" class="btn btn-danger pull-right" ng-click="counter_edit = null">Cancel</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, $window) {
        $scope.init = function () {
            $http.get("api/get_settings_data", $scope.visa)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.visa_types = response.data.visa_types;
                    $scope.counters = response.data.counters;
                }, function (response) {
                    console.error("ERROR", response);
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()

        $scope.saveVisaType = function () {
            $http.post("api/visa_types", $scope.visa_type)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.visa_type = null;
                    $scope.visa_types = null;
                    $scope.visa_types = response.data;
                    $scope.error_messages = null;
                    toast.success('Successfully Updated');
                }, function (response) {
                    console.error('response', response);
                   
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function(value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    }else  toast.error(response.statusText);
                });
        }
        $scope.blockVisaType = function (visa_type) {
            $scope.visa_type = angular.copy(visa_type);
            $scope.visa_type.status = 'Blocked';
            $scope.saveVisaType();
        }
        $scope.unblockVisaType = function (visa_type) {
            $scope.visa_type = angular.copy(visa_type);
            $scope.visa_type.status = 'Active';
            $scope.saveVisaType();
        }
        $scope.deleteVisaType = function (visa_type) {
            $http.delete("api/visa_type/" + visa_type.id)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.visa_type = $scope.visa_type;
                    $scope.visa_types = null;
                    $scope.visa_types = response.data;
                    toast.success('Successfully Deleted');
                }, function (response) {
                    console.error('response', response);
                    toast.error(response.statusText);
                    if (response.status == -1)
                        toast.error('Network Error !!');
                });
        }
        $scope.editCounter = function (counter) {
            $scope.counter_edit = angular.copy(counter);
        }
        $scope.saveCounter = function (setting) {
            $http.put("api/setting", setting)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.counter_edit = null;
                    $scope.counters = response.data;
                    $scope.counter_errors = null;
                    toast.success('Successfully Updated');
                }, function (response) {
                    console.error('response', response);                   
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.counter_errors = [];
                        angular.forEach(response.data.errors, function(value, key) {
                            $scope.counter_errors.push(value[0]);
                        });
                        toast.error(response.data.message);
                    }else  toast.error(response.statusText);
                });
        }
    });
</script>
@endpush