@extends('layouts.landing')

@section('meta_tags')
    <meta property="og:description" content="Get the best visa services in UAE with VisaByMakto. Simplify your UAE visit visa process and experience hassle-free travel. Contact us now!" />
    <meta property="og:title" content="Visa Services in UAE | UAE Visit Visa - VisaByMakto" />
    <meta name="description" content="Looking for the best visa services in UAE? VisaByMakto specialises in UAE visit visas. Trust our expert team for hassle-free visa processing. Contact us now!" />
    <meta name="keywords" content="visa services in UAE, UAE visit visa, visa services in UAE, Visa renewal UAE ,Visa application UAE,dubai visit visa extension,uae visit visa renewal">
    <meta name="title" content="Best Visit Visa Services in Dubai, UAE | VisaByMakto">

@endsection

@section('tagline')
    <section id="tagline" class="mt-n5">
        <h1 style="font-family: segoe script;">Marhaba!</h1>
        <h1>We are <span style="color: yellow;font-family: segoe script;"> here</span>
            to help you get <span style="color: #6cc091;font-family: segoe script;">there</span> </h1>
    </section>
    @push('css')
        <style>
            #tagline h1, #tagline h1 b{
                font-size: 2rem;
                color: white;
                line-height: 1.3em;
            }
            @media screen and (max-width: 1280px){
                #tagline h1, #tagline h1 b{
                    font-size: 2rem;
                }
            }
            @media screen and (max-width: 980px){
                #tagline h1, #tagline h1 b{
                    font-size: 1.6rem;
                }
            }
            @media screen and (max-width: 736px){
                #tagline h1, #tagline h1 b{
                    font-size: 1.3rem;
                }
            }
        </style>
    @endpush
@endsection

@section('content')
    <div class="flex">
        <div>
            <a href="#" ng-click="showApplyForm(1)">
                <span class="icon fa-clock-o"></span>
                <h3>30 Days Visa</h3>
                <p>Starting from <b style="color: yellow"> 279 AED</b></p>
            </a>
        </div>
        <div>
            <a href="#" ng-click="showApplyForm(2)">
                <span class="icon fa-clock-o"></span>
                <h3>90 Days Visa</h3>
                <p>Starting from <b style="color: yellow"> 629 AED</b></p>
            </a>
        </div>
        <div>
            <a href="tel:{{ str_replace(' ', '', config('app.contact')) }}">
                <span class="icon fa-phone"></span>
                <h3><strong class="text-white">{{ config('app.contact') }}</strong></h3>
                <p>Bahrain, Kuwait, Oman & Saudi Visa</p>
            </a>
        </div>
    </div>

    <footer>
        <a href="#" data-toggle="modal" data-target="#applyModal" data-index="0" class="button m-2"
           style="width: 241px;">Apply
            For Visa</a>
        <a href="#" data-toggle="modal" data-target="#trackModal" class="button m-2" style="width: 241px;">Track
            Application</a>
    </footer>

@endsection

@section('modals')
    @include('welcome.payment_modal')
    @include('welcome.apply_modal')
    @include('welcome.track_modal')
    {{-- @include('welcome.popup') --}}
@endsection

@section('bottom')
    <div class="fixed-bottom" style="bottom: 15px;">
        <a href="https://wa.me/{{ str_replace([' ', '+'], '',config('app.contact')) }}" class="whatsapp" target="_blank">
            <img src="{{ asset('landing/images/whatsapp.png') }}">
        </a>
    </div>
    @include('welcome.counter')
    <!-- @include('welcome.images')  -->

@endsection
@section('middle')
    <div class="welome-to-visa-wrapper">
        <div class="container">
            <h2 class="text-center">Welcome to Visa ByMakto</h2>
            <p class="text-center">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                aliquip ex ea commodo consequat. Duis aute
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                    <div class="welcome-visa-card text-center">
                        <i class="fa fa-user fa-3x" aria-hidden="true"></i>
                        <h5>Apply for Visa</h5>
                        <p>You Apply for new UAE Visa</p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                    <div class="welcome-visa-card text-center">
                        <i class="fa fa-list-alt fa-3x" aria-hidden="true"></i>
                        <h5>Backend</h5>
                        <p>Our backend team carefully review your application</p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                    <div class="welcome-visa-card text-center">
                        <i class="fa fa-cogs fa-3x" aria-hidden="true"></i>
                        <h5>Cash</h5>
                        <p>You receive the payment link after the review from us</p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                    <div class="welcome-visa-card text-center">
                        <i class="fa fa-list-alt fa-3x" aria-hidden="true"></i>
                        <h5>Dispatch</h5>
                        <p>You UAE Visa is emailed to your registered email id</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dedicated-wrapper py-5">
        <div class="container">
            <div class="row">
                <div class="col-10 col-sm-6 col-md-4 mb-3">
                    <h3>
                        Dedicated <br />
                        teams
                    </h3>
                    <p>
                        We has teams focused on building products that everyone can use.
                        These teams work with engineers, designers and others across the
                        company to help ensure that our products are built with
                        accessibility in mind.
                    </p>
                </div>
                <div class="col-10 col-sm-6 col-md-4 mb-3">
                    <h3>
                        Research and <br />
                        advocacy
                    </h3>
                    <p>
                        We conduct research with people who have accessibility needs,
                        and work with experts in the community. If you're interested in
                        participating in a session about accessibility at, fill in our
                        <a href="#">accessibility research form</a>.
                    </p>
                </div>
                <div class="col-10 col-sm-6 col-md-4 mb-3">
                    <h3>
                        Digital accessibility <br />
                        standards
                    </h3>
                    <p>
                        We're working towards the digital accessibility standards laid
                        out by the
                        <a href="">Web Content Accessibility Guidelines</a>. We're also
                        investing in automated testing tools to help us catch more
                        issues.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="frequently-asked-wrapper py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <h2 class="mb-4">Frequently Asked <br />
                        Questions
                    </h2>
                    <p>FAQs About <b>UAE Visa Services</b></p>
                </div>
                <div class="col-12 col-md-8">
                    <div class="accordion asked-questions-wrapper" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <div class="mb-0 d-flex align-items-center">
                                    <h4 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <button class="btn btn-link" type="button">What is the UAE visa cost?</button>
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </h4>
                                </div>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">varied visa types, periods of stay, and supplemental services will result in varied entrance
                                    fees to the UAE. Contact Visa by Makto for a quote personalized to your specific
                                    requirements.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <div class="mb-0 d-flex align-items-center">
                                    <h4 class="mb-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <button class="btn btn-link collapsed" type="button">How many days does a UAE visa take?</button>
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">Visa processing periods in the UAE may vary depending on the kind of visa and the
                                    immigration office's current caseload. It might take a few days to a number of weeks. Visa
                                    by Makto keeps you informed of the progress of your application and ensures that it is
                                    completed as promptly and effectively as possible.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <div class="mb-0 d-flex align-items-center">
                                    <h4 class="mb-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <button class="btn btn-link collapsed" type="button">Where can I apply for visa services in UAE?</button>
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">Citizens of the United Arab Emirates (UAE) may apply for visas via either official
                                    government channels or approved third-party service providers such as Visa by Makto. We
                                    will apply for your visa on your behalf, saving you the effort and time.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <div class="mb-0 d-flex align-items-center">
                                    <h4 class="mb-0" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <button class="btn btn-link collapsed" type="button">Is UAE issuing visas now?</button>
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </h4>
                                </div>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body">The UAE is currently issuing visas to people who meet the requirements. It is, nevertheless,
                                    essential to be informed of any changes in travel regulations. Visa by Makto tracks the
                                    status of visa approvals and ensures that your application is processed in accordance with
                                    the current laws.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="client-wrapper">
        <div class="container">
            <div class="row">
                @foreach(['cul1.png', 'eco1.png','expo1.png','hum1.png'] as $image)
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="px-5 py-3">
                            <img src="{{ asset('landing/images/'.$image)}}" alt="clint-imgs" class="img w-100">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script>
        app.controller('myCtrl', function ($scope, $http) {
            $scope.title = ['Visa Details', 'Personal Details', 'Passport Details', 'Summary', 'Payment', 'Confirmation'];
            $scope.max_visa_count = {{ config('app.max_visa_count') }};
            $scope.upload_progress = 0;
            var fp1;
            function pay(options){
                fp1 = new Foloosipay(options);
                fp1.open();
            }
            function closePay(){
                fp1.close();
            }
            $scope.initVisa = function () {
                $scope.visa = {
                    application_id: $scope.application.id,
                    citizenship:'',
                    residence:'',
                    visa_type:'',
                    entry_type:'Single Entry',
                    delivery_type: 'Normal Delivery',
                    gender : 'Male',
                    marital_status:'Single',
                    passport_first_page: { thumb: "{{ url('preview') }}"},
                    passport_last_page: { thumb: "{{ url('preview') }}"},
                    user_image: { thumb: "{{ url('preview') }}"},
                    national_id: { thumb: "{{ url('preview') }}"},
                    attachments: []
                };
                $scope.errors = {};
                $scope.success = {};
                $scope.price = 0;
            }
            $scope.proceedPayment = function (id) {
                $http.post("api/proceed_payment",{id:id})
                    .then(function (response) {
                        console.log("SUCCESS proceedPayment", response);
                        pay(response.data);
                        console.log('proceedPayment fp1', fp1);
                    }, function (response) {
                        console.error("ERROR proceedPayment", response);
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        } else
                            toastError('Oops! Something went wrong');
                    });
            }
            $scope.init = function () {
                $scope.application = {
                    id : null,
                    visas:[{
                        passport_first_page: { thumb: "{{ url('preview') }}"},
                        passport_last_page: { thumb: "{{ url('preview') }}"},
                        user_image: { thumb: "{{ url('preview') }}"},
                        national_id: { thumb: "{{ url('preview') }}"}
                    }]
                };
                @env('ui')
                    $scope.price = 432;
                $scope.application.reference = 'REF123456789';
                $scope.application.total_amt =  321;
                $scope.application.amount = 321;
                $scope.payment_success = true;
                $scope.payment_error = true;
                @endenv

                    $scope.visa_index = 0;
                $scope.tab_index = 0;
                $http.get("api/get_data", $scope.visa)
                    .then(function (response) {
                        console.log("SUCCESS", response);
                        $scope.languages = response.data.languages;
                        $scope.visa_types = response.data.visa_types;
                        $scope.countries = response.data.countries;
                        $scope.data = response.data;
                        $scope.initVisa()
                    }, function (response) {
                        console.error("ERROR", response);
                        if (response.status == -1) toastError('Network Error !!')
                    });

                // Payment
                if (window.location.pathname == "/pay"){
                    $scope.app_id = (new URL(window.location.href)).searchParams.get('id');
                    if ($scope.app_id == null) return;
                    $scope.proceedPayment($scope.app_id);

                    // $('#paymentModal').modal('show');
                    //     $scope.payment_success = true;
                    //     $scope.payment_error = true;
                }

            }
            $scope.init()

            $scope.nextTab = function (url, next_tab = null) {
                if (next_tab == null) next_tab = $scope.tab_index+1;
                @env('ui') $scope.tab_index = next_tab; return; @endenv

                console.log('visa', $scope.visa);
                $http.post("api/"+url, $scope.visa)
                    .then(function (response) {
                        console.log("SUCCESS", response);
                        $scope.errors = null;
                        if (response.data.application) $scope.application = response.data.application;
                        if (response.data.visa) $scope.visa = response.data.visa;
                        if (next_tab == 1) { // Add more person
                            if ($scope.application.visas.length == $scope.max_visa_count) {
                                toastError('Only '+$scope.max_visa_count+' visa are allowed'); return;
                            } else $scope.initVisa()
                        }
                        $scope.tab_index = next_tab;
                    }, function (response) {
                        console.error("ERROR", response);
                        if (response.status == 422) $scope.errors = response.data.errors;
                        else if (response.status == -1) toastError('Network Error !!')
                    });
            };
            $scope.confirmNext = function () {
                console.log('confirmed', $scope.confirmed);
                if ($scope.application.visas.length > $scope.visa_index+1){
                    $scope.visa_index ++;
                } else if ($scope.confirmed) {
                    $scope.tab_index++;
                } else {
                    toastError('Please confirm the details');
                }
            }
            $scope.confirmPrevious = function () {
                if ($scope.visa_index > 0 ) $scope.visa_index --;
            }
            $scope.fileUpload = function (e, field, preview = null, param_name = 'file') {
                var formData = new FormData();
                formData.append(param_name, e.target.files[0]);
                formData.append('file_type', field);
                if (e.target.files[0].size > 5242880){
                    toastError('Maximum size is 5MB');
                    return;
                }
                if (field == 'attachments'){
                    console.log('doc_type', $scope.doc_type);
                    if (!$scope.doc_type){
                        toastError('Choose document type');
                        return;
                    }
                    formData.append('doc_type', $scope.doc_type);
                }
                // toastr.uploading();
                $http({
                    method: 'POST',
                    url: "api/file_upload",
                    data: formData,
                    headers: { 'Content-Type': undefined },
                    uploadEventHandlers: { progress: function(e) {
                            console.log('uploading', $scope.upload_progress);
                            $scope.upload_progress = Math.round((e.loaded/e.total)*100);
                        }}
                }).then(function (response) {
                    console.log("SUCCESS", response);
                    $('#loader').fadeOut();
                    if ($scope.errors){
                        $scope.errors[field + '.id'] = null;
                    } else $scope.errors = null;
                    if (field == 'attachments') {
                        if ($scope.errors)
                            $scope.errors[field] = null;
                        $scope.visa.attachments.push(response.data);
                    } else {
                        $scope.visa[field] = response.data;
                    }

                    if ($scope.success) $scope.success[field] = true;
                    else $scope.success = {[field] : true};
                    console.log('$scope.success', $scope.success);

                    // toastr.uploaded();
                    $scope.upload_progress = 0;
                    console.log('$scope.visa', $scope.visa);
                    toastScucess('File uploaded successfully')
                }, function (response) {
                    console.log("ERROR", response);
                    // toastr.uploaded();
                    if (response.status == 422) {
                        if ($scope.errors) $scope.errors[field + '.id'] = response.data.errors[param_name];
                        else $scope.errors = {[field + '.id'] : response.data.errors[param_name]};
                        console.log('$scope.errors', $scope.errors);
                    } else if (response.status == -1) toastError('Network Error !!')
                });
            }
            $scope.deleteFile = function (file) {
                console.log('file', file);
                $scope.visa.attachments.splice($scope.visa.attachments.indexOf(file), 1);
            }
            $scope.getPrice = function () {
                console.info('visa', $scope.visa);
                $http.post("api/get_price", $scope.visa)
                    .then(function (response) {
                        console.log("SUCCESS getPrice", response);
                        $scope.price = response.data.amount;
                    }, function (response) {
                        console.error("ERROR getPrice", response);
                        $scope.price = 0;
                        if (response.status == -1) toastError('Network Error !!');
                    });
            }
            $scope.emailChanged = function () {
                $scope.otp_sent = false;
                $scope.otp_verified = false;
                $scope.promo_applied = false;
            }
            $scope.requestOTP = function () {
                $('#btn-request-otp').addClass('disabled');
                console.info('visa', $scope.visa);
                $http.post("api/request_otp", $scope.application)
                    .then(function (response) {
                        console.log("SUCCESS requestOTP", response);
                        $('#btn-request-otp').removeClass('disabled');
                        $scope.errors = null;
                        $scope.otp_sent = true;
                        toastScucess('OTP has been send via email');
                    }, function (response) {
                        console.error("ERROR requestOTP", response);
                        $('#btn-request-otp').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        }
                    });
            }
            $scope.submitPhone = function () {
                $('#btn-submit-phone').addClass('disabled');
                console.info('visa', $scope.visa);
                $scope.application.phone = angular.copy($scope.phone);
                $http.post("api/submit_phone", $scope.application)
                    .then(function (response) {
                        console.log("SUCCESS submitPhone", response);
                        $('#btn-submit-phone').removeClass('disabled');
                        $scope.errors = null;
                        $scope.application = response.data;
                        $scope.phone_saved = true;
                        toastScucess('Your phone number has been saved');
                    }, function (response) {
                        console.error("ERROR submitPhone", response);
                        $('#btn-submit-phone').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        }
                    });
            }
            $scope.submitOTP = function () {
                $('#btn-submit-otp').addClass('disabled');
                console.info('visa', $scope.visa);
                $scope.application.otp = angular.copy($scope.otp);
                $http.post("api/submit_otp", $scope.application)
                    .then(function (response) {
                        console.log("SUCCESS submitOTP", response);
                        $('#btn-submit-otp').removeClass('disabled');
                        $scope.errors = null;
                        $scope.application = response.data;
                        $scope.otp_verified = true;
                        toastScucess('Your email has been verified');
                    }, function (response) {
                        console.error("ERROR submitOTP", response);
                        $('#btn-submit-otp').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        }
                    });
            }
            $scope.applyPromocode = function () {
                $('#btn-apply-promocode').addClass('disabled');
                console.info('visa', $scope.visa);
                $scope.application.promocode = angular.copy($scope.promocode);
                $http.post("api/apply_promocode", $scope.application)
                    .then(function (response) {
                        console.log("SUCCESS applyPromocode", response);
                        $('#btn-apply-promocode').removeClass('disabled');
                        $scope.errors = null;
                        $scope.promo_applied = true;
                        $scope.application = response.data;
                        toastScucess('Promocode has been applied');
                    }, function (response) {
                        console.error("ERROR applyPromocode", response);
                        $('#btn-apply-promocode').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        } else
                            toastError('Oops! Something went wrong');
                    });
            }
            $scope.showApplyForm = function (index) {
                $('#applyModal').modal('show')
                $scope.visa.visa_type = index+'';
                $scope.getPrice();
            }
            $scope.submitApplication = function () {
                @env('ui') $scope.tab_index++; return; @endenv

                $('#btn-proceed-payment').addClass('disabled');
                $http.post("api/submit_application", $scope.application)
                    .then(function (response) {
                        console.log("SUCCESS submitApplication", response);
                        $('#btn-proceed-payment').removeClass('disabled');
                        $scope.application_submitted = true;
                        $scope.tab_index++;
                    }, function (response) {
                        console.error("ERROR submitApplication", response);
                        $('#btn-proceed-payment').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        } else
                            toastError('Oops! Something went wrong');
                    });
            }
            $scope.paymentSuccess = function (data) {
                $http.post("api/payment_success", data)
                    .then(function (response) {
                        console.log("SUCCESS paymentSuccess", response);
                        closePay();
                        $('#paymentModal').modal('show');
                        $scope.payment_success = true;
                        $scope.application = response.data;
                    }, function (response) {
                        console.error("ERROR paymentSuccess", response);
                        closePay();
                        $('#btn-proceed-payment').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        } else
                            toastError('Oops! Something went wrong');
                    });
            }
            $scope.paymentError = function () {
                $http.post("api/payment_error", {id:$scope.app_id})
                    .then(function (response) {
                        console.log("SUCCESS paymentError", response);
                        closePay();
                        $('#paymentModal').modal('show');
                        $scope.payment_error = true;
                        $scope.application = response.data;
                    }, function (response) {
                        console.error("ERROR paymentError", response);
                        $('#btn-proceed-payment').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        } else
                            toastError('Oops! Something went wrong');
                    });
            }

            $scope.trackApplication = function () {
                $('#btn-track').addClass('disabled');
                $http.post("api/track_application", $scope.track)
                    .then(function (response) {
                        console.log("SUCCESS trackApplication", response);
                        $('#btn-track').removeClass('disabled');
                        $scope.track_application = response.data;
                        $scope.errors = null;
                    }, function (response) {
                        console.error("ERROR trackApplication", response);
                        $('#btn-track').removeClass('disabled');
                        $scope.track_application = null;
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        } else
                            toastError('Oops! Something went wrong');
                    });

            }
            $scope.editVisa = function (visa, tab_index) {
                console.log('editVisa', visa);
                $scope.visa = angular.copy(visa);
                $scope.visa.first_language = ''+$scope.visa.first_language;
                $scope.visa.second_language = ''+$scope.visa.second_language;
                console.log('visa', $scope.visa);
                $scope.tab_index = tab_index;
            }
            foloosiHandler(response, function (e) {
                console.log('FOLOOSI', e);
                if(e.data.status == 'success'){
                    console.log('FOLOOSI SUCCESS', e);
                    $scope.paymentSuccess(e.data.data);
                }
                if(e.data.status == 'error'){
                    console.error('FOLOOSI ERROR', e);
                    $scope.paymentError();
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.date').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy',
            });
            $('.counter-up').counterUp({
                delay: 10,
                time: 1000
            });
        });
    </script>
@endpush

@push('css')
    <style>
        .welome-to-visa-wrapper {
            padding: 60px 0;
        }
        .welome-to-visa-wrapper h2 {
            font-size: 26px;
            font-weight: 600;
            color: #000000;
            margin-bottom: 0px;
        }
        .welome-to-visa-wrapper p {
            font-size: 18px;
            font-weight: 400;
            color: #000000;
            padding: 40px 0 60px;
            margin-bottom: 0px;
        }
        .welome-to-visa-wrapper .welcome-visa-card img {
            width: auto;
            height: 60px;
            margin-bottom: 5px;
        }
        .welome-to-visa-wrapper .welcome-visa-card h5 {
            font-size: 18px;
            font-weight: 600;
            color: #000000;
            margin-bottom: 0;
        }
        .welome-to-visa-wrapper .welcome-visa-card p {
            font-size: 14px;
            font-weight: 400;
            color: #000000;
            padding: 0px;
            margin-bottom: 0px;
        }
        .dedicated-wrapper {
            background: #000000;
        }
        .dedicated-wrapper h3 {
            font-size: 26px;
            font-weight: 600;
            color: #ffffff;
            margin-bottom: 20px;
        }
        .dedicated-wrapper p {
            font-size: 16px;
            font-weight: 400;
            color: #ffffff;
        }
        .dedicated-wrapper a {
            color: #ffffff;
            text-decoration: underline;
        }
        .frequently-asked-wrapper h2 {
            font-size: 36px;
            font-weight: 600;
            color: #000000;
        }
        @media screen and (max-width: 767px) {
            .frequently-asked-wrapper h2 {
                font-size: 20px;
            }
        }
        .frequently-asked-wrapper p {
            font-size: 18px;
            font-weight: 400;
            color: #000000;
        }
        .frequently-asked-wrapper p a {
            font-weight: 600;
            color: #000000;
            text-decoration: underline;
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card {
            background: none;
            border: none;
            border-bottom: 1px solid #0000001a !important;
            border-radius: 0px;
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card:last-child {
            border-bottom: none !important;
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card .card-header {
            background: #ffffff;
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card .card-header .fa-circle {
            font-size: 6px;
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card .card-header h4 {
            display: flex;
            align-items: center;
            justify-content: space-between;
            flex-grow: 1;
            cursor: pointer;
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card .card-header h4[aria-expanded="true"] .fa-angle-down {
            transition: 0.5s;
            transform: rotate(-180deg);
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card .card-header h4 .fa-angle-down {
            transition: 0.5s;
        }
        .frequently-asked-wrapper .asked-questions-wrapper .card .card-header h4 button {
            padding-left: 5px;
            font-size: 18px;
            font-weight: 400;
            color: #000000;
            text-decoration: none;
            text-align: left;
        }
        .client-wrapper {
            background-color: #dddddd;
        }
        footer {
            padding: 20px 0px;
        }
        footer h6 {
            color: #000000;
            font-weight: 700;
            font-size: 16px;
            margin-bottom: 20px;
        }
        footer p {
            color: #000000;
            font-weight: 400;
            font-size: 14px;
        }
        footer a {
            color: #000000;
            font-weight: 400;
            font-size: 14px;
        }
        footer a:hover {
            color: #000000;
        }
        footer ul {
            margin: 0px;
            padding: 0px;
            list-style: none;
        }
        footer ul li {
            margin-bottom: 5px;
            padding-left: 0px;
        }


    </style>
@endpush
