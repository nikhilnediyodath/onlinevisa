@extends('layouts.admin')

@section('content')
<div class="col-md-12 col-12 mr-auto ml-auto">
    <!--      Wizard container        -->
    <div class="wizard-container">
        <div class="card card-wizard active" data-color="rose" id="wizardProfile">
            <form action="" method=""
                novalidate="novalidate">
                <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                <div class="card-header text-center">
                    <h3 class="card-title">
                        Build Your Profile
                    </h3>
                    <h5 class="card-description">This information will let us know more about you.</h5>
                </div>
                <div class="wizard-navigation">
                    <ul class="nav nav-pills">
                        <li class="nav-item" style="width: 33.3333%;">
                            <a class="nav-link active show"
                                href="#about"
                                data-toggle="tab" role="tab" aria-selected="true">
                                About
                            </a>
                        </li>
                        <li class="nav-item" style="width: 33.3333%;">
                            <a class="nav-link"
                                href="#account"
                                data-toggle="tab" role="tab" aria-selected="false">
                                Account
                            </a>
                        </li>
                        <li class="nav-item" style="width: 33.3333%;">
                            <a class="nav-link"
                                href="#address"
                                data-toggle="tab" role="tab" aria-selected="false">
                                Address
                            </a>
                        </li>
                    </ul>
                    <div class="moving-tab"
                        style="width: 222.443px; transform: translate3d(-8px, 0px, 0px); transition: transform 0s ease 0s;">
                        About
                    </div>
                    <div class="moving-tab"
                        style="width: 222.443px; transform: translate3d(-8px, 0px, 0px); transition: transform 0s ease 0s;">
                        About
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="about">
                            <h5 class="info-text"> Let's start with the basic information (with validation)</h5>
                            <div class="row justify-content-center">
                                <div class="col-sm-4">
                                    <div class="picture-container">
                                        <div class="picture">
                                            <img src="./Material Dashboard PRO by Creative Tim_files/default-avatar.png"
                                                class="picture-src" id="wizardPicturePreview" title="">
                                            <input type="file" id="wizard-picture">
                                        </div>
                                        <h6 class="description">Choose Picture</h6>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-group form-control-lg">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">face</i>
                                            </span>
                                        </div>
                                        <div class="form-group bmd-form-group is-filled has-success">
                                            <label for="exampleInput1" class="bmd-label-floating">First Name
                                                (required)</label>
                                            <input type="text" class="form-control valid" id="exampleInput1"
                                                value="nikhil" name="firstname" required="" aria-required="true"
                                                aria-invalid="false"><label id="exampleInput1-error" class="error"
                                                for="exampleInput1"></label>
                                        </div>
                                    </div>
                                    <div class="input-group form-control-lg">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">record_voice_over</i>
                                            </span>
                                        </div>
                                        <div class="form-group bmd-form-group is-filled has-success">
                                            <label for="exampleInput11" class="bmd-label-floating">Second Name</label>
                                            <input type="text" class="form-control valid" id="exampleInput11"
                                                name="lastname" required="" aria-required="true" value="nikhil"
                                                aria-invalid="false"><label id="exampleInput11-error" class="error"
                                                for="exampleInput11"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-10 mt-3">
                                    <div class="input-group form-control-lg">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">email</i>
                                            </span>
                                        </div>
                                        <div class="form-group bmd-form-group is-filled has-success">
                                            <label for="exampleInput1" class="bmd-label-floating">Email
                                                (required)</label>
                                            <input type="email" class="form-control valid" id="exampleemalil"
                                                value="nikhil@gmail.com" name="email" required="" aria-required="true"
                                                autocomplete="off" aria-invalid="false"><label id="exampleemalil-error"
                                                class="error" for="exampleemalil"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="account">
                            <h5 class="info-text"> What are you doing? (checkboxes) </h5>
                            <div class="row justify-content-center">
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="jobb" value="Design">
                                                <div class="icon">
                                                    <i class="fa fa-pencil"></i>
                                                </div>
                                                <h6>Design</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="jobb" value="Code">
                                                <div class="icon">
                                                    <i class="fa fa-terminal"></i>
                                                </div>
                                                <h6>Code</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="jobb" value="Develop">
                                                <div class="icon">
                                                    <i class="fa fa-laptop"></i>
                                                </div>
                                                <h6>Develop</h6>
                                            </div>
                                            <div class="dropdown bootstrap-select">
                                                <div class="dropdown bootstrap-select"><select class="selectpicker"
                                                        data-style="btn btn-primary btn-round" title="Single Select"
                                                        data-size="7" tabindex="-98">
                                                        <option class="bs-title-option" value=""></option>
                                                        <option class="bs-title-option" value=""></option>
                                                        <option disabled="" selected="">Choose city</option>
                                                        <option value="2">Foobar</option>
                                                        <option value="3">Is great</option>
                                                    </select><button type="button"
                                                        class="dropdown-toggle btn btn-primary btn-round"
                                                        data-toggle="dropdown" role="button" title="Choose city">
                                                        <div class="filter-option">
                                                            <div class="filter-option-inner">
                                                                <div class="filter-option-inner-inner">Choose city</div>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    <div class="dropdown-menu " role="combobox">
                                                        <div class="inner show" role="listbox" aria-expanded="false"
                                                            tabindex="-1">
                                                            <ul class="dropdown-menu inner show"></ul>
                                                        </div>
                                                    </div>
                                                </div><button type="button"
                                                    class="dropdown-toggle btn btn-primary btn-round"
                                                    data-toggle="dropdown" role="button" title="Choose city">
                                                    <div class="filter-option">
                                                        <div class="filter-option-inner">
                                                            <div class="filter-option-inner-inner">Choose city</div>
                                                        </div>
                                                    </div>
                                                </button>
                                                <div class="dropdown-menu " role="combobox">
                                                    <div class="inner show" role="listbox" aria-expanded="false"
                                                        tabindex="-1">
                                                        <ul class="dropdown-menu inner show"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="address">
                            <div class="row justify-content-center">
                                <div class="col-sm-12">
                                    <h5 class="info-text"> Are you living in a nice area? </h5>
                                </div>
                                <div class="col-sm-7">


                                <div class="choice active" data-toggle="wizard-checkbox">
                                  <input type="checkbox" name="jobb" value="Design">
                                  <div class="icon">
                                    <i class="fa fa-pencil"></i>
                                  </div>
                                  <h6>Design</h6>
                                </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Street No.</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">City</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group select-wizard">
                                        <label>Country</label>
                                        <div class="dropdown bootstrap-select">
                                            <div class="dropdown bootstrap-select"><select class="selectpicker"
                                                    data-size="7" data-style="select-with-transition"
                                                    title="Single Select" tabindex="-98">
                                                    <option class="bs-title-option" value=""></option>
                                                    <option class="bs-title-option" value=""></option>
                                                    <option value="Afghanistan"> Afghanistan </option>
                                                    <option value="Albania"> Albania </option>
                                                    <option value="Algeria"> Algeria </option>
                                                    <option value="American Samoa"> American Samoa </option>
                                                    <option value="Andorra"> Andorra </option>
                                                    <option value="Angola"> Angola </option>
                                                    <option value="Anguilla"> Anguilla </option>
                                                    <option value="Antarctica"> Antarctica </option>
                                                </select><button type="button"
                                                    class="btn dropdown-toggle bs-placeholder select-with-transition"
                                                    data-toggle="dropdown" role="button" title="Single Select">
                                                    <div class="filter-option">
                                                        <div class="filter-option-inner">
                                                            <div class="filter-option-inner-inner">Single Select</div>
                                                        </div>
                                                    </div>
                                                </button>
                                                <div class="dropdown-menu " role="combobox">
                                                    <div class="inner show" role="listbox" aria-expanded="false"
                                                        tabindex="-1">
                                                        <ul class="dropdown-menu inner show"></ul>
                                                    </div>
                                                </div>
                                            </div><button type="button"
                                                class="btn dropdown-toggle bs-placeholder select-with-transition"
                                                data-toggle="dropdown" role="button" title="Single Select">
                                                <div class="filter-option">
                                                    <div class="filter-option-inner">
                                                        <div class="filter-option-inner-inner">Single Select</div>
                                                    </div>
                                                </div>
                                            </button>
                                            <div class="dropdown-menu " role="combobox">
                                                <div class="inner show" role="listbox" aria-expanded="false"
                                                    tabindex="-1">
                                                    <ul class="dropdown-menu inner show"></ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd disabled"
                            name="previous" value="Previous">
                        <div class="ripple-container">
                            <div class="ripple-decorator ripple-on ripple-out"
                                style="left: 554px; top: 599px; background-color: rgb(255, 255, 255); transform: scale(14.4785);">
                            </div>
                            <div class="ripple-decorator ripple-on ripple-out"
                                style="left: 554px; top: 599px; background-color: rgb(255, 255, 255); transform: scale(14.4785);">
                            </div>
                        </div>
                    </div>
                    <div class="ml-auto">
                        <input type="button" class="btn btn-next btn-fill btn-rose btn-wd" name="next" value="Next"
                            style="">
                        <div class="ripple-container">
                            <div class="ripple-decorator ripple-on ripple-out"
                                style="left: 1091px; top: 601px; background-color: rgb(255, 255, 255); transform: scale(11.2578);">
                            </div>
                            <div class="ripple-decorator ripple-on ripple-out"
                                style="left: 1091px; top: 601px; background-color: rgb(255, 255, 255); transform: scale(11.2578);">
                            </div>
                            <div class="ripple-decorator ripple-on ripple-out"
                                style="left: 1091px; top: 601px; background-color: rgb(255, 255, 255); transform: scale(11.2578);">
                            </div>
                        </div>
                        <input type="button" class="btn btn-finish btn-fill btn-rose btn-wd" name="finish"
                            value="Finish" style="display: none;">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
    <!-- wizard container -->
</div>
@endsection

@push('js')

<script>
    $(document).ready(function () {
        // Initialise the wizard
        demo.initMaterialWizard();
        setTimeout(function () {
            $('.card.card-wizard').addClass('active');
        }, 600);
    });
    demo = {
        initMaterialWizard: function () {
            // Code for the Validator
            var $validator = $('.card-wizard form').validate({
                rules: {
                    firstname: {
                        required: true,
                        minlength: 3
                    },
                    lastname: {
                        required: true,
                        minlength: 3
                    },
                    email: {
                        required: true,
                        minlength: 3,
                    }
                },

                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass(
                        'has-danger');
                },
                success: function (element) {
                    $(element).closest('.form-group').removeClass('has-danger').addClass(
                        'has-success');
                },
                errorPlacement: function (error, element) {
                    $(element).append(error);
                }
            });



            // Wizard Initialization
            $('.card-wizard').bootstrapWizard({
                'tabClass': 'nav nav-pills',
                'nextSelector': '.btn-next',
                'previousSelector': '.btn-previous',

                onNext: function (tab, navigation, index) {
                    var $valid = $('.card-wizard form').valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                },

                onInit: function (tab, navigation, index) {
                    //check number of tabs and fill the entire row
                    var $total = navigation.find('li').length;
                    var $wizard = navigation.closest('.card-wizard');

                    $first_li = navigation.find('li:first-child a').html();
                    $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
                    $('.card-wizard .wizard-navigation').append($moving_div);

                    refreshAnimation($wizard, index);

                    $('.moving-tab').css('transition', 'transform 0s');
                },

                onTabClick: function (tab, navigation, index) {
                    var $valid = $('.card-wizard form').valid();

                    if (!$valid) {
                        return false;
                    } else {
                        return true;
                    }
                },

                onTabShow: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;

                    var $wizard = navigation.closest('.card-wizard');

                    // If it's the last tab then hide the last button and show the finish instead
                    if ($current >= $total) {
                        $($wizard).find('.btn-next').hide();
                        $($wizard).find('.btn-finish').show();
                    } else {
                        $($wizard).find('.btn-next').show();
                        $($wizard).find('.btn-finish').hide();
                    }

                    button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                    setTimeout(function () {
                        $('.moving-tab').text(button_text);
                    }, 150);

                    var checkbox = $('.footer-checkbox');

                    if (!index == 0) {
                        $(checkbox).css({
                            'opacity': '0',
                            'visibility': 'hidden',
                            'position': 'absolute'
                        });
                    } else {
                        $(checkbox).css({
                            'opacity': '1',
                            'visibility': 'visible'
                        });
                    }

                    refreshAnimation($wizard, index);
                }
            });


            // Prepare the preview for profile picture
            $("#wizard-picture").change(function () {
                readURL(this);
            });

            $('[data-toggle="wizard-radio"]').click(function () {
                wizard = $(this).closest('.card-wizard');
                wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
                $(this).addClass('active');
                $(wizard).find('[type="radio"]').removeAttr('checked');
                $(this).find('[type="radio"]').attr('checked', 'true');
            });

            $('[data-toggle="wizard-checkbox"]').click(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).find('[type="checkbox"]').removeAttr('checked');
                } else {
                    $(this).addClass('active');
                    $(this).find('[type="checkbox"]').attr('checked', 'true');
                }
            });

            $('.set-full-height').css('height', 'auto');

            //Function to show image before upload

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(window).resize(function () {
                $('.card-wizard').each(function () {
                    $wizard = $(this);

                    index = $wizard.bootstrapWizard('currentIndex');
                    refreshAnimation($wizard, index);

                    $('.moving-tab').css({
                        'transition': 'transform 0s'
                    });
                });
            });

            function refreshAnimation($wizard, index) {
                $total = $wizard.find('.nav li').length;
                $li_width = 100 / $total;

                total_steps = $wizard.find('.nav li').length;
                move_distance = $wizard.width() / total_steps;
                index_temp = index;
                vertical_level = 0;

                mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    vertical_level = parseInt(index / 2);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        },

    }

</script>
@endpush
