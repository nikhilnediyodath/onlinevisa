@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-5">
        <div class="card card-chart">
            <div class="card-header card-header-icon card-header-danger">
                <div class="card-icon">
                    <i class="material-icons">pie_chart</i>
                </div>
                <h4 class="card-title">Pie Chart</h4>
            </div>
            <div class="card-body">
                <div id="chartPreferences" class="ct-chart"></div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-12">
                        <h6 class="card-category">Legend</h6>
                    </div>
                    <div class="col-md-12">
                        <i class="fa fa-circle text-info"></i> Apple
                        <i class="fa fa-circle text-warning"></i> Samsung
                        <i class="fa fa-circle text-danger"></i> Windows Phone
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var dataPreferences = {
        labels: ['41%','32%'],
        series: [41, 32, 44, 15],
        colors:["#333", "#222", "#111", "#444"],
    };

    Chartist.Pie('#chartPreferences', dataPreferences, {
        height: '230px'
    });

</script>
@endpush
