@extends('layouts.landing')

@section('heading', 'Terms and conditions')

@section('content')
<p style="text-align: justify">When you travel with us, you are our Guest. This document uses the
    reference "Guests" by which we define "passengers". We do hereby declare that they have no role
    whatsoever in the assessment of a visa application, which is the sole prerogative of the Government
    of UAE / Immigration Department. Therefore, we will not in any manner be liable or responsible for
    any delay in the processing or rejection of any visa applications. Issuance and approval of a visa are solely regulated by the Government of the UAE and
    governed by their rules and regulations. These may be amended from time to time. We will sponsor the
    Guest application subject to Guest fulfilling the eligibility conditions. Visa fees will not be
    refunded under any circumstances. Guests who request a Transit Visa sponsored by us should hold a
    valid ticket to travel into Abu Dhabi Airport, and onwards from Abu Dhabi Airport. This ticket can
    be issued by any airline. Guests who request any type of Tourist Visa sponsored by us should hold a
    valid ticket to travel into any airport in the UAE, and onwards from any of the airports in the UAE.
    We will not be responsible for - and will not be liable for - Guests not being allowed to travel due
    to being denied boarding, offloading, cancellation of flight, delays and any other cause or
    circumstances beyond Etihad Airways' control. We shall not be liable in the case of any change in
    the date of travel not being communicated to us. The Guest will be required to fill in a Visa
    Application Form accurately and submit it, along with the applicable visa fees, valid passport and
    necessary documentation as specified in the Visa Application Form. Guests must hold valid travel
    documents and comply with the requirements of the Government of UAE / Immigration Authorities. The
    decision to grant or refuse a visa is the sole prerogative of the Immigration Authorities and it is
    final. In case of rejection of visa application, the applicant will be informed of such, no
    correspondence will be entertained, no visa fees will be refunded and no reasons will be required to
    be given. It is understood that whenever the processing of the visa application is prevented,
    delayed, restricted or interfered with for any reasons whatsoever by UAE Immigration Authorities to
    process Guest visa application, then we shall not be liable to any Guest for any loss or damage
    which may be suffered as result of such causes and we shall be discharged of all its obligations
    hereunder. Issuance of a visa or approval of the visa application does not in any way guarantee the
    Guest the right to enter UAE. The entry is at the sole discretion of the Immigration officer at the
    Airport. In case of denial of visa or entry into UAE by the Government, we shall in no way be liable
    to Guest in any manner whatsoever. We shall not be liable for any losses or damages which the Guest
    may suffer, arising from delay in processing or receiving the visa. The visa is valid as per the
    Government of UAE / Issuing Authorities' rules and regulations, as amended from time to time. The
    visa must be used within its period of validity. we will not be liable to a Guest for any changes or
    cancellations to Government Regulations that may result in a Guest not being able to travel to or
    enter the UAE. Guests will be solely responsible to ensure they fulfill UAE Government requirements
    for travel, which may include police clearances, medical insurance, etc. We shall take all
    reasonable measures to ensure that information provided by the Guest in its application form shall
    remain confidential. However, we shall not be liable for any unauthorized access by any means to
    that information. We reserve the right to add, alter or vary these Terms and Conditions at any time
    without notice or liability, and all Guests availing themselves of this facility shall be bound by
    the same. Guest expressly declares that they have read and understood these Terms & Conditions and
    that they are in compliance thereof. The processed visa will be sent to Guest by email with JPEG or
    PDF format in the attached files. Guest is responsible for ensuring the information you have
    provided, including your email address, is accurate. After receiving a visa, Guest must print a visa
    for boarding. Guests are requested to check their visa is correct, and bring any discrepancy to our
    notice immediately. Guests are advised to carefully go through the “Note” printed on your visa for
    “validity of permit” and “duration of stay”. The visas are valid for two months from the date of
    issuance to enter UAE and will be active from the date of entry for Transit 96 Hrs, Short Term 30
    days, Long term 90 days. Only machine-readable passports will be accepted. Handwritten passports will not be accepted. We hereby
    declare that the applicant data collected by us will be used for the intended purpose only, and it
    will not be shared with any unauthorized person. We will not be liable for any damages that occurred
    due to discrepancies in visas if Guest does not revert back to us within 24 hours of receiving the
    visa.</p>
@endsection