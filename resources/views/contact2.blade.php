<!DOCTYPE HTML>
<html ng-app="myApp" ng-controller="myCtrl">

<head>
    <title>OnlineVisa by Blueleaf Technology</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta property="og:description" content="Discover the best travel services in Abu Dhabi with VisaByMakto. We offer expert visa services to make your travel experience seamless and" />
    <meta property="og:title" content="Travel Services Abu Dhabi | VisaByMakto - Your Guide" />
    <meta name="description" content="Explore Abu Dhabi with our comprehensive travel services. Contact VisaByMakto for expert visa services and start your unforgettable journey today!" />
    <meta name="keywords" content="Travel services abu dhabi, visa service abu dhabi">
    <meta name="title" content="Best Travel Services Abu Dhabi | Contact Us | VisaByMakto">

    {{-- Fonts --}}
    <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet">
    {{-- Style --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" href="{{ asset('landing/icheck/skins/square/green.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/main.css') }}" />
    {{-- Simple toast --}}
    <link rel="stylesheet" href="{{ asset('landing/simple_toast/toast.css') }}">
    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/counter.css') }}">
    <style>
        .btn-pay {
            width: 249px;
        }

        .form-control {
            color: #1b5c9c;
            background-color: #e0d9d9;
            background-clip: padding-box;
        }

        .i {
            font-size: 32px;
            color: #4aef6d;
            float: left;
            line-height: 1;
        }

        .headline h3 {
            font-family: 'ostrich_sansblack';
            text-transform: uppercase;
            padding: 0;
            margin: 20px 0 30px 0;
            font-size: 64px;
            text-align: center;
            text-shadow: 0 1px 0 #ffffff;
            line-height: 1.1em;
            color: #3a3a3a;
            font-weight: 300;
        }

        .headline {
            text-align: center;
            width: 100%;
        }

        .container {
            padding: 0 20px 0 20px;
            margin-right: auto;
            margin-left: auto;
        }

        ul {
            display: block;
            list-style-type: disc;
            margin-block-start: 1em;
            margin-block-end: 1em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
            padding-inline-start: 40px;
        }

        ul.contact-list {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        ul.contact-list li input.form_input,
        ul.contact-list li textarea.form_textarea {
            width: 100%;
            padding: 10px 5px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        .contact-list .form_input,
        .form_textarea {
            margin-bottom: 0px;
        }

        #banner p {
            margin-top: 1em;
        }

        p {
            margin-bottom: 1.5rem;
        }

        ul.contact-list li input.form_input {
            width: 100%;
            padding: 3px 5px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 5px;
        }

        #banner {
            text-align: left;
        }

    </style>
    @stack('css')
</head>

<body>
    <!-- Header -->
    <header id="header">
        <div class="inner">
            <!-- <a href="index.html" class="logo"><strong>Online Visa</strong> by Blueleaf Technology</a> -->
            <nav id="nav">
                <a href="{{ url('') }}">Home</a>
                <a href="{{ url('about') }}">About Us</a>
                <a href="{{ url('contact') }}">Contact Us</a>
            </nav>
            <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
        </div>
    </header>

    <!-- Banner -->
    <section id="banner" style="min-height: 100vh;">
        <div class="inner">
            <div class="container">

                <div class="row">
                    <div class="span12">
                        <div class="headline">
                            <h1><span>Get in Touch</span></h1>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="section-intro">
                            <p style="color: aliceblue;">
                                Qui te reprimique scripserit, et zril ornatus duo, mei te habemus fastidii
                                interpretaris.
                                Mel eu vide pericula, te ius sint concludaturque. No quo tale vocibus.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4 style="color: aliceblue;"><i class="icon-circled icon-32 icon-envelope"></i> Contact form</h4>
                        <form action="" method="post" role="form" class="contactForm">
                            <ul class="contact-list">
                                <li class="form-group">
                                    <input type="text" name="name" class="form_input" id="name" placeholder="Your Name"
                                        ng-model="contact2.name">
                                    <div class="invalid-feedback" ng-show="errors.name">@{{ errors.name[0] }}</div>
                                <li class="form-group">
                                    <input type="email" class="form_input" name="email" id="email"
                                        placeholder="Your Email" ng-model="contact2.email">
                                    <div class="invalid-feedback" ng-show="errors.email">@{{ errors.email[0] }}</div>
                                </li>
                                <li class="form-group">
                                    <input type="text" class="form_input" name="subject" id="subject"
                                        placeholder="Subject" ng-model="contact2.subject">
                                    <div class="invalid-feedback" ng-show="errors.subject">@{{ errors.subject[0] }}
                                    </div>
                                </li>

                                <li class="form-group">
                                    <textarea class="form_textarea" name="message" rows="12" data-rule="required"
                                        ng-model="contact.message" placeholder="Message"></textarea>
                                    <div class="invalid-feedback" ng-show="errors.message">@{{ errors.message[0] }}
                                    </div>
                                </li>
                                <li class="last">
                                    <button class="btn btn-large btn-success" style="color: aliceblue;" type="submit"
                                        id="send" onclick="return false" ng-click="sendMessage()">Send a
                                        message</button>
                                </li>
                            </ul>
                        </form>
                        <!-- end contact form -->
                    </div>
                    <div class="col-md-6" style="text-align: initial;">
                        <h4 style="color: aliceblue;"><i class="icon-circled icon-32 icon-user"></i> Get in touch</h4>
                        {{-- <p style="color: aliceblue;">
                            Feel free to ask if you have any question regarding our services or if you just want to say
                            Hello, we would love to hear from you.
                        </p> --}}
                        <div class="dotted_line">
                        </div>
                        <p style="color: aliceblue;">
                            <span><i class="icon-phone"></i> <strong
                                    style="color: aquamarine;">Phone:</strong>{{ config('app.contact') }}</span>
                        </p>
                        <p style="color: aliceblue;">
                            <span><i class="icon-comment"></i> <strong style="color: aquamarine;">Address:</strong>
                                Makto Travel Services LLC
                                Abu Dhabi</span>
                        </p>
                        <p style="color: aliceblue;">
                            <span><i class="icon-envelope-alt"></i> <strong style="color: aquamarine;">Email:</strong>
                                info@yourdomain.com</span>
                        </p>
                        <div>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3631.4450671003115!2d54.33995611499522!3d24.470031384239554!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e6588baafaa8d%3A0x70856ffdded65ff!2sMakto%20Travel%20Services%20LLC!5e0!3m2!1sen!2suk!4v1573017271257!5m2!1sen!2suk"
                                width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('landing/icheck/icheck.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.js"></script>

    {{-- <!-- <script src="{{ asset('landing/js/jquery.min.js') }}"></script> --> --}}
    <script src="{{ asset('landing/js/skel.min.js') }}"></script>
    <script src="{{ asset('landing/js/util.js') }}"></script>
    <script src="{{ asset('landing/js/main.js') }}"></script>
    <!-- simple toast -->
    <script src="{{ asset('landing/simple_toast/toast.js') }}"></script>
    <script>
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function ($scope, $http, $window) {

            $scope.sendMessage = function () {
                $('#btn-send').addClass('disabled');
                $http.post("api/contact", $scope.contact)
                    .then(function (response) {
                        console.log("SUCCESS", response);
                        $('#btn-send').removeClass('disabled');
                        toastScucess('Your message has been sent');
                        $scope.errors = null;
                    }, function (response) {
                        console.error("ERROR", response);
                        $('#btn-send').removeClass('disabled');
                        if (response.status == -1) {
                            toastError('Network Error !!');
                            return;
                        } else if (response.status == 422) {
                            $scope.errors = response.data.errors;
                            toastError(response.data.message);
                        } else
                            toastError('Oops! Something went wrong');
                    });
            }
        });

    </script>
</body>

</html>
