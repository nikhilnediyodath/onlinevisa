@extends('layouts.admin')

@push('css')
<link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
@endpush

@section('content')
<div class="row">
    <div class="col-md-8 mx-auto">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">@{{ action }} Staff</h4>
            </div>
            <div class="card-body">
                <div class="alert alert-danger" ng-show="error_messages">
                    <span ng-repeat="error_message in error_messages">@{{ error_message }}</span>
                </div>
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Full Name</label>
                                <input type="text" id="name" class="form-control" ng-model="user.name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" ng-init="user.gender='Male'">
                                <div class="custom-control custom-radio  custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="male" ng-model="user.gender"
                                        value="Male">
                                    <label for="male" class="custom-control-label">Male</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="female" ng-model="user.gender"
                                        value="Female">
                                    <label for="female" class="custom-control-label">Female</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Email</label>
                                <input type="email" class="form-control" ng-model="user.email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{-- <label for="exampleFormControlSelect1">Example select</label> --}}
                                <select class="form-control" ng-model="user.designation">
                                    <option>Designation</option>
                                    @foreach (config('app.designations') as $designation)
                                    <option>{{ $designation }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Phone</label>
                                <input type="text" class="form-control" ng-model="user.phone">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="saveUser()">Submit</button>
                    <button type="submit" class="btn btn-danger pull-right" ng-show="action=='Edit'" ng-click="cancelEdit()">Cancel</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Staff</h4>
                {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Designation</th>
                            <th>Phone</th>
                            <th>Edit</th>
                        </thead>
                        <tbody ng-show="users">
                            <tr ng-repeat="user in users">
                                <td>@{{ user.id }}</td>
                                <td>@{{ user.email }}</td>
                                <td>@{{ user.name }}</td>
                                <td>@{{ user.gender }}</td>
                                <td>@{{ user.designation }}</td>
                                <td>@{{ user.phone }}</td>
                                <td><a   ng-click="editUser(user)">
                                        <i class="material-icons">edit</i></a>
                                    <a   ng-click="deleteUser(user)" title="Delete">
                                        <i class="material-icons">delete</i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, $window) {

        $scope.defaultUser = {
            designation: 'Designation',
            gender: 'Male'
        };

        $scope.init = function () {
            $scope.action = "Add";
            $scope.user = angular.copy($scope.defaultUser);
            $http.get("api/users", $scope.visa)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.users = response.data;
                }, function (response) {
                    console.error('response', response);
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()

        $scope.saveUser = function () {
            $http.post("api/user", $scope.user)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.user = angular.copy($scope.defaultUser);
                    $scope.users = null;
                    $scope.users = response.data;
                    $scope.action = "Add";
                    toast.success('Successfully Updated');
                }, function (response) {
                    console.error('response', response);
                    if (response.status == -1) {
                        toast.error('Network Error !!');
                    } else if (response.status == 422) {
                        $scope.error_messages = [];
                        angular.forEach(response.data.errors, function (value, key) {
                            $scope.error_messages.push(value[0]);
                        });
                        toast.error(response.data.message);
                    } else toast.error(response.statusText);
                });
        }
        $scope.deleteUser = function (user) {
            $http.delete("api/users/" + user.id)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.user = angular.copy($scope.defaultUser);
                    $scope.users = null;
                    $scope.users = response.data;
                    $scope.action = "Add";
                    toast.success('Successfully Deleted');
                }, function (response) {
                    console.error('response', response);
                    if (response.status == -1)
                        toast.error('Network Error !!');
                    else toast.error(response.statusText);
                });
        }

        $scope.editUser = function (user) {
            $scope.user = angular.copy(user);
            $scope.action = "Edit";
            $('#name').focus();
        }
        $scope.cancelEdit = function () {
            $scope.action = 'Add';
            $scope.user = angular.copy($scope.defaultUser);
        }
    });

</script>
@endpush

@push('css')
<style>
    p {
        margin-top: 0;
        margin-bottom: 0rem;
    }

</style>
@endpush
