@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Payments</h4>
                {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>ID</th>
                            <th>Application</th>
                            {{-- <th>Email</th>
                            <th>Visas</th> --}}
                            <th>Transaction No</th>
                            <th>Payment ID</th>
                            <th>Amount</th>
                            <th>Payment Status</th>
                            <th>Application Status</th>
                            <th>Date</th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="payment in payments">
                                <td>@{{ payment.id }}</td>
                                <td><a href="{{ url('applications') }}?reference=@{{ payment.application.reference }}" target="_blank">@{{ payment.application.reference }}</a></td>
                                <td>@{{ payment.transaction_no }}</td>
                                <td>@{{ payment.payment_transaction_id }}</td>
                                <td class="text-primary">@{{ payment.merchant_amount }}</td>
                                <td>
                                    <span ng-show="payment.status=='success'" class="badge badge-success">Success</span>
                                    <span ng-show="payment.status=='pending'" class="badge badge-warning">Pending</span>
                                    <span ng-show="payment.status=='error'" class="badge badge-danger">Error</span>
                                    <span ng-show="payment.status=='refunded'"  class="badge badge-info">Refunded</span>
                                </td>
                                <td>@{{ payment.application.status }}</td>
                                <td>@{{ payment.created_at }}</td>
                        </tbody>
                    </table>
                </div>
                
                <div class="row mt-3">
                        <p class="col-md-6 col-xl-4">Total Income : @{{ data.income + data.refunded }}</p>
                        <p class="col-md-6 col-xl-4">Total Refunded : @{{ data.refunded }}</p>
                        <p class="col-md-6 col-xl-4">Total Revenue : @{{ data.income }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, $window) {
        $scope.applications = null;
        $scope.init = function () {
            $http.get("api/payments", $scope.visa)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $scope.payments = response.data.payments;
                    $scope.data = response.data.data;
                }, function (response) {
                    console.error("ERROR", response);
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()
    });
</script>
@endpush