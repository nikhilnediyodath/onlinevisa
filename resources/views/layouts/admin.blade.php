 <!DOCTYPE html>
 <html lang="en">
 
 <head>
   <meta charset="utf-8" />
   <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('admin/img/apple-icon.png') }}">
   <link rel="icon" type="image/png" href="{{ asset('admin/img/favicon.png') }}">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

   <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <title>{{ config('app.name', 'Laravel') }}</title>

   <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
   <!--     Fonts and icons     -->
   <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
   <!-- CSS Files -->
   <link href="{{ asset('admin/css/material-dashboard-pro.css?v=2.1.1') }}" rel="stylesheet" />
   <!-- CSS Just for demo purpose, don't include it in your project -->
   <link href="{{ asset('admin/demo/demo.css" rel="stylesheet') }}" />
   <style>
     .ghostwhite{
       background-color: ghostwhite;
     }

   </style>
   @stack('css')
 </head>
 
 <body class="" ng-app="myApp" ng-controller="myCtrl">
   @include('layouts.partials.loader')
   <div class="wrapper ">
     <div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('admin/img/sidebar-1.jpg') }}">
       <!--
         Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
 
         Tip 2: you can also add an image using data-image tag
     -->
       <div class="logo">
          <a href="{{ url('') }}" class="simple-text logo-mini">@</a>
          <a href="{{ url('') }}" class="simple-text logo-normal">
            {{ config('app.name', 'Laravel') }}
          </a>
       </div>
       @include('layouts.partials.sidebar')
     </div>
     <div class="main-panel">
        @include('layouts.partials.navbar')
       <div class="content">
         <div class="container-fluid">
            @yield('content')
         </div>
       </div>
       {{-- @include('layouts.partials.footer') --}}
     </div>
   </div>
   {{-- @include('layouts.partials.fixed-plugin') --}}
   @include('layouts.partials.scripts')
   @stack('js')
 </body>
 
 </html>
 