<div class="sidebar-wrapper">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('dashboard') }}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        @if (Auth::user()->is_admin)
            <li class="nav-item ">
                <a class="nav-link" href="{{ url('staff') }}">
                    <i class="material-icons">people</i>
                    <p>Staff</p>
                </a>
            </li>
        @endif
        <li class="nav-item ">
            <a class="nav-link" href="{{ url('applications') }}">
                <i class="material-icons">library_books</i>
                <p>Applications</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ url('promocodes') }}">
                <i class="material-icons">local_offer</i>
                <p>Promocodes</p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('payment_reports') }}">
                <i class="material-icons">content_paste</i>
                <p>Payment Report</p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('application_reports') }}">
                <i class="material-icons">content_paste</i>
                <p>Application Report</p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('summary') }}">
                <i class="material-icons">assessment</i>
                <p>Summary</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ url('payments') }}">
                <i class="material-icons">payment</i>
                <p>Payments</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ url('pricing') }}">
                <i class="material-icons">monetization_on</i>
                <p>Pricing</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ url('settings') }}">
                <i class="material-icons">build</i>
                <p>Settings</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ url('mail_templates') }}">
                <i class="material-icons">email</i>
                <p>Mail Templates</p>
            </a>
        </li>
    </ul>
</div>
