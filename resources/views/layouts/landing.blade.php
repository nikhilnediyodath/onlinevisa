<!DOCTYPE HTML>
<html ng-app="myApp" ng-controller="myCtrl">

<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5GT5T4P');</script>
    <!-- End Google Tag Manager -->

    <title>Windowseat | where next?</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta property="og:image" content="{{ asset('logo.png') }}"/>
    <meta property="og:url" content="{{ request()->fullUrl() }}">
    <meta name="twitter:url" content="{{ request()->fullUrl() }}">
    <link rel="canonical" href="{{ request()->fullUrl() }}">
{{--    <meta property="og:description" content="Marhaba! We are here to help you get there" />--}}
    @yield('meta_tags')
{{--    <meta property="og:description" content="" />--}}
{{--    <meta property="og:title" content="" />--}}
{{--    <meta name="description" content="" />--}}
{{--    <meta name="keywords" content="">--}}
{{--    <meta name="title" content="">--}}

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('makto/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('makto/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('makto/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('makto/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('makto/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('makto/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('makto/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('makto/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('makto/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('makto/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('makto/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('makto/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('makto/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('makto/manifest.json') }}">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    {{-- Fonts --}}
    <link href="https://fonts.googleapis.com/css?family=Livvic|Archivo|Montserrat&display=swap" rel="stylesheet">
    {{-- Style --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('landing/icheck/skins/square/green.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/main.css') }}" />
    {{-- Simple toast --}}
    <link rel="stylesheet" href="{{ asset('landing/simple_toast/toast.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/simple_toast/progress.css') }}">
    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/counter.css') }}">
    @stack('css')
    <style>
        .logo {
            background-image: url('logo.png');
            width:180px;
            height: 180px;
            position: absolute;
            background-size: cover;
            background-position: center;
            z-index: 1;
            margin: 20px;
        }
        @media only screen and (max-width: 1100px) {
            .logo {
                width:150px;
                height: 150px;
                margin: 10px;
            }
        }
        @media only screen and (max-width: 768px) {
            .logo {
                width:110px;
                height: 110px;
                margin: 10px;
            }
        }
    </style>
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5GT5T4P"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="logo"></div>
<div id="uploading" style="display:none;" class="meter">
    <div class="uploading">Uploading @{{ upload_progress }}%</div>
    <span style="width: @{{ upload_progress }}%"></span>
</div>
@include('welcome.loader')
<!-- Header -->
<header id="header">
    <div class="inner">
        {{-- <a id="banner" href="{{ url('') }}" class="logo" target="_blank">
            <b class="icon"><h5 class="mt-3" style="color:white">Online Visa</h5></b>
        </a> --}}
        <nav id="nav">
            <a href="{{ url('') }}">Home</a>
            <a href="{{ url('faq') }}">FAQs</a>
            <a href="{{ url('get_in_touch') }}">Contact Us</a>
        </nav>
        <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
    </div>
</header>

<!-- Banner -->
<section id="banner" style="min-height:100vh;">
    <div class="inner">
        <header>
            <h1>@yield('heading')</h1>
            @yield('tagline')
        </header>
        @yield('content')
    </div>
    @yield('bottom')

</section>
@yield('middle')

<footer class="mt-4">
    <div class="container">
        <div class="row py-5">
            <div class="col-lg-3 col-md-6 mb-2">
                <div class="">
                    <h6>VisaByMakto</h6>
                    <p class="text-dark">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-2">
                <div class="f-box">
                    <h6>Link</h6>
                    <ul>
                        <li><a href="{{ url('home') }}">Home</a></li>
                        <li><a href="{{ url('30-days-visa') }}">30 Days Visa</a></li>
                        <li><a href="{{ url('90-days-visa') }}">90 Days Visa</a></li>
                        <li><a href="{{ url('family-visa') }}">Family Visa</a></li>
                        <li><a href="{{ url('visa-agency-abu-dhabi') }}">Visa agency Abu Dhabi</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-2">
                <div class="f-box">
                    <h6>Apply</h6>
                    <ul>
                        <li><a href="">Apply for visa</a></li>
                        <li><a href="">Track Application</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-2">
                <div class="f-box">
                    <h6>Contact Us</h6>
                    <p class="mb-0 text-dark">Visa by Makto </p>
                    <p class="my-2 text-dark"> Dubai,220 </p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row pt-3">
            <div class="col-lg-6 col-md-6">
                <div class="">© VisaByMakto.co
                    <script>
                        var CurrentYear = new Date().getFullYear()
                        document.write(CurrentYear)
                    </script>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 text-right">
                <a href="{{ url('privacy-policy') }}">Privacy Policy</a>
                <span style="border-right: 1px solid lightgray; margin: 0 5px"></span>
                <a href="{{ url('terms-conditions') }}">Terms and Conditions</a>
            </div>
        </div>
    </div>
</footer>

@yield('modals')

@include('welcome.tawk')

{{-- Foloosi payment gateway --}}
<script type="text/javascript" src="https://www.foloosi.com/js/foloosipay.v2.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="{{ asset('landing/icheck/icheck.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.js"></script>

{{-- <!-- <script src="{{ asset('landing/js/jquery.min.js') }}"></script> --> --}}
<script src="{{ asset('landing/js/skel.min.js') }}"></script>
<script src="{{ asset('landing/js/util.js') }}"></script>
<script src="{{ asset('landing/js/main.js') }}"></script>

{{-- counter-up --}}
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>


<!-- simple toast -->
<script src="{{ asset('landing/simple_toast/toast.js') }}"></script>
<script>
    var app = angular.module('myApp', []);
    app.filter('trusted', ['$sce', function ($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);
</script>
@stack('js')
</body>
</html>
