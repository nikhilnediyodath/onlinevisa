@extends('layouts.admin')

@push('css')
<link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="d-flex justify-content-between">
                        <h4 class="card-title">Application Report</h4>
                        <a class="btn btn-sm btn-social btn-link text-white" href="{{ url('reports/application?') }}@{{ filter }}">
                            <i class="fa fa-download"></i> Download</a>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="custom-control custom-radio  custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_all"
                                ng-model="filter" value="@{{filter_values.all}}" ng-change="init()">
                            <label for="filter_all" class="custom-control-label">All</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_processing"
                                ng-model="filter" value="@{{filter_values.last_year}}" ng-change="init()">
                            <label for="filter_processing" class="custom-control-label">Last Year</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_rejected"
                                ng-model="filter" value="@{{filter_values.this_year}}" ng-change="init()">
                            <label for="filter_rejected" class="custom-control-label">This Year</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_suspended"
                                ng-model="filter" value="@{{filter_values.last_month}}" ng-change="init()">
                            <label for="filter_suspended" class="custom-control-label">Last Month</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="filter_approved"
                                ng-model="filter" value="@{{filter_values.this_month}}" ng-change="init()">
                            <label for="filter_approved" class="custom-control-label">This Month</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            @foreach (App\Models\Application::report() as $row)
                            <tr>
                                @foreach ($row as $key => $value)
                                <th><b>@title($key)</b></th>
                                @endforeach
                            </tr>
                            @break
                            @endforeach
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in items">
                                <td ng-repeat="(key, value) in row">@{{value}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <ul class="pagination pull-right">
                        <li class="page-item" ng-class="{disabled:pager.currentPage === 1}"><a class="page-link" href="#" ng-click="setPage(1)">«</a></li>
                        <li class="page-item" ng-class="{disabled:pager.currentPage === 1}"><a class="page-link" href="#" ng-click="setPage(pager.currentPage - 1)">&lsaquo;</a></li>
                        <li class="page-item" ng-repeat="page in pager.pages" ng-class="{active:pager.currentPage === page}"><a class="page-link" href="#" ng-click="setPage(page)">@{{page}}</a></li>
                        <li class="page-item" ng-class="{disabled:pager.currentPage === pager.totalPages}"><a class="page-link" href="#" ng-click="setPage(pager.currentPage + 1)">&rsaquo;</a></li>
                        <li class="page-item" ng-class="{disabled:pager.currentPage === pager.totalPages}"><a class="page-link" href="#" ng-click="setPage(pager.totalPages)">»</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http, PagerService) {
        console.log('PagerService', PagerService);
        
        $scope.filter_values = {
            all: '',
            last_year: ('year='+({{ date('Y') }}-1)),
            this_year: ('year='+{{ date('Y') }}),
            last_month: ('year='+{{ date('Y') }}+'&month='+({{ date('m') }}-1)),
            this_month: ('year='+{{ date('Y') }}+'&month='+{{ date('m') }}),
        };
        $scope.filter = $scope.filter_values.this_month;
        $scope.init = function () {
            $('#loader').fadeIn();
            $http.get("api/application_report?"+$scope.filter)
                .then(function (response) {
                    console.log("SUCCESS", response);
                    $('#loader').fadeOut();
                    $scope.data = response.data;
                    $scope.all_items = response.data.payment;
                    console.log('$scope.all_items', $scope.all_items);
                    $scope.setPage(1);
                }, function (response) {
                    console.error("ERROR", response);
                    $('#loader').fadeOut();
                    if (response.status == -1) toastError('Network Error !!')
                });
        }
        $scope.init()

        $scope.all_items = [];
        
        $scope.setPage = function (page) {
            $scope.items = null;
            // get pager object from service
            $scope.pager = PagerService.GetPager($scope.all_items.length, page);
            if (page < 1 || page > $scope.pager.totalPages) return;

            // get current page of items
            $scope.items = $scope.all_items.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
        }
        $scope.setPage(1);
    });
</script>
<script src="{{ asset('angularjs/pager-service.js') }}"></script>
@endpush
