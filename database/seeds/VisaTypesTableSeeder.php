<?php

use Illuminate\Database\Seeder;

class VisaTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visa_types')->insert([
            'id' => '1',
            'name' => '30 Days Visa',
        ]);
        DB::table('visa_types')->insert([
            'id' => '2',
            'name' => '90 Days Visa',
        ]);
        DB::table('visa_types')->insert([
            'id' => '3',
            'name' => '1 Month Extension',
        ]);
    }
}
