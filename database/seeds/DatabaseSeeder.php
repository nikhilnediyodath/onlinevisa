<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CountriesTableSeeder::class,
            LanguagesTableSeeder::class,
            VisaTypesTableSeeder::class,
            SettingsTableSeeder::class,
            PricesTableSeeder::class,
            PromocodesTableSeeder::class,
            MailTemplatesTableSeeder::class,
            // ApplicationsTableSeeder::class,
            // VisasTableSeeder::class,
            // PaymentsTableSeeder::class,
            // FilesTableSeeder::class,
        ]);
    }
}
