<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'id' => 1,
            'name' => 'Applications',
            'text' => '3024',
        ]);
        DB::table('settings')->insert([
            'id' => 2,
            'name' => 'Rejections',
            'text' => '24',
        ]);
        DB::table('settings')->insert([
            'id' => 3,
            'name' => 'In progress',
            'text' => '520',
        ]);
        DB::table('settings')->insert([
            'id' => 4,
            'name' => 'Visa Issued',
            'text' => '2480',
        ]);
    }
}
