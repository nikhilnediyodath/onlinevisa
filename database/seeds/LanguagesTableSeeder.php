<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'code' => 'ab',
            'name' => 'Abkhaz',
            'native_name' => 'аҧсуа',
        ]);
        DB::table('languages')->insert([
            'code' => 'aa',
            'name' => 'Afar',
            'native_name' => 'Afaraf',
        ]);
        DB::table('languages')->insert([
            'code' => 'af',
            'name' => 'Afrikaans',
            'native_name' => 'Afrikaans',
        ]);
        DB::table('languages')->insert([
            'code' => 'ak',
            'name' => 'Akan',
            'native_name' => 'Akan',
        ]);
        DB::table('languages')->insert([
            'code' => 'sq',
            'name' => 'Albanian',
            'native_name' => 'Shqip',
        ]);
        DB::table('languages')->insert([
            'code' => 'am',
            'name' => 'Amharic',
            'native_name' => 'አማርኛ',
        ]);
        DB::table('languages')->insert([
            'code' => 'ar',
            'name' => 'Arabic',
            'native_name' => 'العربية',
        ]);
        DB::table('languages')->insert([
            'code' => 'an',
            'name' => 'Aragonese',
            'native_name' => 'Aragonés',
        ]);
        DB::table('languages')->insert([
            'code' => 'hy',
            'name' => 'Armenian',
            'native_name' => 'Հայերեն',
        ]);
        DB::table('languages')->insert([
            'code' => 'as',
            'name' => 'Assamese',
            'native_name' => 'অসমীয়া',
        ]);
        DB::table('languages')->insert([
            'code' => 'av',
            'name' => 'Avaric',
            'native_name' => 'авар мацӀ, магӀарул мацӀ',
        ]);
        DB::table('languages')->insert([
            'code' => 'ae',
            'name' => 'Avestan',
            'native_name' => 'avesta',
        ]);
        DB::table('languages')->insert([
            'code' => 'ay',
            'name' => 'Aymara',
            'native_name' => 'aymar aru',
        ]);
        DB::table('languages')->insert([
            'code' => 'az',
            'name' => 'Azerbaijani',
            'native_name' => 'azərbaycan dili',
        ]);
        DB::table('languages')->insert([
            'code' => 'bm',
            'name' => 'Bambara',
            'native_name' => 'bamanankan',
        ]);
        DB::table('languages')->insert([
            'code' => 'ba',
            'name' => 'Bashkir',
            'native_name' => 'башҡорт теле',
        ]);
        DB::table('languages')->insert([
            'code' => 'eu',
            'name' => 'Basque',
            'native_name' => 'euskara, euskera',
        ]);
        DB::table('languages')->insert([
            'code' => 'be',
            'name' => 'Belarusian',
            'native_name' => 'Беларуская',
        ]);
        DB::table('languages')->insert([
            'code' => 'bn',
            'name' => 'Bengali',
            'native_name' => 'বাংলা',
        ]);
        DB::table('languages')->insert([
            'code' => 'bh',
            'name' => 'Bihari',
            'native_name' => 'भोजपुरी',
        ]);
        DB::table('languages')->insert([
            'code' => 'bi',
            'name' => 'Bislama',
            'native_name' => 'Bislama',
        ]);
        DB::table('languages')->insert([
            'code' => 'bs',
            'name' => 'Bosnian',
            'native_name' => 'bosanski jezik',
        ]);
        DB::table('languages')->insert([
            'code' => 'br',
            'name' => 'Breton',
            'native_name' => 'brezhoneg',
        ]);
        DB::table('languages')->insert([
            'code' => 'bg',
            'name' => 'Bulgarian',
            'native_name' => 'български език',
        ]);
        DB::table('languages')->insert([
            'code' => 'my',
            'name' => 'Burmese',
            'native_name' => 'ဗမာစာ',
        ]);
        DB::table('languages')->insert([
            'code' => 'ca',
            'name' => 'Catalan; Valencian',
            'native_name' => 'Català',
        ]);
        DB::table('languages')->insert([
            'code' => 'ch',
            'name' => 'Chamorro',
            'native_name' => 'Chamoru',
        ]);
        DB::table('languages')->insert([
            'code' => 'ce',
            'name' => 'Chechen',
            'native_name' => 'нохчийн мотт',
        ]);
        DB::table('languages')->insert([
            'code' => 'ny',
            'name' => 'Chichewa; Chewa; Nyanja',
            'native_name' => 'chiCheŵa, chinyanja',
        ]);
        DB::table('languages')->insert([
            'code' => 'zh',
            'name' => 'Chinese',
            'native_name' => '中文 (Zhōngwén), 汉语, 漢語',
        ]);
        DB::table('languages')->insert([
            'code' => 'cv',
            'name' => 'Chuvash',
            'native_name' => 'чӑваш чӗлхи',
        ]);
        DB::table('languages')->insert([
            'code' => 'kw',
            'name' => 'Cornish',
            'native_name' => 'Kernewek',
        ]);
        DB::table('languages')->insert([
            'code' => 'co',
            'name' => 'Corsican',
            'native_name' => 'corsu, lingua corsa',
        ]);
        DB::table('languages')->insert([
            'code' => 'cr',
            'name' => 'Cree',
            'native_name' => 'ᓀᐦᐃᔭᐍᐏᐣ',
        ]);
        DB::table('languages')->insert([
            'code' => 'hr',
            'name' => 'Croatian',
            'native_name' => 'hrvatski',
        ]);
        DB::table('languages')->insert([
            'code' => 'cs',
            'name' => 'Czech',
            'native_name' => 'česky, čeština',
        ]);
        DB::table('languages')->insert([
            'code' => 'da',
            'name' => 'Danish',
            'native_name' => 'dansk',
        ]);
        DB::table('languages')->insert([
            'code' => 'dv',
            'name' => 'Divehi; Dhivehi; Maldivian;',
            'native_name' => 'ދިވެހި',
        ]);
        DB::table('languages')->insert([
            'code' => 'nl',
            'name' => 'Dutch',
            'native_name' => 'Nederlands, Vlaams',
        ]);
        DB::table('languages')->insert([
            'code' => 'en',
            'name' => 'English',
            'native_name' => 'English',
        ]);
        DB::table('languages')->insert([
            'code' => 'eo',
            'name' => 'Esperanto',
            'native_name' => 'Esperanto',
        ]);
        DB::table('languages')->insert([
            'code' => 'et',
            'name' => 'Estonian',
            'native_name' => 'eesti, eesti keel',
        ]);
        DB::table('languages')->insert([
            'code' => 'ee',
            'name' => 'Ewe',
            'native_name' => 'Eʋegbe',
        ]);
        DB::table('languages')->insert([
            'code' => 'fo',
            'name' => 'Faroese',
            'native_name' => 'føroyskt',
        ]);
        DB::table('languages')->insert([
            'code' => 'fj',
            'name' => 'Fijian',
            'native_name' => 'vosa Vakaviti',
        ]);
        DB::table('languages')->insert([
            'code' => 'fi',
            'name' => 'Finnish',
            'native_name' => 'suomi, suomen kieli',
        ]);
        DB::table('languages')->insert([
            'code' => 'fr',
            'name' => 'French',
            'native_name' => 'français, langue française',
        ]);
        DB::table('languages')->insert([
            'code' => 'ff',
            'name' => 'Fula; Fulah; Pulaar; Pular',
            'native_name' => 'Fulfulde, Pulaar, Pular',
        ]);
        DB::table('languages')->insert([
            'code' => 'gl',
            'name' => 'Galician',
            'native_name' => 'Galego',
        ]);
        DB::table('languages')->insert([
            'code' => 'ka',
            'name' => 'Georgian',
            'native_name' => 'ქართული',
        ]);
        DB::table('languages')->insert([
            'code' => 'de',
            'name' => 'German',
            'native_name' => 'Deutsch',
        ]);
        DB::table('languages')->insert([
            'code' => 'el',
            'name' => 'Greek, Modern',
            'native_name' => 'Ελληνικά',
        ]);
        DB::table('languages')->insert([
            'code' => 'gn',
            'name' => 'Guaraní',
            'native_name' => 'Avañeẽ',
        ]);
        DB::table('languages')->insert([
            'code' => 'gu',
            'name' => 'Gujarati',
            'native_name' => 'ગુજરાતી',
        ]);
        DB::table('languages')->insert([
            'code' => 'ht',
            'name' => 'Haitian; Haitian Creole',
            'native_name' => 'Kreyòl ayisyen',
        ]);
        DB::table('languages')->insert([
            'code' => 'ha',
            'name' => 'Hausa',
            'native_name' => 'Hausa, هَوُسَ',
        ]);
        DB::table('languages')->insert([
            'code' => 'he',
            'name' => 'Hebrew (modern)',
            'native_name' => 'עברית',
        ]);
        DB::table('languages')->insert([
            'code' => 'hz',
            'name' => 'Herero',
            'native_name' => 'Otjiherero',
        ]);
        DB::table('languages')->insert([
            'code' => 'hi',
            'name' => 'Hindi',
            'native_name' => 'हिन्दी, हिंदी',
        ]);
        DB::table('languages')->insert([
            'code' => 'ho',
            'name' => 'Hiri Motu',
            'native_name' => 'Hiri Motu',
        ]);
        DB::table('languages')->insert([
            'code' => 'hu',
            'name' => 'Hungarian',
            'native_name' => 'Magyar',
        ]);
        DB::table('languages')->insert([
            'code' => 'ia',
            'name' => 'Interlingua',
            'native_name' => 'Interlingua',
        ]);
        DB::table('languages')->insert([
            'code' => 'id',
            'name' => 'Indonesian',
            'native_name' => 'Bahasa Indonesia',
        ]);
        DB::table('languages')->insert([
            'code' => 'ie',
            'name' => 'Interlingue',
            'native_name' => 'Originally called Occidental; then Interlingue after WWII',
        ]);
        DB::table('languages')->insert([
            'code' => 'ga',
            'name' => 'Irish',
            'native_name' => 'Gaeilge',
        ]);
        DB::table('languages')->insert([
            'code' => 'ig',
            'name' => 'Igbo',
            'native_name' => 'Asụsụ Igbo',
        ]);
        DB::table('languages')->insert([
            'code' => 'ik',
            'name' => 'Inupiaq',
            'native_name' => 'Iñupiaq, Iñupiatun',
        ]);
        DB::table('languages')->insert([
            'code' => 'io',
            'name' => 'Ido',
            'native_name' => 'Ido',
        ]);
        DB::table('languages')->insert([
            'code' => 'is',
            'name' => 'Icelandic',
            'native_name' => 'Íslenska',
        ]);
        DB::table('languages')->insert([
            'code' => 'it',
            'name' => 'Italian',
            'native_name' => 'Italiano',
        ]);
        DB::table('languages')->insert([
            'code' => 'iu',
            'name' => 'Inuktitut',
            'native_name' => 'ᐃᓄᒃᑎᑐᑦ',
        ]);
        DB::table('languages')->insert([
            'code' => 'ja',
            'name' => 'Japanese',
            'native_name' => '日本語 (にほんご／にっぽんご)',
        ]);
        DB::table('languages')->insert([
            'code' => 'jv',
            'name' => 'Javanese',
            'native_name' => 'basa Jawa',
        ]);
        DB::table('languages')->insert([
            'code' => 'kl',
            'name' => 'Kalaallisut, Greenlandic',
            'native_name' => 'kalaallisut, kalaallit oqaasii',
        ]);
        DB::table('languages')->insert([
            'code' => 'kn',
            'name' => 'Kannada',
            'native_name' => 'ಕನ್ನಡ',
        ]);
        DB::table('languages')->insert([
            'code' => 'kr',
            'name' => 'Kanuri',
            'native_name' => 'Kanuri',
        ]);
        DB::table('languages')->insert([
            'code' => 'ks',
            'name' => 'Kashmiri',
            'native_name' => 'कश्मीरी, كشميري‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'kk',
            'name' => 'Kazakh',
            'native_name' => 'Қазақ тілі',
        ]);
        DB::table('languages')->insert([
            'code' => 'km',
            'name' => 'Khmer',
            'native_name' => 'ភាសាខ្មែរ',
        ]);
        DB::table('languages')->insert([
            'code' => 'ki',
            'name' => 'Kikuyu, Gikuyu',
            'native_name' => 'Gĩkũyũ',
        ]);
        DB::table('languages')->insert([
            'code' => 'rw',
            'name' => 'Kinyarwanda',
            'native_name' => 'Ikinyarwanda',
        ]);
        DB::table('languages')->insert([
            'code' => 'ky',
            'name' => 'Kirghiz, Kyrgyz',
            'native_name' => 'кыргыз тили',
        ]);
        DB::table('languages')->insert([
            'code' => 'kv',
            'name' => 'Komi',
            'native_name' => 'коми кыв',
        ]);
        DB::table('languages')->insert([
            'code' => 'kg',
            'name' => 'Kongo',
            'native_name' => 'KiKongo',
        ]);
        DB::table('languages')->insert([
            'code' => 'ko',
            'name' => 'Korean',
            'native_name' => '한국어 (韓國語), 조선말 (朝鮮語)',
        ]);
        DB::table('languages')->insert([
            'code' => 'ku',
            'name' => 'Kurdish',
            'native_name' => 'Kurdî, كوردی‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'kj',
            'name' => 'Kwanyama, Kuanyama',
            'native_name' => 'Kuanyama',
        ]);
        DB::table('languages')->insert([
            'code' => 'la',
            'name' => 'Latin',
            'native_name' => 'latine, lingua latina',
        ]);
        DB::table('languages')->insert([
            'code' => 'lb',
            'name' => 'Luxembourgish, Letzeburgesch',
            'native_name' => 'Lëtzebuergesch',
        ]);
        DB::table('languages')->insert([
            'code' => 'lg',
            'name' => 'Luganda',
            'native_name' => 'Luganda',
        ]);
        DB::table('languages')->insert([
            'code' => 'li',
            'name' => 'Limburgish, Limburgan, Limburger',
            'native_name' => 'Limburgs',
        ]);
        DB::table('languages')->insert([
            'code' => 'ln',
            'name' => 'Lingala',
            'native_name' => 'Lingála',
        ]);
        DB::table('languages')->insert([
            'code' => 'lo',
            'name' => 'Lao',
            'native_name' => 'ພາສາລາວ',
        ]);
        DB::table('languages')->insert([
            'code' => 'lt',
            'name' => 'Lithuanian',
            'native_name' => 'lietuvių kalba',
        ]);
        DB::table('languages')->insert([
            'code' => 'lu',
            'name' => 'Luba-Katanga',
            'native_name' => '',
        ]);
        DB::table('languages')->insert([
            'code' => 'lv',
            'name' => 'Latvian',
            'native_name' => 'latviešu valoda',
        ]);
        DB::table('languages')->insert([
            'code' => 'gv',
            'name' => 'Manx',
            'native_name' => 'Gaelg, Gailck',
        ]);
        DB::table('languages')->insert([
            'code' => 'mk',
            'name' => 'Macedonian',
            'native_name' => 'македонски јазик',
        ]);
        DB::table('languages')->insert([
            'code' => 'mg',
            'name' => 'Malagasy',
            'native_name' => 'Malagasy fiteny',
        ]);
        DB::table('languages')->insert([
            'code' => 'ms',
            'name' => 'Malay',
            'native_name' => 'bahasa Melayu, بهاس ملايو‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'ml',
            'name' => 'Malayalam',
            'native_name' => 'മലയാളം',
        ]);
        DB::table('languages')->insert([
            'code' => 'mt',
            'name' => 'Maltese',
            'native_name' => 'Malti',
        ]);
        DB::table('languages')->insert([
            'code' => 'mi',
            'name' => 'Māori',
            'native_name' => 'te reo Māori',
        ]);
        DB::table('languages')->insert([
            'code' => 'mr',
            'name' => 'Marathi (Marāṭhī)',
            'native_name' => 'मराठी',
        ]);
        DB::table('languages')->insert([
            'code' => 'mh',
            'name' => 'Marshallese',
            'native_name' => 'Kajin M̧ajeļ',
        ]);
        DB::table('languages')->insert([
            'code' => 'mn',
            'name' => 'Mongolian',
            'native_name' => 'монгол',
        ]);
        DB::table('languages')->insert([
            'code' => 'na',
            'name' => 'Nauru',
            'native_name' => 'Ekakairũ Naoero',
        ]);
        DB::table('languages')->insert([
            'code' => 'nv',
            'name' => 'Navajo, Navaho',
            'native_name' => 'Diné bizaad, Dinékʼehǰí',
        ]);
        DB::table('languages')->insert([
            'code' => 'nb',
            'name' => 'Norwegian Bokmål',
            'native_name' => 'Norsk bokmål',
        ]);
        DB::table('languages')->insert([
            'code' => 'nd',
            'name' => 'North Ndebele',
            'native_name' => 'isiNdebele',
        ]);
        DB::table('languages')->insert([
            'code' => 'ne',
            'name' => 'Nepali',
            'native_name' => 'नेपाली',
        ]);
        DB::table('languages')->insert([
            'code' => 'ng',
            'name' => 'Ndonga',
            'native_name' => 'Owambo',
        ]);
        DB::table('languages')->insert([
            'code' => 'nn',
            'name' => 'Norwegian Nynorsk',
            'native_name' => 'Norsk nynorsk',
        ]);
        DB::table('languages')->insert([
            'code' => 'no',
            'name' => 'Norwegian',
            'native_name' => 'Norsk',
        ]);
        DB::table('languages')->insert([
            'code' => 'ii',
            'name' => 'Nuosu',
            'native_name' => 'ꆈꌠ꒿ Nuosuhxop',
        ]);
        DB::table('languages')->insert([
            'code' => 'nr',
            'name' => 'South Ndebele',
            'native_name' => 'isiNdebele',
        ]);
        DB::table('languages')->insert([
            'code' => 'oc',
            'name' => 'Occitan',
            'native_name' => 'Occitan',
        ]);
        DB::table('languages')->insert([
            'code' => 'oj',
            'name' => 'Ojibwe, Ojibwa',
            'native_name' => 'ᐊᓂᔑᓈᐯᒧᐎᓐ',
        ]);
        DB::table('languages')->insert([
            'code' => 'cu',
            'name' => 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic',
            'native_name' => 'ѩзыкъ словѣньскъ',
        ]);
        DB::table('languages')->insert([
            'code' => 'om',
            'name' => 'Oromo',
            'native_name' => 'Afaan Oromoo',
        ]);
        DB::table('languages')->insert([
            'code' => 'or',
            'name' => 'Oriya',
            'native_name' => 'ଓଡ଼ିଆ',
        ]);
        DB::table('languages')->insert([
            'code' => 'os',
            'name' => 'Ossetian, Ossetic',
            'native_name' => 'ирон æвзаг',
        ]);
        DB::table('languages')->insert([
            'code' => 'pa',
            'name' => 'Panjabi, Punjabi',
            'native_name' => 'ਪੰਜਾਬੀ, پنجابی‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'pi',
            'name' => 'Pāli',
            'native_name' => 'पाऴि',
        ]);
        DB::table('languages')->insert([
            'code' => 'fa',
            'name' => 'Persian',
            'native_name' => 'فارسی',
        ]);
        DB::table('languages')->insert([
            'code' => 'pl',
            'name' => 'Polish',
            'native_name' => 'polski',
        ]);
        DB::table('languages')->insert([
            'code' => 'ps',
            'name' => 'Pashto, Pushto',
            'native_name' => 'پښتو',
        ]);
        DB::table('languages')->insert([
            'code' => 'pt',
            'name' => 'Portuguese',
            'native_name' => 'Português',
        ]);
        DB::table('languages')->insert([
            'code' => 'qu',
            'name' => 'Quechua',
            'native_name' => 'Runa Simi, Kichwa',
        ]);
        DB::table('languages')->insert([
            'code' => 'rm',
            'name' => 'Romansh',
            'native_name' => 'rumantsch grischun',
        ]);
        DB::table('languages')->insert([
            'code' => 'rn',
            'name' => 'Kirundi',
            'native_name' => 'kiRundi',
        ]);
        DB::table('languages')->insert([
            'code' => 'ro',
            'name' => 'Romanian, Moldavian, Moldovan',
            'native_name' => 'română',
        ]);
        DB::table('languages')->insert([
            'code' => 'ru',
            'name' => 'Russian',
            'native_name' => 'русский язык',
        ]);
        DB::table('languages')->insert([
            'code' => 'sa',
            'name' => 'Sanskrit (Saṁskṛta)',
            'native_name' => 'संस्कृतम्',
        ]);
        DB::table('languages')->insert([
            'code' => 'sc',
            'name' => 'Sardinian',
            'native_name' => 'sardu',
        ]);
        DB::table('languages')->insert([
            'code' => 'sd',
            'name' => 'Sindhi',
            'native_name' => 'सिन्धी, سنڌي، سندھی‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'se',
            'name' => 'Northern Sami',
            'native_name' => 'Davvisámegiella',
        ]);
        DB::table('languages')->insert([
            'code' => 'sm',
            'name' => 'Samoan',
            'native_name' => 'gagana faa Samoa',
        ]);
        DB::table('languages')->insert([
            'code' => 'sg',
            'name' => 'Sango',
            'native_name' => 'yângâ tî sängö',
        ]);
        DB::table('languages')->insert([
            'code' => 'sr',
            'name' => 'Serbian',
            'native_name' => 'српски језик',
        ]);
        DB::table('languages')->insert([
            'code' => 'gd',
            'name' => 'Scottish Gaelic; Gaelic',
            'native_name' => 'Gàidhlig',
        ]);
        DB::table('languages')->insert([
            'code' => 'sn',
            'name' => 'Shona',
            'native_name' => 'chiShona',
        ]);
        DB::table('languages')->insert([
            'code' => 'si',
            'name' => 'Sinhala, Sinhalese',
            'native_name' => 'සිංහල',
        ]);
        DB::table('languages')->insert([
            'code' => 'sk',
            'name' => 'Slovak',
            'native_name' => 'slovenčina',
        ]);
        DB::table('languages')->insert([
            'code' => 'sl',
            'name' => 'Slovene',
            'native_name' => 'slovenščina',
        ]);
        DB::table('languages')->insert([
            'code' => 'so',
            'name' => 'Somali',
            'native_name' => 'Soomaaliga, af Soomaali',
        ]);
        DB::table('languages')->insert([
            'code' => 'st',
            'name' => 'Southern Sotho',
            'native_name' => 'Sesotho',
        ]);
        DB::table('languages')->insert([
            'code' => 'es',
            'name' => 'Spanish; Castilian',
            'native_name' => 'español, castellano',
        ]);
        DB::table('languages')->insert([
            'code' => 'su',
            'name' => 'Sundanese',
            'native_name' => 'Basa Sunda',
        ]);
        DB::table('languages')->insert([
            'code' => 'sw',
            'name' => 'Swahili',
            'native_name' => 'Kiswahili',
        ]);
        DB::table('languages')->insert([
            'code' => 'ss',
            'name' => 'Swati',
            'native_name' => 'SiSwati',
        ]);
        DB::table('languages')->insert([
            'code' => 'sv',
            'name' => 'Swedish',
            'native_name' => 'svenska',
        ]);
        DB::table('languages')->insert([
            'code' => 'ta',
            'name' => 'Tamil',
            'native_name' => 'தமிழ்',
        ]);
        DB::table('languages')->insert([
            'code' => 'te',
            'name' => 'Telugu',
            'native_name' => 'తెలుగు',
        ]);
        DB::table('languages')->insert([
            'code' => 'tg',
            'name' => 'Tajik',
            'native_name' => 'тоҷикӣ, toğikī, تاجیکی‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'th',
            'name' => 'Thai',
            'native_name' => 'ไทย',
        ]);
        DB::table('languages')->insert([
            'code' => 'ti',
            'name' => 'Tigrinya',
            'native_name' => 'ትግርኛ',
        ]);
        DB::table('languages')->insert([
            'code' => 'bo',
            'name' => 'Tibetan Standard, Tibetan, Central',
            'native_name' => 'བོད་ཡིག',
        ]);
        DB::table('languages')->insert([
            'code' => 'tk',
            'name' => 'Turkmen',
            'native_name' => 'Türkmen, Түркмен',
        ]);
        DB::table('languages')->insert([
            'code' => 'tl',
            'name' => 'Tagalog',
            'native_name' => 'Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔',
        ]);
        DB::table('languages')->insert([
            'code' => 'tn',
            'name' => 'Tswana',
            'native_name' => 'Setswana',
        ]);
        DB::table('languages')->insert([
            'code' => 'to',
            'name' => 'Tonga (Tonga Islands)',
            'native_name' => 'faka Tonga',
        ]);
        DB::table('languages')->insert([
            'code' => 'tr',
            'name' => 'Turkish',
            'native_name' => 'Türkçe',
        ]);
        DB::table('languages')->insert([
            'code' => 'ts',
            'name' => 'Tsonga',
            'native_name' => 'Xitsonga',
        ]);
        DB::table('languages')->insert([
            'code' => 'tt',
            'name' => 'Tatar',
            'native_name' => 'татарча, tatarça, تاتارچا‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'tw',
            'name' => 'Twi',
            'native_name' => 'Twi',
        ]);
        DB::table('languages')->insert([
            'code' => 'ty',
            'name' => 'Tahitian',
            'native_name' => 'Reo Tahiti',
        ]);
        DB::table('languages')->insert([
            'code' => 'ug',
            'name' => 'Uighur, Uyghur',
            'native_name' => 'Uyƣurqə, ئۇيغۇرچە‎',
        ]);
        DB::table('languages')->insert([
            'code' => 'uk',
            'name' => 'Ukrainian',
            'native_name' => 'українська',
        ]);
        DB::table('languages')->insert([
            'code' => 'ur',
            'name' => 'Urdu',
            'native_name' => 'اردو',
        ]);
        DB::table('languages')->insert([
            'code' => 'uz',
            'name' => 'Uzbek',
            'native_name' => 'zbek, Ўзбек, أۇزبېك‎',
        ]);
        DB::table('languages')->insert([
            'code' => 've',
            'name' => 'Venda',
            'native_name' => 'Tshivenḓa',
        ]);
        DB::table('languages')->insert([
            'code' => 'vi',
            'name' => 'Vietnamese',
            'native_name' => 'Tiếng Việt',
        ]);
        DB::table('languages')->insert([
            'code' => 'vo',
            'name' => 'Volapük',
            'native_name' => 'Volapük',
        ]);
        DB::table('languages')->insert([
            'code' => 'wa',
            'name' => 'Walloon',
            'native_name' => 'Walon',
        ]);
        DB::table('languages')->insert([
            'code' => 'cy',
            'name' => 'Welsh',
            'native_name' => 'Cymraeg',
        ]);
        DB::table('languages')->insert([
            'code' => 'wo',
            'name' => 'Wolof',
            'native_name' => 'Wollof',
        ]);
        DB::table('languages')->insert([
            'code' => 'fy',
            'name' => 'Western Frisian',
            'native_name' => 'Frysk',
        ]);
        DB::table('languages')->insert([
            'code' => 'xh',
            'name' => 'Xhosa',
            'native_name' => 'isiXhosa',
        ]);
        DB::table('languages')->insert([
            'code' => 'yi',
            'name' => 'Yiddish',
            'native_name' => 'ייִדיש',
        ]);
        DB::table('languages')->insert([
            'code' => 'yo',
            'name' => 'Yoruba',
            'native_name' => 'Yorùbá',
        ]);
        DB::table('languages')->insert([
            'code' => 'za',
            'name' => 'Zhuang, Chuang',
            'native_name' => 'Saɯ cueŋƅ, Saw cuengh',
        ]);
    }
}
