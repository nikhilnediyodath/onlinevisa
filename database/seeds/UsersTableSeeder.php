<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Nikhil',
            'email' => 'nikhilnediyodath@gmail.com',
            'password' => bcrypt('12345678'),
            'is_admin' => true,
        ]);
    }
}
