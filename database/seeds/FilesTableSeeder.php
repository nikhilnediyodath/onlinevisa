<?php

use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('files')->insert([

            'id' => '1',
            'parent_id' => '3',
            'parent_type' => 'App\Models\Visa',
            'name' => 'files/V2HFNPhtfOprO9Uypfu5benUXjbxrKUbHkCTlZQa.jpeg',
            'file_type' => 'user_image',
            'created_at' => '2019-10-25 09:39:19',
            'updated_at' => '2019-10-25 09:45:17',
        ]);
        
        DB::table('files')->insert([

            'id' => '2',
            'parent_id' => '3',
            'parent_type' => 'App\Models\Visa',
            'name' => 'files/wkQFS7aUzRHuSj0JnzlOK0AwhJha9kokEU9xaiAI.jpeg',
            'file_type' => 'passport_first_page',
            'created_at' => '2019-10-25 09:44:21',
            'updated_at' => '2019-10-25 09:45:17',
        ]);
        
        DB::table('files')->insert([

            'id' => '3',
            'parent_id' => '3',
            'parent_type' => 'App\Models\Visa',
            'name' => 'files/oaxXaHOfHTfr4wdQcdgZOyCEq0DT7npvcNKbYafN.pdf',
            'file_type' => 'passport_last_page',
            'created_at' => '2019-10-25 09:44:29',
            'updated_at' => '2019-10-25 09:45:17',
        ]);
        
        DB::table('files')->insert([

            'id' => '4',
            'parent_id' => '4',
            'parent_type' => 'App\Models\Visa',
            'name' => 'files/VCxIwtViLHZuJvoApLeCpL8hnYSXGUJ44V98p5j2.jpeg',
            'file_type' => 'user_image',
            'created_at' => '2019-10-25 09:52:28',
            'updated_at' => '2019-10-25 09:55:54',
        ]);
        
        DB::table('files')->insert([

            'id' => '5',
            'parent_id' => '4',
            'parent_type' => 'App\Models\Visa',
            'name' => 'files/8Z7uKAetf9CUfvP43Vhskx8wWp6tpQR8yFuEiSgT.jpeg',
            'file_type' => 'passport_first_page',
            'created_at' => '2019-10-25 09:53:14',
            'updated_at' => '2019-10-25 09:55:54',
        ]);
        
        DB::table('files')->insert([

            'id' => '6',
            'parent_id' => '4',
            'parent_type' => 'App\Models\Visa',
            'name' => 'files/M60mbmEPWjQKsCVFzsZgZwwcc2KtVgNsCJfikjRB.jpeg',
            'file_type' => 'passport_last_page',
            'created_at' => '2019-10-25 09:53:19',
            'updated_at' => '2019-10-25 09:55:54',
        ]);
    }
}
