<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            'application_id' => '22',
            'reference_token' => 'asdfvbtth',
            'transaction_no' => 'FOICPM67501570196733',
            'payment_transaction_id' => '030023749603',
            'request_amount' => '300',
            'customer_amount' => '300',
            'merchant_amount' => '300',
            'status' => 'success',
           
        ]);
        DB::table('payments')->insert([
            'application_id' => '23',
            'reference_token' => 'gfhfhfhf',
            'transaction_no' => 'FOICPM67501570196733',
            'payment_transaction_id' => '030023749603',
            'request_amount' => '300',
            'customer_amount' => '300',
            'merchant_amount' => '300',
            'status' => 'success',
           
        ]);
        DB::table('payments')->insert([
            'application_id' => '24',
            'reference_token' => 'rytytuyi,sc',
            'transaction_no' => 'FOICPM67501570196733',
            'payment_transaction_id' => '030023749603',
            'request_amount' => '300',
            'customer_amount' => '300',
            'merchant_amount' => '300',
            'status' => 'pending',
           
        ]);
    }
}
