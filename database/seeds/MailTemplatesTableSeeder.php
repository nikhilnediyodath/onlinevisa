<?php

use Illuminate\Database\Seeder;

class MailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mail_templates')->insert([
            'id' => 1,
            'name' => 'Application received',
            'subject' => 'Application received',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 2,
            'name' => 'Documents Requird',
            'subject' => 'Documents Requird',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 3,
            'name' => 'Application rejected by Makto',
            'subject' => 'Application rejected by Makto',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 4,
            'name' => 'Payment Requested',
            'subject' => 'Payment Requested',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 5,
            'name' => 'Payment Error',
            'subject' => 'Payment Error',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 6,
            'name' => 'Payment Received',
            'subject' => 'Payment Received',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 7,
            'name' => 'Application rejected by immigration',
            'subject' => 'Application rejected by immigration',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 8,
            'name' => 'Visa Ready',
            'subject' => 'Visa Ready',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 9,
            'name' => 'Application Received (For admin)',
            'subject' => 'New visa application received',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 10,
            'name' => 'Payment Received (For admin)',
            'subject' => 'New payment received',
            'text' => "<p>Your&nbsp;application&nbsp;reference&nbsp;number&nbsp;is <strong>REFERENCE_NO</strong>.&nbsp;</p>",
        ]);
        DB::table('mail_templates')->insert([
            'id' => 11,
            'name' => 'Signature',
            'subject' => 'NOT NEEDED',
            'text' => "<p>Regards onlinevisa</p>",
        ]);
    }
}
