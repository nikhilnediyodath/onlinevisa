<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('prices')->insert([
            'visa_type_id' => '1',
            'citizenship_id' => '*',
            'residence_id' => '*',
            'entry_type' => 'Multiple Entry',
            'amount' => '350',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '1',
            'citizenship_id' => '*',
            'residence_id' => '*',
            'entry_type' => 'Single Entry',
            'amount' => '250',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '2',
            'citizenship_id' => '*',
            'residence_id' => '*',
            'entry_type' => 'Single Entry',
            'amount' => '275',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '2',
            'citizenship_id' => '*',
            'residence_id' => '*',
            'entry_type' => 'Multiple Entry',
            'amount' => '280',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '3',
            'citizenship_id' => '*',
            'residence_id' => '*',
            'entry_type' => 'Multiple Entry',
            'amount' => '280',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '3',
            'citizenship_id' => '*',
            'residence_id' => '*',
            'entry_type' => 'Single Entry',
            'amount' => '280',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '1',
            'citizenship_id' => 'IN',
            'residence_id' => 'PK',
            'entry_type' => 'Single Entry',
            'amount' => '280',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '2',
            'citizenship_id' => 'IN',
            'residence_id' => 'PK',
            'entry_type' => 'Multiple Entry',
            'amount' => '200',
        ]);
        DB::table('prices')->insert([
            'visa_type_id' => '3',
            'citizenship_id' => 'IN',
            'residence_id' => 'PK',
            'entry_type' => 'Single Entry',
            'amount' => '300',
        ]);
        
        
    }
}
