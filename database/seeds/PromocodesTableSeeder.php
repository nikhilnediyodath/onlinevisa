<?php

use Illuminate\Database\Seeder;

class PromocodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promocodes')->insert([
            'name' => 'VISA15',
            'discount_type' => 'Fixed Amount',
            'amount' => '15',
            'minimum_amt' => '500',
            'maximum_amt' => '1000',
            'limit' => '1',
            'active' => 'Blocked',
        ]);
        DB::table('promocodes')->insert([
            'name' => 'VISA10',
            'discount_type' => 'Percentage',
            'amount' => '10',
            'minimum_amt' => '1000',
            'maximum_amt' => '1500',
            'limit' => '2',
        ]);
        DB::table('promocodes')->insert([
            'name' => 'VISA20',
            'discount_type' => 'Percentage',
            'amount' => '20',
        ]);
    }
}
