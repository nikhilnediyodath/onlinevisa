<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'name' => 'Afghanistan',
            'code' => 'AF',
        ]);
        DB::table('countries')->insert([
            'name' => 'Åland Islands',
            'code' => 'AX',
        ]);
        DB::table('countries')->insert([
            'name' => 'Albania',
            'code' => 'AL',
        ]);
        DB::table('countries')->insert([
            'name' => 'Algeria',
            'code' => 'DZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'American Samoa',
            'code' => 'AS',
        ]);
        DB::table('countries')->insert([
            'name' => 'AndorrA',
            'code' => 'AD',
        ]);
        DB::table('countries')->insert([
            'name' => 'Angola',
            'code' => 'AO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Anguilla',
            'code' => 'AI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Antarctica',
            'code' => 'AQ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Antigua and Barbuda',
            'code' => 'AG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Argentina',
            'code' => 'AR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Armenia',
            'code' => 'AM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Aruba',
            'code' => 'AW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Australia',
            'code' => 'AU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Austria',
            'code' => 'AT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Azerbaijan',
            'code' => 'AZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bahamas',
            'code' => 'BS',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bahrain',
            'code' => 'BH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bangladesh',
            'code' => 'BD',
        ]);
        DB::table('countries')->insert([
            'name' => 'Barbados',
            'code' => 'BB',
        ]);
        DB::table('countries')->insert([
            'name' => 'Belarus',
            'code' => 'BY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Belgium',
            'code' => 'BE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Belize',
            'code' => 'BZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Benin',
            'code' => 'BJ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bermuda',
            'code' => 'BM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bhutan',
            'code' => 'BT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bolivia',
            'code' => 'BO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bosnia and Herzegovina',
            'code' => 'BA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Botswana',
            'code' => 'BW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bouvet Island',
            'code' => 'BV',
        ]);
        DB::table('countries')->insert([
            'name' => 'Brazil',
            'code' => 'BR',
        ]);
        DB::table('countries')->insert([
            'name' => 'British Indian Ocean Territory',
            'code' => 'IO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Brunei Darussalam',
            'code' => 'BN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Bulgaria',
            'code' => 'BG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Burkina Faso',
            'code' => 'BF',
        ]);
        DB::table('countries')->insert([
            'name' => 'Burundi',
            'code' => 'BI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cambodia',
            'code' => 'KH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cameroon',
            'code' => 'CM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Canada',
            'code' => 'CA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cape Verde',
            'code' => 'CV',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cayman Islands',
            'code' => 'KY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Central African Republic',
            'code' => 'CF',
        ]);
        DB::table('countries')->insert([
            'name' => 'Chad',
            'code' => 'TD',
        ]);
        DB::table('countries')->insert([
            'name' => 'Chile',
            'code' => 'CL',
        ]);
        DB::table('countries')->insert([
            'name' => 'China',
            'code' => 'CN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Christmas Island',
            'code' => 'CX',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cocos (Keeling) Islands',
            'code' => 'CC',
        ]);
        DB::table('countries')->insert([
            'name' => 'Colombia',
            'code' => 'CO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Comoros',
            'code' => 'KM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Congo',
            'code' => 'CG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Congo, The Democratic Republic of the',
            'code' => 'CD',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cook Islands',
            'code' => 'CK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Costa Rica',
            'code' => 'CR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cote D\'Ivoire',
            'code' => 'CI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Croatia',
            'code' => 'HR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cuba',
            'code' => 'CU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Cyprus',
            'code' => 'CY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Czech Republic',
            'code' => 'CZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Denmark',
            'code' => 'DK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Djibouti',
            'code' => 'DJ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Dominica',
            'code' => 'DM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Dominican Republic',
            'code' => 'DO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Ecuador',
            'code' => 'EC',
        ]);
        DB::table('countries')->insert([
            'name' => 'Egypt',
            'code' => 'EG',
        ]);
        DB::table('countries')->insert([
            'name' => 'El Salvador',
            'code' => 'SV',
        ]);
        DB::table('countries')->insert([
            'name' => 'Equatorial Guinea',
            'code' => 'GQ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Eritrea',
            'code' => 'ER',
        ]);
        DB::table('countries')->insert([
            'name' => 'Estonia',
            'code' => 'EE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Ethiopia',
            'code' => 'ET',
        ]);
        DB::table('countries')->insert([
            'name' => 'Falkland Islands (Malvinas)',
            'code' => 'FK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Faroe Islands',
            'code' => 'FO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Fiji',
            'code' => 'FJ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Finland',
            'code' => 'FI',
        ]);
        DB::table('countries')->insert([
            'name' => 'France',
            'code' => 'FR',
        ]);
        DB::table('countries')->insert([
            'name' => 'French Guiana',
            'code' => 'GF',
        ]);
        DB::table('countries')->insert([
            'name' => 'French Polynesia',
            'code' => 'PF',
        ]);
        DB::table('countries')->insert([
            'name' => 'French Southern Territories',
            'code' => 'TF',
        ]);
        DB::table('countries')->insert([
            'name' => 'Gabon',
            'code' => 'GA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Gambia',
            'code' => 'GM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Georgia',
            'code' => 'GE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Germany',
            'code' => 'DE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Ghana',
            'code' => 'GH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Gibraltar',
            'code' => 'GI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Greece',
            'code' => 'GR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Greenland',
            'code' => 'GL',
        ]);
        DB::table('countries')->insert([
            'name' => 'Grenada',
            'code' => 'GD',
        ]);
        DB::table('countries')->insert([
            'name' => 'Guadeloupe',
            'code' => 'GP',
        ]);
        DB::table('countries')->insert([
            'name' => 'Guam',
            'code' => 'GU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Guatemala',
            'code' => 'GT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Guernsey',
            'code' => 'GG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Guinea',
            'code' => 'GN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Guinea-Bissau',
            'code' => 'GW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Guyana',
            'code' => 'GY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Haiti',
            'code' => 'HT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Heard Island and Mcdonald Islands',
            'code' => 'HM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Holy See (Vatican City State)',
            'code' => 'VA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Honduras',
            'code' => 'HN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Hong Kong',
            'code' => 'HK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Hungary',
            'code' => 'HU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Iceland',
            'code' => 'IS',
        ]);
        DB::table('countries')->insert([
            'name' => 'India',
            'code' => 'IN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Indonesia',
            'code' => 'ID',
        ]);
        DB::table('countries')->insert([
            'name' => 'Iran, Islamic Republic Of',
            'code' => 'IR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Iraq',
            'code' => 'IQ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Ireland',
            'code' => 'IE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Isle of Man',
            'code' => 'IM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Israel',
            'code' => 'IL',
        ]);
        DB::table('countries')->insert([
            'name' => 'Italy',
            'code' => 'IT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Jamaica',
            'code' => 'JM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Japan',
            'code' => 'JP',
        ]);
        DB::table('countries')->insert([
            'name' => 'Jersey',
            'code' => 'JE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Jordan',
            'code' => 'JO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Kazakhstan',
            'code' => 'KZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Kenya',
            'code' => 'KE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Kiribati',
            'code' => 'KI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Korea, Democratic People\'S Republic of',
            'code' => 'KP',
        ]);
        DB::table('countries')->insert([
            'name' => 'Korea, Republic of',
            'code' => 'KR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Kuwait',
            'code' => 'KW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Kyrgyzstan',
            'code' => 'KG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Lao People\'S Democratic Republic',
            'code' => 'LA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Latvia',
            'code' => 'LV',
        ]);
        DB::table('countries')->insert([
            'name' => 'Lebanon',
            'code' => 'LB',
        ]);
        DB::table('countries')->insert([
            'name' => 'Lesotho',
            'code' => 'LS',
        ]);
        DB::table('countries')->insert([
            'name' => 'Liberia',
            'code' => 'LR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Libyan Arab Jamahiriya',
            'code' => 'LY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Liechtenstein',
            'code' => 'LI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Lithuania',
            'code' => 'LT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Luxembourg',
            'code' => 'LU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Macao',
            'code' => 'MO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Macedonia, The Former Yugoslav Republic of',
            'code' => 'MK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Madagascar',
            'code' => 'MG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Malawi',
            'code' => 'MW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Malaysia',
            'code' => 'MY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Maldives',
            'code' => 'MV',
        ]);
        DB::table('countries')->insert([
            'name' => 'Mali',
            'code' => 'ML',
        ]);
        DB::table('countries')->insert([
            'name' => 'Malta',
            'code' => 'MT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Marshall Islands',
            'code' => 'MH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Martinique',
            'code' => 'MQ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Mauritania',
            'code' => 'MR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Mauritius',
            'code' => 'MU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Mayotte',
            'code' => 'YT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Mexico',
            'code' => 'MX',
        ]);
        DB::table('countries')->insert([
            'name' => 'Micronesia, Federated States of',
            'code' => 'FM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Moldova, Republic of',
            'code' => 'MD',
        ]);
        DB::table('countries')->insert([
            'name' => 'Monaco',
            'code' => 'MC',
        ]);
        DB::table('countries')->insert([
            'name' => 'Mongolia',
            'code' => 'MN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Montserrat',
            'code' => 'MS',
        ]);
        DB::table('countries')->insert([
            'name' => 'Morocco',
            'code' => 'MA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Mozambique',
            'code' => 'MZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Myanmar',
            'code' => 'MM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Namibia',
            'code' => 'NA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Nauru',
            'code' => 'NR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Nepal',
            'code' => 'NP',
        ]);
        DB::table('countries')->insert([
            'name' => 'Netherlands',
            'code' => 'NL',
        ]);
        DB::table('countries')->insert([
            'name' => 'Netherlands Antilles',
            'code' => 'AN',
        ]);
        DB::table('countries')->insert([
            'name' => 'New Caledonia',
            'code' => 'NC',
        ]);
        DB::table('countries')->insert([
            'name' => 'New Zealand',
            'code' => 'NZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Nicaragua',
            'code' => 'NI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Niger',
            'code' => 'NE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Nigeria',
            'code' => 'NG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Niue',
            'code' => 'NU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Norfolk Island',
            'code' => 'NF',
        ]);
        DB::table('countries')->insert([
            'name' => 'Northern Mariana Islands',
            'code' => 'MP',
        ]);
        DB::table('countries')->insert([
            'name' => 'Norway',
            'code' => 'NO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Oman',
            'code' => 'OM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Pakistan',
            'code' => 'PK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Palau',
            'code' => 'PW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Palestinian Territory, Occupied',
            'code' => 'PS',
        ]);
        DB::table('countries')->insert([
            'name' => 'Panama',
            'code' => 'PA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Papua New Guinea',
            'code' => 'PG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Paraguay',
            'code' => 'PY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Peru',
            'code' => 'PE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Philippines',
            'code' => 'PH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Pitcairn',
            'code' => 'PN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Poland',
            'code' => 'PL',
        ]);
        DB::table('countries')->insert([
            'name' => 'Portugal',
            'code' => 'PT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Puerto Rico',
            'code' => 'PR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Qatar',
            'code' => 'QA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Reunion',
            'code' => 'RE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Romania',
            'code' => 'RO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Russian Federation',
            'code' => 'RU',
        ]);
        DB::table('countries')->insert([
            'name' => 'RWANDA',
            'code' => 'RW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Saint Helena',
            'code' => 'SH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Saint Kitts and Nevis',
            'code' => 'KN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Saint Lucia',
            'code' => 'LC',
        ]);
        DB::table('countries')->insert([
            'name' => 'Saint Pierre and Miquelon',
            'code' => 'PM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Saint Vincent and the Grenadines',
            'code' => 'VC',
        ]);
        DB::table('countries')->insert([
            'name' => 'Samoa',
            'code' => 'WS',
        ]);
        DB::table('countries')->insert([
            'name' => 'San Marino',
            'code' => 'SM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Sao Tome and Principe',
            'code' => 'ST',
        ]);
        DB::table('countries')->insert([
            'name' => 'Saudi Arabia',
            'code' => 'SA',
        ]);
        DB::table('countries')->insert([
            'name' => 'Senegal',
            'code' => 'SN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Serbia and Montenegro',
            'code' => 'CS',
        ]);
        DB::table('countries')->insert([
            'name' => 'Seychelles',
            'code' => 'SC',
        ]);
        DB::table('countries')->insert([
            'name' => 'Sierra Leone',
            'code' => 'SL',
        ]);
        DB::table('countries')->insert([
            'name' => 'Singapore',
            'code' => 'SG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Slovakia',
            'code' => 'SK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Slovenia',
            'code' => 'SI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Solomon Islands',
            'code' => 'SB',
        ]);
        DB::table('countries')->insert([
            'name' => 'Somalia',
            'code' => 'SO',
        ]);
        DB::table('countries')->insert([
            'name' => 'South Africa',
            'code' => 'ZA',
        ]);
        DB::table('countries')->insert([
            'name' => 'South Georgia and the South Sandwich Islands',
            'code' => 'GS',
        ]);
        DB::table('countries')->insert([
            'name' => 'Spain',
            'code' => 'ES',
        ]);
        DB::table('countries')->insert([
            'name' => 'Sri Lanka',
            'code' => 'LK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Sudan',
            'code' => 'SD',
        ]);
        DB::table('countries')->insert([
            'name' => 'Suriname',
            'code' => 'SR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Svalbard and Jan Mayen',
            'code' => 'SJ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Swaziland',
            'code' => 'SZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Sweden',
            'code' => 'SE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Switzerland',
            'code' => 'CH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Syrian Arab Republic',
            'code' => 'SY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Taiwan, Province of China',
            'code' => 'TW',
        ]);
        DB::table('countries')->insert([
            'name' => 'Tajikistan',
            'code' => 'TJ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Tanzania, United Republic of',
            'code' => 'TZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Thailand',
            'code' => 'TH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Timor-Leste',
            'code' => 'TL',
        ]);
        DB::table('countries')->insert([
            'name' => 'Togo',
            'code' => 'TG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Tokelau',
            'code' => 'TK',
        ]);
        DB::table('countries')->insert([
            'name' => 'Tonga',
            'code' => 'TO',
        ]);
        DB::table('countries')->insert([
            'name' => 'Trinidad and Tobago',
            'code' => 'TT',
        ]);
        DB::table('countries')->insert([
            'name' => 'Tunisia',
            'code' => 'TN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Turkey',
            'code' => 'TR',
        ]);
        DB::table('countries')->insert([
            'name' => 'Turkmenistan',
            'code' => 'TM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Turks and Caicos Islands',
            'code' => 'TC',
        ]);
        DB::table('countries')->insert([
            'name' => 'Tuvalu',
            'code' => 'TV',
        ]);
        DB::table('countries')->insert([
            'name' => 'Uganda',
            'code' => 'UG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Ukraine',
            'code' => 'UA',
        ]);
        DB::table('countries')->insert([
            'name' => 'United Arab Emirates',
            'code' => 'AE',
        ]);
        DB::table('countries')->insert([
            'name' => 'United Kingdom',
            'code' => 'GB',
        ]);
        DB::table('countries')->insert([
            'name' => 'United States',
            'code' => 'US',
        ]);
        DB::table('countries')->insert([
            'name' => 'United States Minor Outlying Islands',
            'code' => 'UM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Uruguay',
            'code' => 'UY',
        ]);
        DB::table('countries')->insert([
            'name' => 'Uzbekistan',
            'code' => 'UZ',
        ]);
        DB::table('countries')->insert([
            'name' => 'Vanuatu',
            'code' => 'VU',
        ]);
        DB::table('countries')->insert([
            'name' => 'Venezuela',
            'code' => 'VE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Viet Nam',
            'code' => 'VN',
        ]);
        DB::table('countries')->insert([
            'name' => 'Virgin Islands, British',
            'code' => 'VG',
        ]);
        DB::table('countries')->insert([
            'name' => 'Virgin Islands, U.S.',
            'code' => 'VI',
        ]);
        DB::table('countries')->insert([
            'name' => 'Wallis and Futuna',
            'code' => 'WF',
        ]);
        DB::table('countries')->insert([
            'name' => 'Western Sahara',
            'code' => 'EH',
        ]);
        DB::table('countries')->insert([
            'name' => 'Yemen',
            'code' => 'YE',
        ]);
        DB::table('countries')->insert([
            'name' => 'Zambia',
            'code' => 'ZM',
        ]);
        DB::table('countries')->insert([
            'name' => 'Zimbabwe',
            'code' => 'ZW',
        ]);
    }
}
