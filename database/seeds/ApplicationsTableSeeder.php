<?php

use Illuminate\Database\Seeder;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applications')->insert([
            'id' => '22',
            'reference' => 'REF201910031936231',
            'email' => 'asd@gmail.com',
            'email_verified_at' => null,
            'otp' => null,
            'status' => 'Processing',
            'promocode_id' => 1,
            'payment_status' => 'pending',
            'total_amt' => '300',
            'amount' => '300',
            'message' => 'We are processing your request. We will notify you further updates',
            'comment' => null,
            'updated_at' => '2019-10-10 11:47:19',
        ]);
        DB::table('applications')->insert([
            'id' => '23',
            'reference' => 'REF201910032105586',
            'email' => 'nnnn@gmail.com',
            'email_verified_at' => null,
            'otp' => null,
            'status' => 'Approved',
            'promocode_id' => 2,
            'payment_status' => 'pending',
            'total_amt' => '300',
            'amount' => '300',
            'message' => 'We are processing your request. We will notify you further updates',
            'comment' => null,
            'updated_at' => '2019-10-10 11:47:19',
        ]);
        DB::table('applications')->insert([
            'id' => '24',
            'reference' => 'REF201910031934765',
            'email' => 'eeee@gmail.com',
            'email_verified_at' => null,
            'otp' => null,
            'status' => 'Rejected',
            'promocode_id' => null,
            'payment_status' => 'pending',
            'total_amt' => '300',
            'amount' => '300',
            'message' => 'We are processing your request. We will notify you further updates',
            'comment' => null,
            'updated_at' => '2019-10-10 11:47:19',
        ]);
        
        DB::table('applications')->insert([
            'id' => '5',
            'reference' => 'REF201910031934766',
            'email' => 'eeee@gmail.com',
            'email_verified_at' => '2019-10-10 11:47:19',
            'otp' => null,
            'status' => 'Rejected',
            'promocode_id' => null,
            'payment_status' => 'pending',
            'total_amt' => '300',
            'amount' => '300',
            'message' => 'We are processing your request. We will notify you further updates',
            'comment' => null,
            'updated_at' => '2019-10-10 11:47:19',
        ]);

    }
}
