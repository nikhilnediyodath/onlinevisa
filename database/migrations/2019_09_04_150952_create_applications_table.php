<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('otp')->nullable();
            $table->string('status')->default('temp');
            $table->tinyInteger('status_code')->default(1);
            $table->bigInteger('promocode_id')->nullable();
            $table->string('payment_status')->default('Pending');
            $table->double('total_amt', 8, 2)->nullable();
            $table->double('amount', 8, 2)->nullable();
            $table->text('message')->nullable();
            $table->text('comment')->nullable();
            $table->timestamp('payment_requested_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
