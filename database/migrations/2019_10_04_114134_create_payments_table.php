<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('application_id')->nullable();
            $table->text('reference_token')->nullable();
            $table->string('transaction_no')->nullable();
            $table->string('payment_transaction_id')->nullable();
            $table->double('request_amount', 8, 2)->nullable();
            $table->double('customer_amount', 8, 2)->nullable();
            $table->double('merchant_amount', 8, 2)->nullable();
            $table->string('status')->default('pending');
            $table->string('created')->nullable();
            $table->string('optional1')->nullable();
            $table->string('optional2')->nullable();
            $table->string('optional3')->nullable();
            $table->timestamp('payment_received_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
