<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('discount_type');
            $table->double('amount', 8, 2);
            $table->double('minimum_amt', 8, 2)->nullable();
            $table->double('maximum_amt', 8, 2)->nullable();
            $table->double('limit', 8, 2)->nullable();
            $table->string('active')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocodes');
    }
}
